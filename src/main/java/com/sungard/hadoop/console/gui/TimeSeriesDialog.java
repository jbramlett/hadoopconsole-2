/*
 * @(#)TimeSeriesDialog.java
 *
 * Copyright 2013 Sungard and/or its affiliates. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or 
 * without modification, are permitted provided that the following 
 * conditions are met:
 * 
 * -Redistributions of source code must retain the above copyright  
 * notice, this  list of conditions and the following disclaimer.
 * 
 * -Redistribution in binary form must reproduct the above copyright 
 * notice, this list of conditions and the following disclaimer in 
 * the documentation and/or other materials provided with the 
 * distribution.
 * 
 * Neither the name of Sungard and/or its affiliates. or the names of 
 * contributors may be used to endorse or promote products derived 
 * from this software without specific prior written permission.
 * 
 * This software is provided "AS IS," without a warranty of any 
 * kind. ALL EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND 
 * WARRANTIES, INCLUDING ANY IMPLIED WARRANTY OF MERCHANTABILITY, 
 * FITNESS FOR A PARTICULAR PURPOSE OR NON-INFRINGEMENT, ARE HEREBY 
 * EXCLUDED. SUN AND ITS LICENSORS SHALL NOT BE LIABLE FOR ANY 
 * DAMAGES OR LIABILITIES  SUFFERED BY LICENSEE AS A RESULT OF  OR 
 * RELATING TO USE, MODIFICATION OR DISTRIBUTION OF THE SOFTWARE OR 
 * ITS DERIVATIVES. IN NO EVENT WILL SUN OR ITS LICENSORS BE LIABLE 
 * FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR DIRECT, INDIRECT, 
 * SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES, HOWEVER 
 * CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING OUT OF 
 * THE USE OF OR INABILITY TO USE SOFTWARE, EVEN IF SUN HAS BEEN 
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 * 
 * You acknowledge that Software is not designed, licensed or 
 * intended for use in the design, construction, operation or 
 * maintenance of any nuclear facility. 
 */
package com.sungard.hadoop.console.gui;

import java.awt.Color;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.hadoop.conf.Configuration;
import org.apache.commons.logging.LogFactory;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Shell;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.DateAxis;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYItemRenderer;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.time.Day;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;
import org.jfree.data.xy.XYDataset;
import org.jfree.experimental.chart.swt.ChartComposite;
import org.jfree.ui.RectangleInsets;

import com.google.gson.reflect.TypeToken;
import com.sungard.common.utils.JSONUtils;
import com.sungard.hadoop.console.profile.HadoopConfig;
import com.sungard.hadoop.console.profile.HadoopConfigFactory;

/**
 * Dialog used to display a time series of our monte carlo simulation
 * @author sungard
 */
public class TimeSeriesDialog extends BaseDialog {
    private static final Log logger = LogFactory.getLog(TimeSeriesDialog.class);
    
    protected Path rawDataFile;
    protected String cpty;
    protected String cob;
    
    /**
     * Constructor
     * @param shell
     */
    public TimeSeriesDialog(Shell shell, Path path, String cpty) {
        super(shell);
        this.rawDataFile = path;
        this.cpty = cpty;
        
        // now pull the cob from the path
        logger.info("Building time series for " + cpty + " from data in " + path.toString());
        String[] pathSplit = this.rawDataFile.toString().split("/");
        cob = pathSplit[pathSplit.length - 3];
        logger.info("Using COB " + cob);
    }

    /**
     * Constructor
     * @param shell
     * @param styles
     */
    public TimeSeriesDialog(Shell shell, int styles) {
        super(shell, styles);
    }

    /* (non-Javadoc)
     * @see com.sungard.hadoop.console.gui.BaseDialog#getTitle()
     */
    @Override
    protected String getTitle() {
        return "Monte Carlo Time Series - " + cpty + " (as of " + cob + ")";
    }

    /* (non-Javadoc)
     * @see com.sungard.hadoop.console.gui.BaseDialog#createControls(org.eclipse.swt.widgets.Composite)
     */
    @Override
    protected void createControls(Composite parent) {
        //Composite container = createContainer(parent, 1);
        JFreeChart chart = createChart(createDataset());
        ChartComposite frame = new ChartComposite(parent, SWT.NONE, chart, true);
        frame.setDisplayToolTips(true);
        frame.setHorizontalAxisTrace(false);
        frame.setVerticalAxisTrace(false);
        //frame.setSize(600, 300);
        
        //Composite buttonComposite = createContainer(parent, 1);
        //this.addCloseButton(buttonComposite);
    }

    /* (non-Javadoc)
     * @see com.sungard.hadoop.console.gui.BaseDialog#initDisplay(org.eclipse.swt.widgets.Composite)
     */
    @Override
    protected void initDisplay(Composite shell) {
        super.initDisplay(shell);
        shell.setSize(850,  650);
    }

    /**
     * Retrieves our path data
     * @return List<List<BigDecimal>> Our path data
     */
    protected List<List<BigDecimal>> getPathData() {
        List<List<BigDecimal>> result = null;
        try {
            JSONUtils<List<List<BigDecimal>>> jsonRawUtils = new JSONUtils<List<List<BigDecimal>>>(new TypeToken<List<List<BigDecimal>>>(){}.getType());

            Configuration config = getConfig();
            FileSystem fs = FileSystem.get(config);

            // find our profile dates file
            if (rawDataFile.getName().startsWith("result")) {
                logger.info("Found raw results file " + rawDataFile.toString());
                BufferedReader reader =new BufferedReader(new InputStreamReader(fs.open(rawDataFile)));
                
                // we will only process the first line - all other lines will
                // be ignored
                String line = reader.readLine();
                while (line != null){
                    String[] lineSplit = line.split("\t", 2);
                    if (cpty.equals(lineSplit[0])) {
                        logger.info("Found our data for our cpty");
                        result = jsonRawUtils.fromJSON(lineSplit[1]);
                        break;
                    }
                    line = reader.readLine();
                }
                reader.close();
                
                // return the first result
                return result;
            }           
        }
        catch (Exception e) {
            logger.error("Failed to load profile dates from HDFS!", e);
        }
        return null;
    }
    
    /**
     * Loads our profile dates from our distributed cache
     * @param config The config
     * @return Calendar[] the set of profile dates
     */
    protected Calendar[] getProfileDates() {
        Calendar[] results = null;
        
        try {
            JSONUtils<List<String>> jsonUtils = new JSONUtils<List<String>>(new TypeToken<List<String>>(){}.getType());

            Configuration config = getConfig();

            Path profileDatesFile = null;
            FileSystem fs = FileSystem.get(config);
            FileStatus[] roots = fs.listStatus(new Path("/user/sungard/creditrisk-montecarlo/profile_dates/" + cob));
            for (FileStatus f : roots) {
                if (f.getPath().getName().startsWith("profile_dates")) {
                    profileDatesFile = f.getPath();
                    break;
                }
            }
            
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
            
            logger.info("Find our profile dates for this run");
            if (profileDatesFile != null) {
                // find our profile dates file
                if (profileDatesFile.getName().startsWith("profile_dates")) {
                    logger.info("Found profile dates file " + profileDatesFile.toString());
                    BufferedReader reader =new BufferedReader(new InputStreamReader(fs.open(profileDatesFile)));
                    
                    // we will only process the first line - all other lines will
                    // be ignored
                    String line = reader.readLine();
                    while (line != null){
                        List<String> profileDates = jsonUtils.fromMRInput(line);
    
                        results = new Calendar[profileDates.size()];
                        for (int i = 0; i < profileDates.size(); i++) {
                            Calendar cal = Calendar.getInstance();
                            cal.setTime(dateFormat.parse(profileDates.get(i).trim()));
                            results[i] = cal;
                        }
                        line = reader.readLine();
                    }
                    reader.close();
                    
                    // return the first result
                    return results;
                }
                else {
                    logger.info("Failed to find our profile dates!");
                }
            }           
        }
        catch (Exception e) {
            logger.error("Failed to load profile dates from HDFS!", e);
        }
        return null;
    }
    
    /**
     * Gets our config for HDFS
     * @return Configuration The configuration
     */
    protected Configuration getConfig() {
        HadoopConfigFactory configFactory = new HadoopConfigFactory();
        HadoopConfig hConfig = configFactory.getConfig();           
        Configuration config = new Configuration();
        config.set("mapred.job.tracker", hConfig.getMapReduceMaster());
        config.set("fs.default.name", hConfig.getHdfsMaster());
        return config;
    }
    
    
    /**
     * Creates a dataset, consisting of two series of monthly data.
     *
     * @return The dataset.
     */
    protected XYDataset createDataset() {
        List<List<BigDecimal>> prices = getPathData();
        Calendar[] profileDates = getProfileDates();
        
        TimeSeriesCollection dataset = new TimeSeriesCollection();
        for (int i = 0; i < prices.size(); i++) {
            List<BigDecimal> path = prices.get(i);
            
            TimeSeries s1 = new TimeSeries("Path " + i);
            for (int d = 0; d < profileDates.length; d++) {
                Calendar c = profileDates[d];
                BigDecimal price = path.get(d);
                s1.add(new Day(c.getTime()), price.doubleValue());                
            }
            dataset.addSeries(s1);
        }
        
        return dataset;
    }
    
    /**
     * Creates a chart.
     * 
     * @param dataset  a dataset.
     * 
     * @return A chart.
     */
    private JFreeChart createChart(XYDataset dataset) {

        JFreeChart chart = ChartFactory.createTimeSeriesChart(
            "Monte Carlo Simulation",  // title
            "Date",             // x-axis label
            "Price",            // y-axis label
            dataset,            // data
            true,               // create legend?
            true,               // generate tooltips?
            false               // generate URLs?
        );

        chart.setBackgroundPaint(Color.white);

        XYPlot plot = (XYPlot) chart.getPlot();
        plot.setBackgroundPaint(Color.lightGray);
        plot.setDomainGridlinePaint(Color.white);
        plot.setRangeGridlinePaint(Color.white);
        plot.setAxisOffset(new RectangleInsets(5.0, 5.0, 5.0, 5.0));
        plot.setDomainCrosshairVisible(true);
        plot.setRangeCrosshairVisible(true);
        
        XYItemRenderer r = plot.getRenderer();
        if (r instanceof XYLineAndShapeRenderer) {
            XYLineAndShapeRenderer renderer = (XYLineAndShapeRenderer) r;
            renderer.setBaseShapesVisible(true);
            renderer.setBaseShapesFilled(true);
        }
        
        DateAxis axis = (DateAxis) plot.getDomainAxis();
        axis.setDateFormatOverride(new SimpleDateFormat("dd-MMM-yyyy"));
        
        return chart;

    }
    
    
}
