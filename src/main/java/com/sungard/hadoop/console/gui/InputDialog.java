/*
 * Copyright 2008 by RBC Capital Markets Inc., All rights reserved.
 * 
 * This source code, and resulting software, is confidential and proprietary
 * information and is the intellectual property of RBC Capital Markets Inc.
 * 
 * You shall not disclose such Proprietary Information and shall use it only in
 * accordance with the terms and conditions of any and all license agreements
 * you have entered into with The Company.
 */
package com.sungard.hadoop.console.gui;

import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

/**
 * Dialog used to enter a some generic text
 *
 * @author jbramlet
 */
public class InputDialog extends BaseDialog {
	protected Text inputText;
	protected Button okButton;
	protected String text;
	protected String title;
	protected String question;
	
	/**
	 * Constructor
	 * @param shell
	 */
	public InputDialog(Shell shell, String question, String title, String defaultText) {
		super(shell);
		this.title = title;
		this.question = question;
		this.text = defaultText;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.jpmorgan.gcrm.basel.ui.BaseDialog#processButton(java.lang.Object)
	 */
	protected boolean processButton(Object source) {
		try {			
			if (source == okButton) {
				text = inputText.getText();
				closeDialog();
			}
			else  {
				text = null;
				super.processButton(source);
			}
		}
		catch (Exception e) {
			throw new RuntimeException("Failed trying to open dialog!", e);
		}

		return true;
	}

	/* (non-Javadoc)
	 * @see org.thirdstreet.backup.gui.BaseDialog#createControls(org.eclipse.swt.widgets.Composite)
	 */
	@Override
	protected void createControls(Composite parent) {
		Composite container = createContainer(parent, 1);
		
		// now add our fields
		inputText = createText(container, 150, 20, question);
		
		// now add our buttons
		Composite buttonContainer = createContainer(container, 2);
		okButton = createButton(buttonContainer, "Ok");
		addCloseButton(buttonContainer, "Cancel");
	}

	/* (non-Javadoc)
	 * @see org.thirdstreet.backup.gui.BaseDialog#getTitle()
	 */
	@Override
	protected String getTitle() {		
		return title;
	}

	/**
	 * Gets the user entered text
	 * @return String The user entered text
	 */
	public String getText() {
		return text;
	}

}
