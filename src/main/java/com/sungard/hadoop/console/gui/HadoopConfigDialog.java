/*
 * Copyright 2008 by RBC Capital Markets Inc., All rights reserved.
 * 
 * This source code, and resulting software, is confidential and proprietary
 * information and is the intellectual property of RBC Capital Markets Inc.
 * 
 * You shall not disclose such Proprietary Information and shall use it only in
 * accordance with the terms and conditions of any and all license agreements
 * you have entered into with The Company.
 */
package com.sungard.hadoop.console.gui;

import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import com.sungard.hadoop.console.profile.HadoopConfig;
import com.sungard.hadoop.console.profile.HadoopConfigFactory;

/**
 * Allows you to edit a backup profile
 *
 * @author jbramlet
 */
public class HadoopConfigDialog extends BaseDialog {
	
	protected Text hadoopCommand;
	protected Button hadoopCommandPicker;
	protected Text nameNodeWebURL;
	protected Text jobTrackerWebURL;
	protected Text hdfsMaster;
	protected Text mapReduceMaster;
	protected Button saveButton;
	protected HadoopConfig config;
	protected boolean saved = false;
	
	/**
	 * Constructor
	 * @param shell The shell
	 * @param profile The profile we are editing
	 */
	public HadoopConfigDialog(Shell shell) {
		super(shell);
		HadoopConfigFactory configFactory = new HadoopConfigFactory();
		config = configFactory.getConfig();
	}
	/**
	 * Constructor
	 * 
	 * @param shell The owning shell
	 * @param styles The dialog style
	 */
	public HadoopConfigDialog(Shell shell, int styles) {
		super(shell, styles);
		HadoopConfigFactory configFactory = new HadoopConfigFactory();
		config = configFactory.getConfig();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.jpmorgan.gcrm.basel.ui.BaseDialog#getTitle()
	 */
	protected String getTitle() {
		return "Hadoop Configuration";
	}
	/*
	 * (non-Javadoc)
	 * 
	 * @see com.jpmorgan.gcrm.basel.ui.BaseDialog#processButton(java.lang.Object)
	 */
	protected boolean processButton(Object source) {
		boolean processed = super.processButton(source);

		try {			
			// if we didn't process this button in our super class then
			// process it here
			if (!processed) {
				// figure out what dialog to display for the button
				if (source == hadoopCommandPicker) {
					pickFile(hadoopCommand);
				}
				else if (source == saveButton) {
					saveSelected();
				}
			}
		}
		catch (Exception e) {
			throw new RuntimeException("Failed trying to open dialog!", e);
		}

		return processed;
	}
	
	
	/**
	 * Process the save button
	 */
	private void saveSelected() {
		// verify our content
		if ((hadoopCommand.getText() == null) || (hadoopCommand.getText().trim().length() == 0)) {
			this.showMessageBox("You must specify your hadoop command", "Error!");
			return;						
		}
		if ((jobTrackerWebURL.getText() == null) || (jobTrackerWebURL.getText().trim().length() == 0)) {
			this.showMessageBox("You must specify a JobTracker Web URL", "Error!");
			return;						
		}
		if ((nameNodeWebURL.getText() == null) || (nameNodeWebURL.getText().trim().length() == 0)) {
			this.showMessageBox("You must specify a NameNode Web URL", "Error!");
			return;						
		} 		
		if ((hdfsMaster.getText() == null) || (hdfsMaster.getText().trim().length() == 0)) {
			this.showMessageBox("You must specify an HDFS Master", "Error!");
			return;						
		}
		if ((mapReduceMaster.getText() == null) || (mapReduceMaster.getText().trim().length() == 0)) {
			this.showMessageBox("You must specify an Map/Reduce Master", "Error!");
			return;						
		}

		config.setHadoopCommand(hadoopCommand.getText());
		config.setNamenodeWebURL(nameNodeWebURL.getText());
		config.setJobTrackerWebURL(jobTrackerWebURL.getText());
		config.setHdfsMaster(hdfsMaster.getText());
		config.setMapReduceMaster(mapReduceMaster.getText());
		HadoopConfigFactory configFactory = new HadoopConfigFactory();
		configFactory.saveConfig(config);
		saved = true;
		this.closeDialog();		
	}
	
	/**
	 * Create the controls to display in the dialog
	 */
	protected void createControls(Composite parent) {
		Composite container = createContainer(parent, 1);


		// create a group box for the set of actions we can perform
		Group backupConfigGroup = createGroup(container, "Hadoop Configuration");

		// hadoop directory
		Composite sdContainer = createContainer(backupConfigGroup, 3);
		Composite sdTextLabelContainer = createContainer(sdContainer, 2);
		createLabel(sdTextLabelContainer, "Hadoop Dir:");
		hadoopCommand = createText(sdTextLabelContainer, 300, 0);
		if (config.getHadoopCommand() != null) hadoopCommand.setText(config.getHadoopCommand());
		hadoopCommandPicker = createButton(sdContainer, "Select", this, 0);
		
		// namenode website
		Composite tdContainer = createContainer(backupConfigGroup, 2);
		createLabel(tdContainer, "NameNode WebURL:");
		nameNodeWebURL = createText(tdContainer, 300, 0);
		if (config.getNamenodeWebURL() != null) nameNodeWebURL.setText(config.getNamenodeWebURL());

		// job tracker website
		Composite jdContainer = createContainer(backupConfigGroup, 2);
		createLabel(jdContainer, "JobTracker WebURL:");
		jobTrackerWebURL = createText(jdContainer, 300, 0);
		if (config.getJobTrackerWebURL() != null) jobTrackerWebURL.setText(config.getJobTrackerWebURL());

		// hdfs master website
		Composite hdContainer = createContainer(backupConfigGroup, 2);
		createLabel(hdContainer, "HDFS Master:");
		hdfsMaster = createText(hdContainer, 300, 0);
		if (config.getHdfsMaster() != null) hdfsMaster.setText(config.getHdfsMaster());

		// mapreduce master website
		Composite mdContainer = createContainer(backupConfigGroup, 2);
		createLabel(mdContainer, "Map/Reduce Master:");
		mapReduceMaster = createText(mdContainer, 300, 0);
		if (config.getMapReduceMaster() != null) mapReduceMaster.setText(config.getMapReduceMaster());

		// and add our run and close
		Composite buttonContainer = createContainer(container, 2);
		saveButton = createButton(buttonContainer, "Save");
		
		addCloseButton(buttonContainer);
	}

	/**
	 * Creates a new button for our set of actions
	 * 
	 * @param parent The parent on which this button will live
	 * @param label The button label
	 * @return Button The newly created button
	 */
	protected Button createButton(Composite parent, String label) {
		return createButton(parent, label, this, 150);
	}
	/**
	 * Gets the saved
	 * @return boolean The saved
	 */
	public boolean isSaved() {
		return saved;
	}

}
