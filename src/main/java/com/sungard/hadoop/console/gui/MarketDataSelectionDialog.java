/*
 * @(#)TimeSeriesCptySelectionDialog.java
 *
 * Copyright 2013 Sungard and/or its affiliates. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or 
 * without modification, are permitted provided that the following 
 * conditions are met:
 * 
 * -Redistributions of source code must retain the above copyright  
 * notice, this  list of conditions and the following disclaimer.
 * 
 * -Redistribution in binary form must reproduct the above copyright 
 * notice, this list of conditions and the following disclaimer in 
 * the documentation and/or other materials provided with the 
 * distribution.
 * 
 * Neither the name of Sungard and/or its affiliates. or the names of 
 * contributors may be used to endorse or promote products derived 
 * from this software without specific prior written permission.
 * 
 * This software is provided "AS IS," without a warranty of any 
 * kind. ALL EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND 
 * WARRANTIES, INCLUDING ANY IMPLIED WARRANTY OF MERCHANTABILITY, 
 * FITNESS FOR A PARTICULAR PURPOSE OR NON-INFRINGEMENT, ARE HEREBY 
 * EXCLUDED. SUN AND ITS LICENSORS SHALL NOT BE LIABLE FOR ANY 
 * DAMAGES OR LIABILITIES  SUFFERED BY LICENSEE AS A RESULT OF  OR 
 * RELATING TO USE, MODIFICATION OR DISTRIBUTION OF THE SOFTWARE OR 
 * ITS DERIVATIVES. IN NO EVENT WILL SUN OR ITS LICENSORS BE LIABLE 
 * FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR DIRECT, INDIRECT, 
 * SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES, HOWEVER 
 * CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING OUT OF 
 * THE USE OF OR INABILITY TO USE SOFTWARE, EVEN IF SUN HAS BEEN 
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 * 
 * You acknowledge that Software is not designed, licensed or 
 * intended for use in the design, construction, operation or 
 * maintenance of any nuclear facility. 
 */
package com.sungard.hadoop.console.gui;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Shell;

import com.sungard.common.utils.JSONUtils;
import com.sungard.hadoop.console.profile.HadoopConfig;
import com.sungard.hadoop.console.profile.HadoopConfigFactory;

/**
 * Dialog used to select a cpty for showing a time series
 * @author sungard
 */
public class MarketDataSelectionDialog extends BaseDialog {
    private static final Log logger = LogFactory.getLog(MarketDataSelectionDialog.class);
    protected Path cobDir;
    protected Combo marketDataTypeBox;
    protected Combo instanceComboBox;
    protected String selectedMktDataType;
    protected String selectedMktDataInstance;
    protected Button okButton;
    protected Map<String, List<String>> mktDataTypes;
    
    /**
     * Constructor
     * @param shell
     */
    public MarketDataSelectionDialog(Shell shell, Path path) {
        super(shell);
        cobDir = path;
    }

    /**
     * Constructor
     * @param shell
     * @param styles
     */
    public MarketDataSelectionDialog(Shell shell, int styles) {
        super(shell, styles);
    }

    /* (non-Javadoc)
     * @see com.sungard.hadoop.console.gui.BaseDialog#getTitle()
     */
    @Override
    protected String getTitle() {
        return "Select Market Data to Graph";
    }

    /* (non-Javadoc)
     * @see com.sungard.hadoop.console.gui.BaseDialog#createControls(org.eclipse.swt.widgets.Composite)
     */
    @Override
    protected void createControls(Composite parent) {
        Composite container = createContainer(parent, 1);
        mktDataTypes = getMktDataTypes();
        String[] listData = new String[mktDataTypes.keySet().size()];
        listData = mktDataTypes.keySet().toArray(listData);
        marketDataTypeBox = createComboBox(container, "Select Mkt Data:", listData);
        instanceComboBox = createComboBox(container, "Select Instance:", listData);
        
        marketDataTypeBox.addSelectionListener(new SelectionListener() {
            @Override
            public void widgetDefaultSelected(SelectionEvent arg0) {
                widgetSelected(arg0);
            }
            @Override
            public void widgetSelected(SelectionEvent arg0) {
                instanceComboBox.removeAll();
                List<String> instances = mktDataTypes.get(marketDataTypeBox.getText());
                String[] instanceData = new String[instances.size()];
                instanceData = instances.toArray(instanceData);
                instanceComboBox.setItems(instanceData);
                if (instanceComboBox.getItemCount() > 0) {
                    instanceComboBox.select(0);
                }
            }
            
        });

        Composite buttonContainer = createContainer(container, 2);
        okButton = createButton(buttonContainer, "Ok");
        addCloseButton(buttonContainer);
    }
    
    /* (non-Javadoc)
     * @see org.thirdstreet.backup.gui.BaseDialog#processButton(java.lang.Object)
     */
    @Override
    protected boolean processButton(Object source) {
        boolean processed = super.processButton(source);

        try {           
            // if we didn't process this button in our super class then
            // process it here
            if (!processed) {
                // figure out what dialog to display for the button
                if (source == okButton) {
                    selectedMktDataType = marketDataTypeBox.getText();
                    selectedMktDataInstance = instanceComboBox.getText();
                    closeDialog();
                }
            }
        }
        catch (Exception e) {
            throw new RuntimeException("Failed trying to open dialog!", e);
        }

        return processed;
    }
    

    /**
     * Retrieves the mkt data from our input file
     * @return Map<String, List<String>>  The market data
     */
    @SuppressWarnings({ "rawtypes", "unchecked" })
	protected Map<String, List<String>> getMktDataTypes() {
        Map<String, List<String>> result = new HashMap<String, List<String>>();
        try {
            Configuration config = getConfig();
            FileSystem fs = FileSystem.get(config);
            
            FileStatus[] filesInDir = fs.listStatus(cobDir);
            for (FileStatus f : filesInDir) {
            	if (f.getPath().getName().startsWith("path_")) {
                    logger.info("Found market data input file " + f.getPath().toString());
                    BufferedReader reader =new BufferedReader(new InputStreamReader(fs.open(f.getPath())));
                    
                    // we will only process the first line - all other lines will
                    // be ignored
                    String line = reader.readLine();
                    while (line != null){
                        String[] lineSplit = line.split("\t", 2);
                        Map jsonData = JSONUtils.fromJSONToMap(lineSplit[1]);
                        for (Object key : jsonData.keySet()) {
                        	String type = key.toString();
                        	List<String> values = new ArrayList<String>();
                        	Object keyValues = jsonData.get(key);
                        	if (Map.class.isAssignableFrom(keyValues.getClass())) {
                        		Map keyValuesMap = (Map)keyValues;
	                        	Object dm = keyValuesMap.get("data");
	                        	if (dm != null) {
	                        		Map dataMap = (Map)dm;
	                        		values.addAll(dataMap.keySet());
	                        	}
                        	}
                        	result.put(type, values);
                        }
                        break;
                    }
                    reader.close();
                    
                    // return the first result
                    return result;            		
            	}
            }
        }
        catch (Exception e) {
            logger.error("Failed to load cpty list from HDFS!", e);
        }
        return null;
    }
    /**
     * Gets our config for HDFS
     * @return Configuration The configuration
     */
    protected Configuration getConfig() {
        HadoopConfigFactory configFactory = new HadoopConfigFactory();
        HadoopConfig hConfig = configFactory.getConfig();           
        Configuration config = new Configuration();
        config.set("mapred.job.tracker", hConfig.getMapReduceMaster());
        config.set("fs.default.name", hConfig.getHdfsMaster());
        return config;
    }

    /**
     * Returns our selected selectedMktDataType
     * @return String The selected selectedMktDataType
     */
    public String getSelectedMktDataType() {
        return selectedMktDataType;
    }

    /**
     * Returns our selected instance
     * @return String The selected isntance
     */
    public String getSelectedMktDataInstance() {
        return selectedMktDataInstance;
    }
}
