/**
 * LogConfiguration.java
 * Dec 19, 2008
 */
package com.sungard.hadoop.console.gui;

import java.io.File;
import java.net.URL;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.ConsoleAppender;
import org.apache.log4j.FileAppender;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.PatternLayout;
import org.apache.log4j.xml.DOMConfigurator;

/**
 * Utility used to configure log4j
 * @author jbramlett
 */
public class LogConfiguration {

    /**
     * Constructor - declared private since access is via static methods
     */
    private LogConfiguration() {
	super();
    }

    /**
     * Configures log4j
     */
    public static void configure() {
	PatternLayout layout = new PatternLayout("%d [%t] %-5p %c - %m%n");
    
	// define our appender
	ConsoleAppender consoleAppender = new ConsoleAppender();
	consoleAppender.setName("console");
	consoleAppender.setLayout(layout);
	consoleAppender.activateOptions();
	
	// define our file appender
	FileAppender fileAppender = new FileAppender();
	fileAppender.setName("fileAppender");
	String outputDir = System.getProperty("user.home") + File.separator + "HadoopConsole" + File.separator + "logs" + File.separator;
	File f = new File(outputDir);
	if (!f.exists()) {
		f.mkdirs();
	}
	String filename = outputDir + "hadoopConsole.log";
	fileAppender.setAppend(false);
	fileAppender.setFile(filename);
	fileAppender.setLayout(layout);
	fileAppender.activateOptions();
	
	Logger root = Logger.getRootLogger();
	root.setLevel(Level.DEBUG);
	root.addAppender(consoleAppender);
	root.addAppender(fileAppender);
	
    }
    
    /**
     * Configures log4j from a file
     */
    public static void configureFromFile() {
	// use the current threads class loader to load in the given
	// resource
	//URL log4jUrl = Thread.currentThread().getContextClassLoader().getResource("log4j.xml");
    	URL log4jUrl = LogConfiguration.class.getResource("log4j.xml");
    
	// if we have our log4j config then initialize using it
	if (log4jUrl != null) {
		DOMConfigurator.configure(log4jUrl);
	}
	else {
		// otherwise just use the default
		BasicConfigurator.configure();
	}	
    }
}
