/*
 * Copyright 2008 by RBC Capital Markets Inc., All rights reserved.
 * 
 * This source code, and resulting software, is confidential and proprietary
 * information and is the intellectual property of RBC Capital Markets Inc.
 * 
 * You shall not disclose such Proprietary Information and shall use it only in
 * accordance with the terms and conditions of any and all license agreements
 * you have entered into with The Company.
 */
package com.sungard.hadoop.console.gui;

import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

/**
 * Dialog used to enter a some generic text
 *
 * @author jbramlet
 */
public class CopyItemInputDialog extends BaseDialog {
	protected Text srcDirText;
	protected Button dirSelectionButton;
	protected Text s3BucketText;
	protected Button okButton;
	protected String srcDir;
	protected String s3Bucket;
	
	/**
	 * Constructor
	 * @param shell
	 */
	public CopyItemInputDialog(Shell shell) {
		super(shell);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.jpmorgan.gcrm.basel.ui.BaseDialog#processButton(java.lang.Object)
	 */
	protected boolean processButton(Object source) {
		try {			
			if (source == okButton) {
				if (srcDirText.getText().length() == 0 || s3BucketText.getText().length() == 0) {
					this.showMessageBox("You must enter both src and bucket!", "Error");
				}
				else {
					srcDir = srcDirText.getText();
					s3Bucket = s3BucketText.getText();
					closeDialog();
				}
			}
			else if (source == dirSelectionButton) {
				pickDirectory(srcDirText);
			}
			else  {
				srcDir = null;
				s3Bucket = null;
				super.processButton(source);
			}
		}
		catch (Exception e) {
			throw new RuntimeException("Failed trying to open dialog!", e);
		}

		return true;
	}

	/* (non-Javadoc)
	 * @see org.thirdstreet.backup.gui.BaseDialog#createControls(org.eclipse.swt.widgets.Composite)
	 */
	@Override
	protected void createControls(Composite parent) {
		Composite container = createContainer(parent, 1);
		
		// now add our fields
		Composite srcDirContainer = createContainer(container, 2);
		srcDirText = createText(srcDirContainer, 350, 20, "Src Dir:");
		dirSelectionButton = createButton(srcDirContainer, "Select");
		
		s3BucketText = createText(container, 350, 20, "S3 Bucket:");
		
		// now add our buttons
		Composite buttonContainer = createContainer(container, 2);
		okButton = createButton(buttonContainer, "Ok");
		addCloseButton(buttonContainer, "Cancel");
	}

	/* (non-Javadoc)
	 * @see org.thirdstreet.backup.gui.BaseDialog#getTitle()
	 */
	@Override
	protected String getTitle() {		
		return "Add AWS Item to Copy";
	}

	/**
	 * Gets the srcDir
	 * @return the srcDir
	 */
	public String getSrcDir() {
		return srcDir;
	}

	/**
	 * Gets the s3Bucket
	 * @return the s3Bucket
	 */
	public String getS3Bucket() {
		return s3Bucket;
	}


}
