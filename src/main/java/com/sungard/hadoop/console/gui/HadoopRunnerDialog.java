/*
 * Copyright 2008 by RBC Capital Markets Inc., All rights reserved.
 * 
 * This source code, and resulting software, is confidential and proprietary
 * information and is the intellectual property of RBC Capital Markets Inc.
 * 
 * You shall not disclose such Proprietary Information and shall use it only in
 * accordance with the terms and conditions of any and all license agreements
 * you have entered into with The Company.
 */
package com.sungard.hadoop.console.gui;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileWriter;
import java.io.InputStreamReader;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.widgets.ProgressBar;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import com.amazonaws.services.s3.model.ProgressEvent;
import com.amazonaws.services.s3.model.ProgressListener;
import com.sungard.common.utils.AWSEMRUtils;
import com.sungard.common.utils.AWSS3Utils;
import com.sungard.common.utils.IRemoteMessageReceiver;
import com.sungard.common.utils.RemoteMessageHandler;
import com.sungard.common.utils.AWSEMRUtils.AWSEMRListener;
import com.sungard.hadoop.console.profile.HadoopConfig;
import com.sungard.hadoop.console.profile.HadoopConfigFactory;
import com.sungard.hadoop.console.profile.HadoopJob;

/**
 * Dialog box that is used to display the backup process
 * 
 * @author jbramlet
 */
/**
 *
 * @author jbramlett
 */
public class HadoopRunnerDialog extends BaseDialog implements IRemoteMessageReceiver, AWSEMRListener, ProgressListener {
    private static final Log logger = LogFactory.getLog(HadoopRunnerDialog.class);

    private static final String kLineSeparator = System.getProperty("line.separator");

    private static final int kProgressMin = 0;
    private static final int kProgressMax = 10;

    protected List statusList;
    protected List jobList;

    protected boolean cancelled = false;
    protected boolean backupComplete = false;
    protected ProgressBar progressBar;
    protected Text progressLabel;
    protected HadoopJob activeProfile;
    protected Button saveOutput;
    protected StringBuffer jobOutput = new StringBuffer();
    protected long bytesCopied = 0;
    
    /**
     * Constructor
     * 
     * @param shell
     * @param profiles
     */
    public HadoopRunnerDialog(Shell shell, HadoopJob profile) {
        super(shell);
        this.activeProfile = profile;
    }


    /*
     * (non-Javadoc)
     * 
     * @see
     * org.thirdstreet.backup.gui.BaseDialog#createControls(org.eclipse.swt.
     * widgets.Composite)
     */
    @Override
    protected void createControls(Composite parent) {
        Composite container = createContainer(parent, 1);

        Group backupGroup = createGroup(container, activeProfile.getName() + " Job Status");

        // now add our list box for events
        // statusList = createList(backupGroup, 300, 200);
        progressBar = new ProgressBar(backupGroup, /* SWT.INDETERMINATE */
        SWT.HORIZONTAL);
        progressBar.setMinimum(kProgressMin);
        progressBar.setMaximum(kProgressMax);
        progressBar.setLayoutData(createGridData(400, 0));
        progressLabel = createText(backupGroup, 400, 0);
        progressLabel.setBackground(backupGroup.getBackground());
        progressLabel.setEnabled(true);
        progressLabel.setEditable(false);
        new Thread(new Runnable() {
            public void run() {
                try {
                    int progress = kProgressMin;
                    while (!cancelled && !backupComplete) {
                        if (progress++ > kProgressMax)
                            progress = kProgressMin;
                        moveProgress(progress);

                        TimeUnit.MILLISECONDS.sleep(500);
                    }
                }
                catch (Exception e) {
                    logger.error(e);
                }
            }
        }, "ProgressBar").start();

        // create our table
        this.createLabel(backupGroup, "Driver Output");
        statusList = this.createList(backupGroup, 810, 200);
        saveOutput = this.createButton(backupGroup, "Save Output");
        saveOutput.setEnabled(false);
        
        // create our table        
        this.createLabel(backupGroup, "Job Status Updates");
        jobList = this.createList(backupGroup, 810, 200);
        RemoteMessageHandler.getInstance().addReceiver(this);
		
        // add a disabled close button
        addCloseButton(container, "Close", 75);
        closeButton.setEnabled(false);
        
        if (activeProfile.isRunOnAWS()) {
            startAWSJob();
        }
        else {
            startHadoopJob();
        }
    }

    /**
     * Starts a job on AWS
     */
    protected void startAWSJob() {
    	final ProgressListener listener = this;
    	final AWSEMRListener emrListener = this;
        new Thread(new Runnable() {
            public void run() {
                final HadoopConfig hConf = new HadoopConfigFactory().getConfig();
                
                // first see if we need to upload our data
                String s3JarDir = hConf.getAwsJarDir();        
                File jarFile = new File(activeProfile.getJarFile());
                final String s3Jar = s3JarDir + "/" + jarFile.getName();        
                
                AWSS3Utils s3Utils = new AWSS3Utils(hConf.getAwsAccessId(), hConf.getAwsSecretKey());

                // see if we need to upload our jar
                if (activeProfile.isAwsAlwaysCopy()) {
                    updateDriverProgressTable("Removing " + jarFile.getName() + " from s3 bucket " + s3JarDir);
                    if (s3Utils.delete(s3JarDir,  jarFile.getName())) {
                    	updateDriverProgressTable("Successfully removed " + jarFile.getName() + " from s3 bucket " + s3JarDir);
                    }
                }

                // now copy our jar if it doesn't exist
                if (!s3Utils.exists(s3JarDir,  jarFile.getName())) {
                    updateDriverProgressTable("Uploading jar " + jarFile.getName() + " to " + s3JarDir);
                	if (s3Utils.copy(jarFile, s3JarDir, listener)) {
                		updateDriverProgressTable(jarFile.getName() + " uploaded to " + s3JarDir);	
                	}
                	else {
                        updateDriverProgressTable("Failed to upload jar! Process ending.");
                        jobComplete();                		
                	}
                }
                else {
                	updateDriverProgressTable(jarFile.getName() + " already in " + s3JarDir + " - local version will not be copied");
                }
                
                // handle our input data
                updateDriverProgressTable("Checking input data");
                File inputDataDir = new File(activeProfile.getInputDataDir());
                copyFilesToAWS(inputDataDir, activeProfile.getS3Dir(), s3Utils);
                updateDriverProgressTable("Input data copied");
                                                
                // now see if we have other files to upload
                updateDriverProgressTable("Checking additional items");
                if (activeProfile.getAdditionalCopyItems() != null) {
                	Map<String, String> copyItems = activeProfile.getAdditionalCopyItems();
                	for (String ld : copyItems.keySet()) {
                		String s3Bucket = copyItems.get(ld);
                        updateDriverProgressTable("Verifying " + ld + " in to bucket " + s3Bucket);
                		File localDir = new File(ld);
                		copyFilesToAWS(localDir, s3Bucket, s3Utils);
                	}
                }
                updateDriverProgressTable("Completed additional item copy");
                
                // finally time to launch the job 
                updateDriverProgressTable("Launching job on AWS");
                AWSEMRUtils aws = new AWSEMRUtils(hConf.getAwsAccessId(), hConf.getAwsSecretKey(), hConf.getAwsLogUrl());                
                String args = activeProfile.getArgs();
                String[] argArray = args.split(" ");
                aws.runJob(activeProfile.getName(), s3Jar, activeProfile.getMainClass(), argArray, emrListener);
            }
        }, "AWSDriver").start();
    }
    
    /**
     * Copies all of the files in the given dir to our specified bucket
     * @param dir The directory we are copying
     * @param bucket The bucket we are copying to
     * @param s3Utils The s3 utils to use
     */
    protected void copyFilesToAWS(File dir, String bucket, AWSS3Utils s3Utils) {
        File[] inputDataFiles = dir.listFiles();
        for (File inputFile : inputDataFiles) {
        	if (inputFile.isFile()) {
	        	if (!s3Utils.exists(bucket, inputFile.getName()) || activeProfile.isAwsAlwaysCopy()) {
	        		updateDriverProgressTable("Copying " + inputFile.getName() + " to bucket " + bucket);
	        		s3Utils.copy(inputFile, bucket, this);
	        	}
	        	else {
	        		updateDriverProgressTable(inputFile.getName() + " already in bucket " + bucket + ", local file will not be copied.");
	        	}
        	}
        }    	
    }
    
    /* (non-Javadoc)
     * @see com.amazonaws.services.s3.model.ProgressListener#progressChanged(com.amazonaws.services.s3.model.ProgressEvent)
     */
    @Override
    public void progressChanged(ProgressEvent progressEvent) {
        bytesCopied = bytesCopied + progressEvent.getBytesTransfered();
        
        updateDriverProgressTable("Transferred " + humanReadableByteCount(bytesCopied));
    }          
    
    /**
     * Converts our bytes to a human readable form
     * @param bytes The bytes we need to convert
     * @return String a human readable representation
     */
    private String humanReadableByteCount(long bytes) {
        int unit = 1000;
        if (bytes < unit) return bytes + " B";
        int exp = (int) (Math.log(bytes) / Math.log(unit));
        String pre = ("kMGTPE").charAt(exp-1) + ("");
        return String.format("%.1f %sB", bytes / Math.pow(unit, exp), pre);
    }                    
    
    /* (non-Javadoc)
     * @see com.sungard.common.utils.AWSEMRUtils.AWSEMRListener#jobStarting(java.lang.String)
     */
    @Override
    public void jobStarting(String jobId) {
        updateJobStatusTable("Job " + jobId + " starting");        
    }


    /* (non-Javadoc)
     * @see com.sungard.common.utils.AWSEMRUtils.AWSEMRListener#jobComplete(java.lang.String)
     */
    @Override
    public void jobComplete(String jobId) {
        updateJobStatusTable("Job " + jobId + " complete");   
        jobComplete();
    }


    /* (non-Javadoc)
     * @see com.sungard.common.utils.AWSEMRUtils.AWSEMRListener#jobTerminated(java.lang.String)
     */
    @Override
    public void jobTerminated(String jobId) {
        updateJobStatusTable("Job " + jobId + " terminated");        
        jobComplete();
    }


    /* (non-Javadoc)
     * @see com.sungard.common.utils.AWSEMRUtils.AWSEMRListener#jobFailed(java.lang.String)
     */
    @Override
    public void jobFailed(String error) {
        updateJobStatusTable("Job failed! " + error);        
        jobComplete();
    }


    /* (non-Javadoc)
     * @see com.sungard.common.utils.AWSEMRUtils.AWSEMRListener#jobRunning(java.lang.String)
     */
    @Override
    public void jobRunning(String jobId) {
        updateJobStatusTable("Job " + jobId + " running");        
    }


    /* (non-Javadoc)
     * @see com.sungard.common.utils.AWSEMRUtils.AWSEMRListener#jobWaiting(java.lang.String)
     */
    @Override
    public void jobWaiting(String jobId) {
        updateJobStatusTable("Job " + jobId + " waiting");        
    }


    /* (non-Javadoc)
     * @see com.sungard.common.utils.AWSEMRUtils.AWSEMRListener#jobShuttingDown(java.lang.String)
     */
    @Override
    public void jobShuttingDown(String jobId) {
        updateJobStatusTable("Job " + jobId + " shutting down");        
    }


    /* (non-Javadoc)
     * @see com.sungard.common.utils.AWSEMRUtils.AWSEMRListener#jobBootstrapping(java.lang.String)
     */
    @Override
    public void jobBootstrapping(String jobId) {
        updateJobStatusTable("Job " + jobId + " bootstrapping");        
    }


    /**
     * Starts our job on Hadoop
     */
    protected void startHadoopJob() {
        try {
            logger.info("Starting Process....");
            ProcessBuilder processBuilder = new ProcessBuilder(activeProfile.getProcessCommand());
            final Process process = processBuilder.start();
    
            new Thread(new Runnable() {
                public void run() {
                    try {
                        logger.info("Capturing input stream...");
                        InputStreamReader stdout = new InputStreamReader(process.getInputStream());
    
                        BufferedReader reader = new BufferedReader(stdout);
    
                        String line = reader.readLine();
                        while ((line != null) && (!backupComplete)) {
                           updateDriverProgressTable(line);
                           line = reader.readLine();
                        }
                        logger.info("Closing our input stream");
                        reader.close();
                    }
                    catch (Exception e) {
                        logger.error(e);
                    }
                }
            }, "InputStreamReader").start();
            new Thread(new Runnable() {
                public void run() {
                    try {
                        logger.info("Capturing error stream...");
                        InputStreamReader stderr = new InputStreamReader(process.getErrorStream());
    
                        BufferedReader reader = new BufferedReader(stderr);
    
                        String line = reader.readLine();
                        while ((line != null) && (!backupComplete)) {
                           updateDriverProgressTable(line);
                           line = reader.readLine();
                        }
                        logger.info("Closing our error stream");
                        reader.close();
                    }
                    catch (Exception e) {
                        logger.error(e);
                    }
                }
            }, "ErrorStreamReader").start();
    
            new Thread(new Runnable() {
                public void run() {
                    try {
                        logger.info("Waiting for job to complete...");
                        process.waitFor();
                        jobComplete();
                    }
                    catch (Exception e) {
                        logger.error(e);
                    }
                }
            }, "JobStatus").start();
        }
        catch (Exception e) {
            logger.error("Failed to start job!", e);
            showMessageBox("Failed to start job!" + e.getMessage(), "Error Starting Job");
            closeButton.setEnabled(true);
        }                
    }
    
    
    /* (non-Javadoc)
	 * @see org.thirdstreet.backup.gui.BaseDialog#processButton(java.lang.Object)
	 */
	@Override
	protected boolean processButton(Object source) {
		boolean processed = super.processButton(source);

		try {			
			// if we didn't process this button in our super class then
			// process it here
			if (!processed) {
				// figure out what dialog to display for the button
				if (source == saveOutput) {
					saveOutput();
				}
			}
		}
		catch (Exception e) {
			throw new RuntimeException("Failed trying to open dialog!", e);
		}

		return processed;
	}


	/**
     * Signals our job is done
     */
    protected void jobComplete() {
    	backupComplete = true;
    	logger.info("Job complete...");
    	GUIManager.display.syncExec(new Runnable() {
            public void run() {
                if (!closeButton.isDisposed()) {
                	closeButton.setEnabled(true);
            	}
            	if (!saveOutput.isDisposed()) {
            		saveOutput.setEnabled(true); 
        		}
            }
        	});    	
    }
    
    /*
     * (non-Javadoc)
     * 
     * @see org.thirdstreet.backup.gui.BaseDialog#getTitle()
     */
    @Override
    protected String getTitle() {
		return "Running Hadoop Job";
    }

    /**
     * Utility to move our progress bar a certain amount
     * 
     * @param pos The new progress selection
     */
    private void moveProgress(final int pos) {
        GUIManager.display.syncExec(new Runnable() {
            public void run() {
                if (!progressBar.isDisposed()) {
                    progressBar.setSelection(pos);
                    progressBar.redraw();
                }
            }
        });
    }

    /**
     * Updates the driver progress table
     * 
     * @param msg The new message
     */
    private void updateDriverProgressTable(final String operation) {
    	logger.info(operation);
    	jobOutput.append(operation + kLineSeparator);
        GUIManager.display.asyncExec(new Runnable() {
            public void run() {
                // if (!statusList.isDisposed()) {
                if (!statusList.isDisposed()) {
                	statusList.add(operation);
                	
                    // Scroll to the bottom
                    statusList.select(statusList.getItemCount() - 1);
                    statusList.showSelection();                    
                }
            }
        });

    }
    
    /**
     * Updates our job status output
     * 
     * @param msg The new message
     */    
    private void updateJobStatusTable(final String operation) {
    	logger.info(operation);
        GUIManager.display.asyncExec(new Runnable() {
            public void run() {
                // if (!statusList.isDisposed()) {
                if (!jobList.isDisposed()) {
                	jobList.add(operation);
                	
                    // Scroll to the bottom
                    jobList.select(jobList.getItemCount() - 1);
                    jobList.showSelection();                    
                }
            }
        });

    }
    /* (non-Javadoc)
     * @see com.sungard.common.utils.IRemoteMessageReceiver#updateMessage(java.lang.String)
     */
    @Override
    public void updateMessage(String message) {
        updateJobStatusTable(message);        
    }
    
    
    /**
     * Saves our output to a file
     */
    protected void saveOutput() {
    	// now that we have our string, get a file selection from the user
		FileDialog dlg = new FileDialog(shell, SWT.SAVE);
		String filename = dlg.open();
		if (filename != null) {
			try {
				FileWriter fw = new FileWriter(filename);
				fw.write(jobOutput.toString());
				fw.close();
			}
			catch (Exception e) {
				logger.error("Failed to save output from job run!", e);
			}			
		}
    }
	/* (non-Javadoc)
	 * @see com.sungard.hadoop.console.gui.BaseDialog#closeDialog()
	 */
	@Override
	protected void closeDialog() {
		RemoteMessageHandler.getInstance().removeReceiver(this);
		super.closeDialog();
	}    

}
