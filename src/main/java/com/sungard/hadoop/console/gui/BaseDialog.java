/*
 * Copyright 2008 by RBC Capital Markets Inc., All rights reserved.
 * 
 * This source code, and resulting software, is confidential and proprietary
 * information and is the intellectual property of RBC Capital Markets Inc.
 * 
 * You shall not disclose such Proprietary Information and shall use it only in
 * accordance with the terms and conditions of any and all license agreements
 * you have entered into with The Company.
 */
package com.sungard.hadoop.console.gui;


import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.events.ShellEvent;
import org.eclipse.swt.events.ShellListener;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Dialog;
import org.eclipse.swt.widgets.DirectoryDialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Layout;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Monitor;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.Tree;


/**
 * The base dialog for any dialog box we need to show - just
 * provides some routines to make creating the dialog
 * easier 
 *
 * @author jbramlet
 */
public abstract class BaseDialog extends Dialog implements SelectionListener, ShellListener {
	private static final Log logger = LogFactory.getLog(BaseDialog.class);

	protected static final int kFactor = 8;
	protected static final int kDayStart = 1;
	protected static final int kDayEnd = 2;

	public Shell shell;
	protected Button closeButton;

	/**
	 * Constructor
	 * 
	 * @param shell The shell we are executing in
	 */
	public BaseDialog(Shell shell) {
		super(shell);
	}

	/**
	 * Constructor
	 * 
	 * @param shell The owning shell
	 * @param styles The dialog style
	 */
	public BaseDialog(Shell shell, int styles) {
		super(shell, styles);
	}

	/**
	 * Gets the dialog title
	 * 
	 * @return String The dialog title
	 */
	protected abstract String getTitle();

	/**
	 * Opens the dialog
	 * 
	 * @return The result of the open operation
	 */
	public int open() {
		shell = getShell();

		shell.setText(getTitle());
		
		FillLayout layout = new FillLayout();
		layout.type = SWT.VERTICAL;
		shell.setLayout(layout);

		// now create our controls
		createControls(shell);
		
		// now calculate our size based on the controls
		shell.setSize(shell.computeSize(SWT.DEFAULT, SWT.DEFAULT));     

		// now initialize our display
		initDisplay(shell);

		// now center on the display
		Monitor primary = GUIManager.display.getPrimaryMonitor();
		Rectangle bounds = primary.getBounds();
		Rectangle rect = shell.getBounds();
		int x = bounds.x + (bounds.width - rect.width) / 2;
		int y = bounds.y + (bounds.height - rect.height) / 2;
		shell.setLocation (x, y);

		
		// now display our dialog
		shell.open();
		Display display = getParent().getDisplay();

		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}

		return SWT.OK;
	}

	/**
	 * Initialize any values we can't initialize while creating our controls
	 */
	protected void initDisplay(Composite shell) {
		// by default do nothing
		;
	}
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.swt.events.ShellListener#shellActivated(org.eclipse.swt.events.ShellEvent)
	 */
	public void shellActivated(ShellEvent arg0) {
		// nothing to do here
		;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.swt.events.ShellListener#shellClosed(org.eclipse.swt.events.ShellEvent)
	 */
	public void shellClosed(ShellEvent arg0) {
		// now restore our parent
		restoreParent();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.swt.events.ShellListener#shellDeactivated(org.eclipse.swt.events.ShellEvent)
	 */
	public void shellDeactivated(ShellEvent arg0) {
		// no default behavior
		;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.swt.events.ShellListener#shellDeiconified(org.eclipse.swt.events.ShellEvent)
	 */
	public void shellDeiconified(ShellEvent arg0) {
		// nothing to do here
		;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.swt.events.ShellListener#shellIconified(org.eclipse.swt.events.ShellEvent)
	 */
	public void shellIconified(ShellEvent arg0) {
		// nothing to do here
		;
	}

	/**
	 * Restores our parent shell to regular state in case it has been minimized
	 * 
	 */
	protected void restoreParent() {
		// if we have a valid parent shell then make sure it is
		// restored to normal (i.e. not minimized)
		if (!getParent().isDisposed()) {
			getParent().setMinimized(false);
		}
	}

	/**
	 * Minimizes this dialog
	 * 
	 */
	protected void minimize() {
		shell.setMinimized(true);
	}

	/**
	 * Gets our shell (or creates a new one) - this is used to manage both our
	 * main window and child windows
	 * 
	 * @return Shell The shell
	 */
	protected Shell getShell() {
		Shell parent = getParent();
		shell = new Shell(parent, SWT.DIALOG_TRIM | SWT.MIN);
		shell.addShellListener(this);
		return shell;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.swt.events.SelectionListener#widgetDefaultSelected(org.eclipse.swt.events.SelectionEvent)
	 */
	public void widgetDefaultSelected(SelectionEvent arg0) {
		widgetSelected(arg0);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.swt.events.SelectionListener#widgetSelected(org.eclipse.swt.events.SelectionEvent)
	 */
	public void widgetSelected(SelectionEvent arg0) {
		// now process the button
		processButton(arg0.getSource());
	}

	/**
	 * Processes a button selection
	 * 
	 * @param source The button being pressed
	 * @return boolean returns true if the button was processed
	 */
	protected boolean processButton(Object source) {
		boolean processed = false;

		// if the user is closing the window
		if (source == closeButton) {
			processed = true;
			closeDialog();
		}

		return processed;
	}

	/**
	 * Closes our dialog
	 * 
	 */
	protected void closeDialog() {
		shell.close();
	}

	/**
	 * Notifies the user when the process is complete
	 * 
	 */
	public void notifyComplete() {
		showMessageBox("Process complete", "Run Extract");
	}

	/**
	 * Create the controls to display on this dialog
	 * 
	 * @param parent
	 */
	protected abstract void createControls(Composite parent);

	/**
	 * Adds our close button
	 * 
	 * @param container the container to add the button to
	 */
	protected void addCloseButton(Composite container) {
		// add our close button
		closeButton = createButton(container, "Close", this, 0);
	}

	/**
	 * Adds our close button
	 * 
	 * @param container the container to add the button to
	 * @param text The button text
	 */
	protected void addCloseButton(Composite container, String text) {
		// add our close button
		closeButton = createButton(container, text, this, 0);
	}

	/**
	 * Adds our close button
	 * 
	 * @param container the container to add the button to
	 * @param text The button text
	 */
	protected void addCloseButton(Composite container, String text, int width) {
		// add our close button
		closeButton = createButton(container, text, this, width);
	}

	/**
	 * Enables or disables a button with null checking
	 * 
	 * @param button The button we are setting the state on
	 * @param enabled Set to true if the button should be enabled, false
	 *            otherwise
	 */
	protected void enableButton(Button button, boolean enabled) {
		if (button != null) {
			button.setEnabled(enabled);
		}
	}

	/**
	 * Throws up a message box with an error message
	 * 
	 * @param e The exception
	 */
	public void alertError(Throwable e) {
		logger.error("Failed with exception!", e);
		alertError("Exception: " + e.getClass().getName() + " -> " + e.getMessage());
	}

	/**
	 * Throws up a message box with an error message
	 * 
	 * @param e The exception
	 */
	public void alertError(String message) {
		MessageBox mb = new MessageBox(GUIManager.shell, SWT.ICON_INFORMATION | SWT.OK);
		mb.setMessage(message);
		mb.setText("Error");
		mb.open();
	}

	/**
	 * Alert our users that the process is complete
	 * 
	 * @param message The message to display
	 * @param title The message box title
	 */
	public void alertComplete(String message, String title) {
		showMessageBox(message, title);
	}

	/**
	 * Shows a message box
	 * 
	 * @param message The message for the message box
	 * @param title The title for the message box
	 */
	protected void showMessageBox(String message, String title) {
		MessageBox mb = new MessageBox(shell, SWT.ICON_INFORMATION | SWT.OK);
		mb.setMessage(message);
		mb.setText(title);
		mb.open();
	}

	/**
	 * Shows a message box asking for confirmation
	 * 
	 * @param message The message for the message box
	 * @param title The title for the message box
	 * @return boolean Returns true if the user confirms, false if they do not
	 */
	protected boolean showConfirmationBox(String message, String title) {
		MessageBox mb = new MessageBox(shell, SWT.ICON_INFORMATION | SWT.YES | SWT.NO);
		mb.setMessage(message);
		mb.setText(title);
		return mb.open() == SWT.YES;
	}


	/**
	 * Creates a new button for our set of actions
	 * 
	 * @param parent The parent on which this button will live
	 * @param label The button label
	 * @return Button The newly created button
	 */
	protected Button createButton(Composite parent, String label) {
		return createButton(parent, label, this, 150);
	}

	/**
	 * Creates a new button for our set of actions
	 * 
	 * @param parent The parent on which this button will live
	 * @param label The button label
	 * @param listener The listener for the button
	 * @return Button The newly created button
	 */
	protected Button createButton(Composite parent, String label, SelectionListener listener, int width) {
		Button result = null;

		// create a new composite for this button
		Composite container = createContainer(parent, 1);

		// now create the actual button
		result = new Button(container, SWT.PUSH);
		result.setText(label);
		// add our listener
		if (listener != null) {
			result.addSelectionListener(listener);
		}

		// now size the button if we have a width
		if (width > 0) {
			result.setLayoutData(createGridData(width, 0));
		}
		return result;
	}

	/**
	 * Creates a new group
	 * 
	 * @param parent The parent for the group
	 * @param label The label for the group
	 * @return Group the new group
	 */
	protected Group createGroup(Composite parent, String label) {
		// group our new group
		Group group = new Group(parent, SWT.NONE);
		group.setText(label);

		// now create our layout
		group.setLayout(createLayout(1));

		// now size the group
		group.setLayoutData(createGridData(0, 0));

		return group;
	}

	/**
	 * Create a new container
	 * 
	 * @param parent The parent one which this new container is going
	 * @param columns The number of columns on this new container
	 * @return Composite The new container
	 */
	protected Composite createContainer(Composite parent, int columns) {
		// create our container
		Composite container = new Composite(parent, SWT.NULL);

		// now create our layour
		container.setLayout(createLayout(columns));

		return container;
	}

    /**
     * Create a new container
     * 
     * @param parent The parent one which this new container is going
     * @param columns The number of columns on this new container
     * @param width The suggested width of the container
     * @param height The suggested height of the container
     * @return Composite The new container
     */
    protected Composite createContainer(Composite parent, int columns, int width, int height) {
        // create our container
        Composite container = new Composite(parent, SWT.NULL);

        container.setLayout(createLayout(columns));

        // now create our layout
        // if we have a width or height hint specified then set our layout data
        if ((width > 0) || (height > 0)) {
            container.setLayoutData(createGridData(width, height));
        }

        return container;
    }

    /**
	 * Creates a new label on the given parent
	 * 
	 * @param parent The parent we are creating this label on
	 * @param text The label text
	 * @return Label The label
	 */
	protected Label createLabel(Composite parent, String text) {
		// create our label
		Label label = new Label(parent, SWT.NONE);
		label.setText(text);

		return label;
	}

    /**
     * Creates a new label on the given parent
     * 
     * @param parent The parent we are creating this label on
     * @param text The label text
     * @param width The hint for the width of the text field
     * @param heigth The hint for the height of the field
     * @return Label The label
     */
    protected Label createLabel(Composite parent, String text, int width, int height) {
        // create our label
        Label label = new Label(parent, SWT.NONE);
        label.setText(text);

        // if we have a width or height hint specified then set our layout data
        if ((width > 0) || (height > 0)) {
            label.setLayoutData(createGridData(width, height));
        }

        
        return label;
    }

    /**
	 * Creates on combo box on a given parent - the combo box will be proceeded
	 * by the label text
	 * 
	 * @param parent The parent container
	 * @param label The label for our combo box
	 * @param data The data for the combo box
	 * @return Combo The combo box
	 */
	protected Combo createComboBox(Composite parent, String label, String[] data) {
		// create our composite
		Composite container = createContainer(parent, 2);

		// now create our label
		createLabel(container, label);

		// and now our combo box
		Combo cb = new Combo(container, SWT.DROP_DOWN | SWT.READ_ONLY);

		if (data != null) {
			for (int i = 0; i < data.length; i++) {
				cb.add(data[i]);
			}
		}
		return cb;
	}

	/**
	 * Creates new grid data
	 * 
	 * @param width The hint for the width
	 * @param heigth The hint for the heigth
	 * @return GridDate The new grid data
	 */
	protected GridData createGridData(int width, int heigth) {
		// create our new grid data
		GridData gridData = new GridData();
		gridData.horizontalAlignment = GridData.FILL;
		gridData.verticalAlignment = GridData.FILL;
		gridData.grabExcessHorizontalSpace = true;
		gridData.grabExcessVerticalSpace = true;

		// if we have a width specified then set the hint
		if (width > 0) {
			gridData.widthHint = width;
		}

		// if the heigth is specified then set the hint
		if (heigth > 0) {
			gridData.heightHint = heigth;
		}
		return gridData;
	}

	/**
	 * Creates a new layout with the given number of columns
	 * 
	 * @param columns The number of columns in the layout
	 * @return Layout The new layout
	 */
	protected Layout createLayout(int columns) {
		// now create our layour
		GridLayout layout = new GridLayout();
		layout.numColumns = columns;
		layout.verticalSpacing = 0;
		layout.marginTop = 2;
		layout.marginBottom = 2;
		layout.marginHeight = 2;

		return layout;
	}

	/**
	 * Creates a new radio button
	 * 
	 * @param parent The parent for the new button
	 * @param label The button label
	 * @return Button The new button
	 */
	protected Button createRadioButton(Composite parent, String label) {
		// create our new radio button
		Button result = new Button(parent, SWT.RADIO);
		result.setText(label);

		return result;
	}

	/**
	 * Creates a new checkbox
	 * 
	 * @param parent The parent for the new button
	 * @param label The button label
	 * @return Button The new button
	 */
	protected Button createCheckbox(Composite parent, String label) {
		// create our new radio button
		Button result = new Button(parent, SWT.CHECK);
		result.setText(label);

		return result;
	}
	
	/**
	 * Creates a new flat button
	 * 
	 * @param parent The parent for the new button
	 * @param label The button label
	 * @return Button The new button
	 */
	protected Button createFlatButton(Composite parent, String label) {
		Composite buttonComposite = createContainer(parent, 2);
		// create our new radio button
		Button result = new Button(buttonComposite, SWT.FLAT);
		createLabel(buttonComposite, label);

		return result;
	}	

	/**
	 * Creates a new text field with the given width as a hint
	 * 
	 * @param parent The parent for this text field
	 * @param width The hint for the width of the text field
	 * @param heigth The hint for the height of the field
	 * @return Text The new field
	 */
	protected Text createText(Composite parent, int width, int height) {
		Text result = new Text(parent, SWT.NONE);

		// if we have a width or height hint specified then set our layout data
		if ((width > 0) || (height > 0)) {
			result.setLayoutData(createGridData(width, height));
		}

		return result;
	}

	/**
	 * Creates a new multi-line text field with the given width as a hint
	 * 
	 * @param parent The parent for this text field
	 * @param width The hint for the width of the text field
	 * @param heigth The hint for the height of the field
	 * @return Text The new field
	 */
	protected Text createMultiText(Composite parent, int width, int height) {
		Text result = new Text(parent, SWT.MULTI | SWT.V_SCROLL | SWT.H_SCROLL);

		// if we have a width or height hint specified then set our layout data
		if ((width > 0) || (height > 0)) {
			result.setLayoutData(createGridData(width, height));
		}

		return result;
	}
	/**
	 * Creates a new text field with the given width as a hint
	 * 
	 * @param parent The parent for this text field
	 * @param width The hint for the width of the text field
	 * @param heigth The hint for the height of the field
	 * @param label The label for the text field
	 * @return Text The new field
	 */
	protected Text createText(Composite parent, int width, int heigth, String label) {
		// start by creating a container to hold the label and text field
		Composite container = createContainer(parent, 2);

		// now create our label
		createLabel(container, label);

		// now we can create the text field
		Text result = new Text(container, SWT.NONE);

		// if we have a width or height hint specified then set our layout data
		if ((width > 0) || (heigth > 0)) {
			result.setLayoutData(createGridData(width, heigth));
		}

		return result;
	}

	/**
	 * Creates a new text field with the given width as a hint
	 * 
	 * @param parent The parent for this text field
	 * @param width The hint for the width of the text field
	 * @param heigth The hint for the height of the field
	 * @param label The label for the text field
	 * @return Text The new field
	 */
	protected Text createMultiText(Composite parent, int width, int heigth, String label) {
		// start by creating a container to hold the label and text field
		Composite container = createContainer(parent, 2);

		// now create our label
		createLabel(container, label);

		// now we can create the text field
		Text result = new Text(container, SWT.MULTI);

		// if we have a width or height hint specified then set our layout data
		if ((width > 0) || (heigth > 0)) {
			result.setLayoutData(createGridData(width, heigth));
		}

		return result;
	}

	/**
	 * Creates a new table field with the given width as a hint
	 * 
	 * @param parent The parent for this table field
	 * @param gridLines Boolean flag indicating if we should have grid lines
	 *            displayed
	 * @param width The hint for the width of the table field
	 * @param heigth The hint for the height of the field
	 * @return Table The new field
	 */
	protected Table createTable(Composite parent, boolean gridLines, int width, int heigth) {
		// create a composite for our table - seems to be the only
		// way to get the sizing to work
		Composite tableContainer = createContainer(parent, 1);

		Table table = new Table(tableContainer, SWT.NONE);
		table.setLayoutData(createGridData(width, heigth));

		// set our flag on grid lines

		table.setLinesVisible(gridLines);
		table.setHeaderVisible(gridLines);

		return table;

	}

	/**
	 * Creates a new list field with the given width as a hint
	 * 
	 * @param parent The parent for this table field
	 * @param width The hint for the width of the table field
	 * @param heigth The hint for the height of the field
	 * @return Table The new field
	 */
	protected List createList(Composite parent, int width, int heigth) {
		// create a composite for our table - seems to be the only
		// way to get the sizing to work
		Composite tableContainer = createContainer(parent, 1);

		List list = new List(tableContainer, SWT.V_SCROLL | SWT.H_SCROLL | SWT.BORDER);
		list.setLayoutData(createGridData(width, heigth));

		return list;

	}

	/**
	 * Creates a new table field with the given width as a hint
	 * 
	 * @param parent The parent for this table field
	 * @param gridLines Boolean flag indicating if we should have grid lines
	 *            displayed
	 * @param width The hint for the width of the table field
	 * @param heigth The hint for the height of the field
	 * @return Table The new field
	 */
	protected Table createMultiTable(Composite parent, boolean gridLines, int width, int heigth) {
		// create a composite for our table - seems to be the only
		// way to get the sizing to work
		Composite tableContainer = createContainer(parent, 1);

		Table table = new Table(tableContainer, SWT.MULTI | SWT.H_SCROLL);
		table.setLayoutData(createGridData(width, heigth));

		// set our flag on grid lines

		table.setLinesVisible(gridLines);
		table.setHeaderVisible(gridLines);

		return table;

	}

	/**
	 * Creates a new column in our table
	 * 
	 * @param name The column header
	 * @param index The index for the new column
	 * @param size The size or -1 if should use the field name size
	 */
	protected void createColumn(Table table, String name, int size) {
		// create our new column
		TableColumn c = new TableColumn(table, SWT.LEFT);
		c.setText(name);

		// if we don't have a width specified then default to the column size
		if (size == -1) {
			c.setWidth(name.length() * kFactor);
		}
		// otherwise use the size provided
		else {
			c.setWidth(size);
		}
	}
	
	/**
	 * Creates a new tree field with the given width as a hint
	 * 
	 * @param parent The parent for this table field
	 * @param width The hint for the width of the table field
	 * @param heigth The hint for the height of the field
	 * @return Tree The new field
	 */
	protected Tree createTree(Composite parent, int width, int heigth) {
		// create a composite for our table - seems to be the only
		// way to get the sizing to work
		Composite tableContainer = createContainer(parent, 1);

		Tree tree = new Tree(tableContainer, SWT.SINGLE | SWT.V_SCROLL | SWT.H_SCROLL | SWT.BORDER);
		tree.setLayoutData(createGridData(width, heigth));

		return tree;
	}
	
	/**
	 * Allows the user to pick a directory as part of our config
	 * @param txtField The text field to populate with the result
	 * 
	 */
	protected void pickDirectory(Text txtField) {
		String dir = pickDirectory(txtField.getText());
		if (dir != null) {
			// now set this in our field
			txtField.setText(dir);
		}
	}	
		
	/**
	 * Picks a directory returning the selected dir
	 * @param initialDir The initial directory for the dialog
	 * @return String The selected directory
	 */
	protected String pickDirectory(String initialDir) {
		DirectoryDialog dlg = new DirectoryDialog(shell, SWT.OPEN);
		dlg.setFilterPath(initialDir);
		return dlg.open();
	}

	/**
	 * Allows the user to pick a file as part of our config
	 * @param txtField The text field to populate with the result
	 * 
	 */
	protected void pickFile(Text txtField) {
		String file = pickFile(txtField.getText());
		if (file != null) {
			// now set this in our field
			txtField.setText(file);
		}
	}	
		
	/**
	 * Picks a directory returning the selected dir
	 * @param initialDir The initial directory for the dialog
	 * @return String The selected directory
	 */
	protected String pickFile(String initialFile) {
		FileDialog dlg = new FileDialog(shell, SWT.OPEN);
		dlg.setFilterPath(initialFile);
		return dlg.open();
	}
	
	
}
