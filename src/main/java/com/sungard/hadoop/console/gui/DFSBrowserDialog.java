/*
 * Copyright 2008 by RBC Capital Markets Inc., All rights reserved.
 * 
 * This source code, and resulting software, is confidential and proprietary
 * information and is the intellectual property of RBC Capital Markets Inc.
 * 
 * You shall not disclose such Proprietary Information and shall use it only in
 * accordance with the terms and conditions of any and all license agreements
 * you have entered into with The Company.
 */
package com.sungard.hadoop.console.gui;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.io.OutputStream;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.MenuEvent;
import org.eclipse.swt.events.MenuListener;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.DirectoryDialog;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeItem;

import com.sungard.hadoop.console.profile.HadoopConfig;
import com.sungard.hadoop.console.profile.HadoopConfigFactory;

/**
 * Allows you to browse DFS
 *
 * @author jbramlet
 */
public class DFSBrowserDialog extends BaseDialog {
	private static final Log logger = LogFactory.getLog(DFSBrowserDialog.class);
	
	private static final String kLineSeparator = System.getProperty("line.separator");
	protected HadoopConfig hConfig;
	protected Configuration config;
	protected FileSystem fileSystem;	
	protected Tree hadoopDir;
	protected Text fileContent;
	protected Button timeSeries;
	protected Button saveButton;
	protected Path selectedFile;
	protected Menu treeMenu;
	protected MenuItem timeSeriesGraphMenu;
    protected MenuItem marketDataGraphMenu;
	protected MenuItem deleteMenu;
	protected MenuItem copyFromLocalMenu;
	protected MenuItem copyToLocalMenu;
	protected MenuItem createFolderMenu;
	
	/**
	 * Constructor
	 * @param shell The shell
	 */
	public DFSBrowserDialog(Shell shell) {
		super(shell);
	}
	/**
	 * Constructor
	 * 
	 * @param shell The owning shell
	 * @param styles The dialog style
	 * @param profile The profile we are editing
	 */
	public DFSBrowserDialog(Shell shell, int styles) {
		super(shell, styles);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.jpmorgan.gcrm.basel.ui.BaseDialog#getTitle()
	 */
	protected String getTitle() {
		return "DFS Browser";
	}
	
	/**
	 * Create the controls to display in the dialog
	 */
	protected void createControls(Composite parent) {
	    // initialize our hadoop configuration
        HadoopConfigFactory configFactory = new HadoopConfigFactory();
        hConfig = configFactory.getConfig();           
        config = new Configuration();
        config.set("mapred.job.tracker", hConfig.getMapReduceMaster());
        config.set("fs.default.name", hConfig.getHdfsMaster());
        try {
        	fileSystem = FileSystem.get(config);
        }
        catch (Exception e) {
        	logger.error("Failed to initialize our file system!", e);
        }
		
	    
	    Composite container = createContainer(parent, 1);

		Composite display = createContainer(container, 2);

		// create a group box for our tree
		Group treeGroup = createGroup(display, "DFS");
		hadoopDir = createTree(treeGroup, 400, 350);
		
		// add a menu to the tree
		treeMenu = new Menu(hadoopDir);
		deleteMenu = new MenuItem(treeMenu, SWT.NONE);
		deleteMenu.setText("Delete");
		deleteMenu.addSelectionListener(new SelectionListener() {
			@Override
			public void widgetDefaultSelected(SelectionEvent arg0) {
				widgetSelected(arg0);
			}
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				deleteFromHDFS();
			}
			
		});

		createFolderMenu = new MenuItem(treeMenu, SWT.NONE);
		createFolderMenu.setText("Create Folder");
		createFolderMenu.addSelectionListener(new SelectionListener() {
			@Override
			public void widgetDefaultSelected(SelectionEvent arg0) {
				widgetSelected(arg0);
			}
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				createFolder();
			}
			
		});

		copyFromLocalMenu = new MenuItem(treeMenu, SWT.NONE);
		copyFromLocalMenu.setText("Copy From Local");
		copyFromLocalMenu.addSelectionListener(new SelectionListener() {
			@Override
			public void widgetDefaultSelected(SelectionEvent arg0) {
				widgetSelected(arg0);
			}
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				copyFromLocal();
			}			
		});
		
		copyToLocalMenu = new MenuItem(treeMenu, SWT.NONE);
		copyToLocalMenu.setText("Copy To Local");
		copyToLocalMenu.addSelectionListener(new SelectionListener() {
			@Override
			public void widgetDefaultSelected(SelectionEvent arg0) {
				widgetSelected(arg0);
			}
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				copyToLocal();
			}			
		});
		
		timeSeriesGraphMenu = new MenuItem(treeMenu, SWT.NONE);
		timeSeriesGraphMenu.setText("Time Series");
		timeSeriesGraphMenu.setEnabled(false);
		timeSeriesGraphMenu.addSelectionListener(new SelectionListener() {
			@Override
			public void widgetDefaultSelected(SelectionEvent arg0) {
				widgetSelected(arg0);
			}
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				FileStatus fileStatus = getSelectionData();
				if (fileStatus != null) {
					if (isValidForTimeSeries(fileStatus.getPath())) {
						showTimeSeries(fileStatus.getPath());
					}
				}				
			}
			
		});
		marketDataGraphMenu = new MenuItem(treeMenu, SWT.NONE);
		marketDataGraphMenu.setText("Graph Market Data");
		marketDataGraphMenu.setEnabled(false);
		marketDataGraphMenu.addSelectionListener(new SelectionListener() {
            @Override
            public void widgetDefaultSelected(SelectionEvent arg0) {
                widgetSelected(arg0);
            }
            @Override
            public void widgetSelected(SelectionEvent arg0) {
                FileStatus fileStatus = getSelectionData();
                if (fileStatus != null) {
                    if (isValidForMarketData(fileStatus.getPath())) {
                        showMarketData(fileStatus.getPath());
                    }
                }               
            }
            
        });
		
		treeMenu.addMenuListener(new MenuListener() {
			@Override
			public void menuHidden(MenuEvent arg0) {
				// do nothing;
			}
			@Override
			public void menuShown(MenuEvent arg0) {
				FileStatus fileStatus = getSelectionData();
				if (fileStatus != null) {
					timeSeriesGraphMenu.setEnabled(isValidForTimeSeries(fileStatus.getPath()));
					marketDataGraphMenu.setEnabled(isValidForMarketData(fileStatus.getPath()));
					copyToLocalMenu.setEnabled(!fileStatus.isDir());
					createFolderMenu.setEnabled(fileStatus.isDir());
					copyFromLocalMenu.setEnabled(fileStatus.isDir());
				}
			}			
		});
		
		hadoopDir.setMenu(treeMenu);
		

		try {			
			FileStatus[] roots = fileSystem.listStatus(new Path("/"));
			
	        for (int i=0; i < roots.length; i++) {
	        	TreeItem root = new TreeItem (hadoopDir, 0);
	            root.setText (roots[i].getPath().getName());
	            root.setData (roots[i]);
	            new TreeItem (root, 0);
	        }
	        hadoopDir.addListener (SWT.Expand, new Listener () {
	        	public void handleEvent (final Event event) {	        		
	        		final TreeItem root = (TreeItem) event.item;
	        		TreeItem [] items = root.getItems ();
	        		for (int i= 0; i<items.length; i++) {
	        			if (items [i].getData () != null) return;
	        			items [i].dispose ();
	        		}
	        		try {
		        		FileStatus file = (FileStatus) root.getData ();
		        		FileStatus [] files = fileSystem.listStatus(file.getPath());
		        		if (files == null) return;
		        		for (int i= 0; i < files.length; i++) {
		        			TreeItem item = new TreeItem (root, 0);
		        			item.setText (files[i].getPath().getName());
		        			item.setData (files [i]);
		        			if (files[i].isDir()) {
		        				new TreeItem (item, 0);
		        			}
		        		}
	        		}
	        		catch (Exception e) {
	        			logger.error("Failed expanding tree!", e);
	        		}
	        	}
	        });
	        hadoopDir.addSelectionListener(new SelectionListener() {
				public void widgetDefaultSelected(SelectionEvent arg0) {
					widgetSelected(arg0);
				}

				@Override
				public void widgetSelected(SelectionEvent event) {
	        		final TreeItem root = (TreeItem) event.item;
	        		FileStatus file = (FileStatus) root.getData();
	        		if (!file.isDir()) {
	        			displayFile(file.getPath());
	        		}
	        		else {
	        		    timeSeries.setEnabled(false);
	        		}
				}
	        });
		}
		catch (Exception e) {
			logger.error("Failed to build tree!", e);
		}
		
		// create a text for our display
		Group dataGroup = createGroup(display, "File Content");
		fileContent = createMultiText(dataGroup, 400, 250);
		fileContent.addModifyListener(new ModifyListener() {
			@Override
			public void modifyText(ModifyEvent arg0) {
				saveButton.setEnabled(true);
			}			
		});
		saveButton = createButton(dataGroup, "Save");
		saveButton.setEnabled(false);
				
		timeSeries = createButton(container, "Time Series");
		timeSeries.setEnabled(false);
		addCloseButton(container);
	}
	
	/**
	 * Handles our right menu click for deleting an item from the tree
	 */
	protected void deleteFromHDFS() {
		FileStatus fileStatus = getSelectionData();
		if (fileStatus != null) {
			String message = null;
			String title = null;
			
			if (fileStatus.isDir()) {
				message = "Are you sure you want to delete the folder\n" + fileStatus.getPath().toString() + "\nand all its contents?";
				title = "Delete Folder";				
			}
			else {
				message = "Are you sure you want to delete\n" + fileStatus.getPath().toString();
				title = "Delete File";				
			}
			if (showConfirmationBox(message, title)) {
				try {
					fileSystem.delete(fileStatus.getPath(), true);
					TreeItem selection = getSelection();
					selection.dispose();
				}
				catch (Exception e) {
					logger.error("Failed to delete " + fileStatus.getPath().toString(), e);
				}
			}
		}		
	}
	
	/**
	 * Copies the files in a directory from the local file system in to the selected
	 * folder in hdfs
	 */
	protected void copyFromLocal() {
		FileStatus fileStatus = getSelectionData();
		if (fileStatus != null) {
			DirectoryDialog dlg = new DirectoryDialog(shell, SWT.SAVE);
			dlg.setFilterPath(System.getProperty("user.home"));
			String selectedDir = dlg.open();
			if (selectedDir != null) {
				logger.info("Copying " + selectedDir + " contents to " + fileStatus.getPath().toString());
				try {
					fileSystem.copyFromLocalFile(new Path(selectedDir), fileStatus.getPath());

					// now rebuild our tree
					TreeItem root = getSelection();
					root.removeAll();
	        		FileStatus [] files = fileSystem.listStatus(fileStatus.getPath());
	        		if (files == null) return;
	        		for (int i= 0; i < files.length; i++) {
	        			TreeItem item = new TreeItem (root, 0);
	        			item.setText (files[i].getPath().getName());
	        			item.setData (files [i]);
	        			if (files[i].isDir()) {
	        				new TreeItem (item, 0);
	        			}
	        		}
					
				}
				catch(Exception e) {
					logger.error("Failed to copy dire contents from " + selectedDir + " to " + fileStatus.getPath().toString(), e);
				}
			}
		}				
		
	}
	
	/**
	 * Copies the selected file or directory to the local filesystem
	 */
	protected void copyToLocal() {
		FileStatus fileStatus = getSelectionData();
		if (fileStatus != null) {
			FileDialog dlg = new FileDialog(shell, SWT.SAVE);
			dlg.setFilterPath(System.getProperty("user.home"));
			String localFile = dlg.open();
			if (localFile != null) {
				logger.info("Copying " + fileStatus.getPath().toString() + " to " + localFile);
				try {
					fileSystem.copyToLocalFile(false, fileStatus.getPath(), new Path(localFile));
				}
				catch(Exception e) {
					logger.error("Failed to copy file " + fileStatus.getPath().toString() + " to " + localFile, e);
				}
			}
		}				
	}
	
	/**
	 * Creates a new folder in the tree
	 */
	protected void createFolder() {
		InputDialog inputDlg = new InputDialog(shell, "Enter folder to create", "Create New Folder", null);
		inputDlg.open();
		String input = inputDlg.getText();
		if ((input != null) && (input.length() > 0)) {
			FileStatus cs = getSelectionData();
			Path cp = cs.getPath();
			Path np = new Path(cp.toString() + File.separator + input);
			logger.info("Creating folder: " + np.toString());
			try {
				fileSystem.mkdirs(np);

				// if this worked then add a new item to the tree
				TreeItem ci = getSelection();
				TreeItem ni = new TreeItem(ci, 0);
				ni.setText (np.getName());
				ni.setData (fileSystem.getFileStatus(np));
				new TreeItem (ni, 0);
			}
			catch (Exception e) {
				logger.error("Failed to create new folder " + np.toString(), e);
			}			
		}
		
	}
	
	/**
	 * Gets the currently selected tree item
	 * @return TreeItem The current selection
	 */
	protected TreeItem getSelection() {
		TreeItem[] selection = hadoopDir.getSelection();
		if ((selection != null) && (selection.length > 0)) {
			return selection[0];
		}
		return null;	
	}

	/**
	 * Gets our selection from the tree
	 * @return FileStatus The selection from the tree
	 */
	protected FileStatus getSelectionData() {
		TreeItem selection = getSelection();
		if (selection != null) {
			return (FileStatus)selection.getData();
		}
		return null;
	}
	
	/**
	 * Returns a flag indicating if our given path is valid for showing a time series
	 * @param path The path we are verifying
	 * @return boolean Returns true if the given path is valid, false otherwise
	 */
	protected boolean isValidForTimeSeries(Path path) {
	    if (path.getName().startsWith("result")) {
	        if (path.toString().contains("raw")) {
	        	return true;
	        }
	    }
	    return false;
	}

	/**
     * Returns a flag indicating if our given path is valid for showing a a graph
     * of our market data
     * @param path The path we are verifying
     * @return boolean Returns true if the given path is valid, false otherwise
     */
    protected boolean isValidForMarketData(Path path) {
        if (path.getParent().toString().endsWith("market_data_environment")) {
            return true;
        }
        return false;
    }

    /**
	 * Display our file in our content box
	 * @param path
	 */
	protected void displayFile(Path path) {
		saveButton.setEnabled(false);
	    selectedFile = path;
		try {
		    if (path.getName().startsWith("result")) {
		        if (path.toString().contains("raw")) {
		            timeSeries.setEnabled(true);
		        }
		        else {
		            timeSeries.setEnabled(false);
		        }
		    }
		    
	        BufferedReader br=new BufferedReader(new InputStreamReader(fileSystem.open(path)));
	        StringBuffer file = new StringBuffer();
	        String line;
	        line=br.readLine();
	        while (line != null){
	        	file.append(line + kLineSeparator);
	        	line=br.readLine();
	        }
	        
	        // now update our display
	        fileContent.setText(file.toString());
		}
		catch (Exception e) {
			logger.error("Failed to load file " + path.toString() + " to display!", e);
		}
	}
	
    /* (non-Javadoc)
     * @see org.thirdstreet.backup.gui.BaseDialog#processButton(java.lang.Object)
     */
    @Override
    protected boolean processButton(Object source) {
        boolean processed = super.processButton(source);

        try {           
            // if we didn't process this button in our super class then
            // process it here
            if (!processed) {
                // figure out what dialog to display for the button
                if (source == timeSeries) {
                    showTimeSeries();
                }
                else if (source == saveButton) {
                	saveChanges();
                }
            }
        }
        catch (Exception e) {
            throw new RuntimeException("Failed trying to open dialog!", e);
        }

        return processed;
    }
    
    /**
     * Saves user changes to the given file back to HDFS
     */
    protected void saveChanges() {
    	try {
	        OutputStream os = fileSystem.create(selectedFile);
	        String newContent = fileContent.getText();
	        os.write(newContent.getBytes(), 0, newContent.getBytes().length);
	        os.close();
    	}
    	catch (Exception e) {
    		logger.error("Failed to write changes to file " + selectedFile.toString(), e);
    	}
    }
    /**
     * Shows the time series for the current selected item in the tree
     */
    protected void showTimeSeries() {
    	showTimeSeries(selectedFile);
    }
	
    /**
     * Shows the time series for the current selected item in the tree
     */
    protected void showTimeSeries(Path p) {
        TimeSeriesCptySelectionDialog cptySelection = new TimeSeriesCptySelectionDialog(GUIManager.shell, p);
        cptySelection.open();
        
        if (cptySelection.getSelectedCpty() != null) {
            TimeSeriesDialog dlg = new TimeSeriesDialog(GUIManager.shell, p, cptySelection.getSelectedCpty());
            dlg.open();
        }
        else {
            logger.info("No counterparty selected - no time series will display!");
        }
    }

    /**
     * Shows a graph of our market data
     */
    protected void showMarketData(Path p) {
        MarketDataSelectionDialog marketDataSelection = new MarketDataSelectionDialog(GUIManager.shell, p);
        marketDataSelection.open();
        
        if (marketDataSelection.getSelectedMktDataType() != null) {
            MarketDataGraphDialog dlg = new MarketDataGraphDialog(GUIManager.shell, p, marketDataSelection.getSelectedMktDataType(), marketDataSelection.getSelectedMktDataInstance());
            dlg.open();
        }
        else {
            logger.info("No market data type selected - no graph will display!");
        }
    }
}