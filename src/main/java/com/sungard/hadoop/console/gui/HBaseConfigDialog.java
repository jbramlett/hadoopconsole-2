/*
 * Copyright 2008 by RBC Capital Markets Inc., All rights reserved.
 * 
 * This source code, and resulting software, is confidential and proprietary
 * information and is the intellectual property of RBC Capital Markets Inc.
 * 
 * You shall not disclose such Proprietary Information and shall use it only in
 * accordance with the terms and conditions of any and all license agreements
 * you have entered into with The Company.
 */
package com.sungard.hadoop.console.gui;

import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import com.sungard.hadoop.console.profile.HadoopConfig;
import com.sungard.hadoop.console.profile.HadoopConfigFactory;

/**
 * Allows you to edit a backup profile
 *
 * @author jbramlet
 */
public class HBaseConfigDialog extends BaseDialog {
	
	protected Text hbaseDir;
	protected Button hbaseDirPicker;
	protected Text hbaseMasterWebURL;
	protected Text hbaseRegionServerWebURL;
	protected Text zookeeperHost;
	protected Text zookeeperPort;
	protected Button saveButton;
	protected HadoopConfig config;
	protected boolean saved = false;
	
	/**
	 * Constructor
	 * @param shell The shell
	 * @param profile The profile we are editing
	 */
	public HBaseConfigDialog(Shell shell) {
		super(shell);
		HadoopConfigFactory configFactory = new HadoopConfigFactory();
		config = configFactory.getConfig();
	}
	/**
	 * Constructor
	 * 
	 * @param shell The owning shell
	 * @param styles The dialog style
	 */
	public HBaseConfigDialog(Shell shell, int styles) {
		super(shell, styles);
		HadoopConfigFactory configFactory = new HadoopConfigFactory();
		config = configFactory.getConfig();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.jpmorgan.gcrm.basel.ui.BaseDialog#getTitle()
	 */
	protected String getTitle() {
		return "HBase Configuration";
	}
	/*
	 * (non-Javadoc)
	 * 
	 * @see com.jpmorgan.gcrm.basel.ui.BaseDialog#processButton(java.lang.Object)
	 */
	protected boolean processButton(Object source) {
		boolean processed = super.processButton(source);

		try {			
			// if we didn't process this button in our super class then
			// process it here
			if (!processed) {
				// figure out what dialog to display for the button
				if (source == hbaseDirPicker) {
					pickDirectory(hbaseDir);
				}
				else if (source == saveButton) {
					saveSelected();
				}
			}
		}
		catch (Exception e) {
			throw new RuntimeException("Failed trying to open dialog!", e);
		}

		return processed;
	}
	
	
	/**
	 * Process the save button
	 */
	private void saveSelected() {
		// verify our content
		if ((hbaseDir.getText() == null) || (hbaseDir.getText().trim().length() == 0)) {
			this.showMessageBox("You must specify your HBase directory", "Error!");
			return;						
		}
		if ((hbaseMasterWebURL.getText() == null) || (hbaseMasterWebURL.getText().trim().length() == 0)) {
			this.showMessageBox("You must specify a HBase Master Web URL", "Error!");
			return;						
		}
		if ((hbaseRegionServerWebURL.getText() == null) || (hbaseRegionServerWebURL.getText().trim().length() == 0)) {
			this.showMessageBox("You must specify an HBase Region Server Web URL", "Error!");
			return;						
		} 		
		if ((zookeeperHost.getText() == null) || (zookeeperHost.getText().trim().length() == 0)) {
			this.showMessageBox("You must specify the Zookeeper Host", "Error!");
			return;						
		}
		if ((zookeeperPort.getText() == null) || (zookeeperPort.getText().trim().length() == 0)) {
			this.showMessageBox("You must specify Zookeeper Port", "Error!");
			return;						
		}

		config.setHbaseDir(hbaseDir.getText());
		config.setHbaseMasterWebUrl(hbaseMasterWebURL.getText());
		config.sethBaseRegionServerWebUrl(hbaseRegionServerWebURL.getText());
		config.setHbaseZookeeperServer(zookeeperHost.getText());
		config.setHbaseZookeeperPort(zookeeperPort.getText());
		HadoopConfigFactory configFactory = new HadoopConfigFactory();
		configFactory.saveConfig(config);
		saved = true;
		this.closeDialog();		
	}
	
	/**
	 * Create the controls to display in the dialog
	 */
	protected void createControls(Composite parent) {
		Composite container = createContainer(parent, 1);


		// create a group box for the set of actions we can perform
		Group backupConfigGroup = createGroup(container, "Hadoop Configuration");

		// hbase directory
		Composite sdContainer = createContainer(backupConfigGroup, 3);
		Composite sdTextLabelContainer = createContainer(sdContainer, 2);
		createLabel(sdTextLabelContainer, "HBase Dir:");
		hbaseDir = createText(sdTextLabelContainer, 300, 0);
		if (config.getHbaseDir() != null) hbaseDir.setText(config.getHbaseDir());
		hbaseDirPicker = createButton(sdContainer, "Select", this, 0);
		
		// master website
		Composite tdContainer = createContainer(backupConfigGroup, 2);
		createLabel(tdContainer, "Master WebURL:");
		hbaseMasterWebURL = createText(tdContainer, 300, 0);
		if (config.getHbaseMasterWebUrl() != null) hbaseMasterWebURL.setText(config.getHbaseMasterWebUrl());

		// region server website
		Composite jdContainer = createContainer(backupConfigGroup, 2);
		createLabel(jdContainer, "Region Server WebURL:");
		hbaseRegionServerWebURL = createText(jdContainer, 300, 0);
		if (config.gethBaseRegionServerWebUrl() != null) hbaseRegionServerWebURL.setText(config.gethBaseRegionServerWebUrl());

		// zookeeper host
		Composite hdContainer = createContainer(backupConfigGroup, 2);
		createLabel(hdContainer, "Zookeeper Host:");
		zookeeperHost = createText(hdContainer, 300, 0);
		if (config.getHbaseZookeeperServer() != null) zookeeperHost.setText(config.getHbaseZookeeperServer());

		// zookeeper port
		Composite mdContainer = createContainer(backupConfigGroup, 2);
		createLabel(mdContainer, "Zookeeper Port:");
		zookeeperPort = createText(mdContainer, 300, 0);
		if (config.getHbaseZookeeperPort() != null) zookeeperPort.setText(config.getHbaseZookeeperPort());

		// and add our run and close
		Composite buttonContainer = createContainer(container, 2);
		saveButton = createButton(buttonContainer, "Save");
		
		addCloseButton(buttonContainer);
	}

	/**
	 * Creates a new button for our set of actions
	 * 
	 * @param parent The parent on which this button will live
	 * @param label The button label
	 * @return Button The newly created button
	 */
	protected Button createButton(Composite parent, String label) {
		return createButton(parent, label, this, 150);
	}
	/**
	 * Gets the saved
	 * @return boolean The saved
	 */
	public boolean isSaved() {
		return saved;
	}

}
