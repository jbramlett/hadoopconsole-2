/*
 * Copyright 2008 by RBC Capital Markets Inc., All rights reserved.
 * 
 * This source code, and resulting software, is confidential and proprietary
 * information and is the intellectual property of RBC Capital Markets Inc.
 * 
 * You shall not disclose such Proprietary Information and shall use it only in
 * accordance with the terms and conditions of any and all license agreements
 * you have entered into with The Company.
 */
package com.sungard.hadoop.console.gui;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.util.Bytes;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;

import com.sungard.common.utils.HBaseUtils;
import com.sungard.hadoop.console.profile.HadoopConfig;
import com.sungard.hadoop.console.profile.HadoopConfigFactory;

/**
 * Allows you to browse DFS
 *
 * @author jbramlet
 */
public class HBaseBrowserDialog extends BaseDialog {
	private static final Log logger = LogFactory.getLog(HBaseBrowserDialog.class);
	
	protected List tableList;
	protected Table tableContent;
	protected HBaseUtils hbaseUtils;
    protected HadoopConfig hConfig;
	
    
    protected Menu listMenu;
    protected MenuItem deleteTableMenuItem;
    
    protected Menu tableMenu;
    protected MenuItem deleteItemMenuItem;

    /**
	 * Constructor
	 * @param shell The shell
	 */
	public HBaseBrowserDialog(Shell shell) {
		super(shell);
	}
	/**
	 * Constructor
	 * 
	 * @param shell The owning shell
	 * @param styles The dialog style
	 * @param profile The profile we are editing
	 */
	public HBaseBrowserDialog(Shell shell, int styles) {
		super(shell, styles);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.jpmorgan.gcrm.basel.ui.BaseDialog#getTitle()
	 */
	protected String getTitle() {
		return "HBase Browser";
	}
	
	/**
	 * Create the controls to display in the dialog
	 */
	protected void createControls(Composite parent) {
	    // initialize our hadoop configuration
        HadoopConfigFactory configFactory = new HadoopConfigFactory();
        hConfig = configFactory.getConfig();           
        Configuration conf = HBaseConfiguration.create();
        conf.set("hbase.zookeeper.quorum", hConfig.getHbaseZookeeperServer());
        conf.set("hbase.zookeeper.property.clientPort", hConfig.getHbaseZookeeperPort());
        hbaseUtils = new HBaseUtils(conf);
        	    
	    Composite container = createContainer(parent, 1);

		Composite display = createContainer(container, 2);

		// create a group box for our tree
		Group listGroup = createGroup(display, "Tables");
		tableList = createList(listGroup, 400, 350);		
		String[] tables = hbaseUtils.listTables();
		for (String t : tables) {
		    tableList.add(t);
		}
        tableList.addSelectionListener(new SelectionListener() {
            @Override
            public void widgetDefaultSelected(SelectionEvent arg0) {
                widgetSelected(arg0);
            }
            @Override
            public void widgetSelected(SelectionEvent arg0) {
                showTableContent();
            }
            
        });
        // add a menu to the tree
        listMenu = new Menu(tableList);
        deleteTableMenuItem = new MenuItem(listMenu, SWT.NONE);
        deleteTableMenuItem.setText("Delete");
        deleteTableMenuItem.addSelectionListener(new SelectionListener() {
            @Override
            public void widgetDefaultSelected(SelectionEvent arg0) {
                widgetSelected(arg0);
            }
            @Override
            public void widgetSelected(SelectionEvent arg0) {
                deleteTable();
            }
            
        });
        tableList.setMenu(listMenu);
        
		
		// create a text for our display
		Group dataGroup = createGroup(display, "Table Content");
		tableContent = createTable(dataGroup, true, 400, 250);
				
        // add a menu to the tree
		tableMenu = new Menu(tableContent);
        deleteItemMenuItem = new MenuItem(tableMenu, SWT.NONE);
        deleteItemMenuItem.setText("Delete");
        deleteItemMenuItem.addSelectionListener(new SelectionListener() {
            @Override
            public void widgetDefaultSelected(SelectionEvent arg0) {
                widgetSelected(arg0);
            }
            @Override
            public void widgetSelected(SelectionEvent arg0) {
                deleteItem();
            }
            
        });
        tableContent.setMenu(tableMenu);
		
		
		addCloseButton(container);
	}
	
	/**
	 * Deletes the selected table
	 */
	protected void deleteTable() {
        try {
            tableContent.removeAll();
            if (tableList.getSelectionCount() == 1) {
                String table = tableList.getSelection()[0];                
                hbaseUtils.deleteTable(table);
                tableList.remove(tableList.getSelectionIndex());
            }
        }
        catch (Exception e) {
            logger.error("Failed building our table!", e);
        }	    
	}
	
    /**
     * Deletes the selected row from our table
     */
    protected void deleteItem() {
        try {
            // get our selected table
            String table = tableList.getSelection()[0];
            
            // now get our row
            String rowKey = (String)tableContent.getSelection()[0].getData();
            hbaseUtils.deleteRecord(table, rowKey);
            tableContent.remove(tableContent.getSelectionIndex());
        }
        catch (Exception e) {
            logger.error("Failed building record from our table!", e);
        }       
    }

    /**
	 * Shows the content of the current selected table
	 */
	protected void showTableContent() {
	    try {
    	    tableContent.removeAll();
    	    if (tableList.getSelectionCount() == 1) {
    	        String table = tableList.getSelection()[0];
    	        
    	        java.util.List<Map<String, byte[]>> tableData = hbaseUtils.getAllRecords(table);
    	        
    	        Set<String> cols = new HashSet<String>();
    	        for (Map<String, byte[]> row : tableData) {
    	            cols.addAll(row.keySet());
    	        }
    	        
    	        // now build our column headers
    	        TableColumn[] tableColumns = tableContent.getColumns();
    	        int i = 0;
    	        for (String col : cols) {
    	            if ((tableColumns != null) && (tableColumns.length > i)) {
    	                tableColumns[i].setText(col);
    	            }
    	            else {
    	                TableColumn tc = new TableColumn(tableContent, SWT.NONE);
    	                tc.setWidth(100);
    	                tc.setText(col);	                
    	            }
    	            i++;
    	        }
    	        if (i < tableColumns.length) {
    	            for (int ii = i; ii < tableColumns.length; ii++) {
    	                tableColumns[ii].setText("");
    	            }
    	        }
    	        
    	        // now add our data	        
                for (Map<String, byte[]> row : tableData) {
                    i = 0;
                    TableItem ti = new TableItem(tableContent, SWT.NONE);
                    ti.setData(new String(row.get("rowKey")));
                    String[] data = new String[cols.size()];
                    for (String col : cols) {
                        String colVal = "";
                        byte[] val = row.get(col);
                        if (val != null) {
                            colVal = Bytes.toString(val);
                        }
                        data[i] = colVal;
                        i++;
                    }
                    ti.setText(data);
                }
    	    }
	    }
	    catch (Exception e) {
	        logger.error("Failed building our table!", e);
	    }
	}
	
    /* (non-Javadoc)
     * @see org.thirdstreet.backup.gui.BaseDialog#processButton(java.lang.Object)
     */
    @Override
    protected boolean processButton(Object source) {
        boolean processed = super.processButton(source);

        try {           
            // if we didn't process this button in our super class then
            // process it here
            if (!processed) {
            }
        }
        catch (Exception e) {
            throw new RuntimeException("Failed trying to open dialog!", e);
        }

        return processed;
    }
    
}