/*
 * @(#)TimeSeriesDialog.java
 *
 * Copyright 2013 Sungard and/or its affiliates. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or 
 * without modification, are permitted provided that the following 
 * conditions are met:
 * 
 * -Redistributions of source code must retain the above copyright  
 * notice, this  list of conditions and the following disclaimer.
 * 
 * -Redistribution in binary form must reproduct the above copyright 
 * notice, this list of conditions and the following disclaimer in 
 * the documentation and/or other materials provided with the 
 * distribution.
 * 
 * Neither the name of Sungard and/or its affiliates. or the names of 
 * contributors may be used to endorse or promote products derived 
 * from this software without specific prior written permission.
 * 
 * This software is provided "AS IS," without a warranty of any 
 * kind. ALL EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND 
 * WARRANTIES, INCLUDING ANY IMPLIED WARRANTY OF MERCHANTABILITY, 
 * FITNESS FOR A PARTICULAR PURPOSE OR NON-INFRINGEMENT, ARE HEREBY 
 * EXCLUDED. SUN AND ITS LICENSORS SHALL NOT BE LIABLE FOR ANY 
 * DAMAGES OR LIABILITIES  SUFFERED BY LICENSEE AS A RESULT OF  OR 
 * RELATING TO USE, MODIFICATION OR DISTRIBUTION OF THE SOFTWARE OR 
 * ITS DERIVATIVES. IN NO EVENT WILL SUN OR ITS LICENSORS BE LIABLE 
 * FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR DIRECT, INDIRECT, 
 * SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES, HOWEVER 
 * CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING OUT OF 
 * THE USE OF OR INABILITY TO USE SOFTWARE, EVEN IF SUN HAS BEEN 
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 * 
 * You acknowledge that Software is not designed, licensed or 
 * intended for use in the design, construction, operation or 
 * maintenance of any nuclear facility. 
 */
package com.sungard.hadoop.console.gui;

import java.awt.Color;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.hadoop.conf.Configuration;
import org.apache.commons.logging.LogFactory;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Shell;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.DateAxis;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYItemRenderer;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.time.Day;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import org.jfree.experimental.chart.swt.ChartComposite;
import org.jfree.ui.RectangleInsets;

import com.google.gson.reflect.TypeToken;
import com.sungard.common.utils.JSONUtils;
import com.sungard.hadoop.console.profile.HadoopConfig;
import com.sungard.hadoop.console.profile.HadoopConfigFactory;

/**
 * Dialog used to display a time series of our monte carlo simulation
 * @author sungard
 */
public class MarketDataGraphDialog extends BaseDialog {
    private static final Log logger = LogFactory.getLog(MarketDataGraphDialog.class);
    
    protected Path dataDir;
    protected String mktDataType;
    protected String mktDataInstance;
    protected String cob;
    protected boolean isCurve = true;
    
    /**
     * Constructor
     * @param shell
     * @param path The path to the data
     * @param mktDataType The type of market data we are graphing
     * @param mktDataInstance The instance of our market data we are graphing
     */
    public MarketDataGraphDialog(Shell shell, Path path, String mktDataType, String mktDataInstance) {
        super(shell);
        this.dataDir = path;
        this.mktDataType = mktDataType;
        this.mktDataInstance = mktDataInstance;
        
        // now pull the cob from the path
        logger.info("Building market data graph for " + mktDataType + ": " + mktDataInstance + " from data in " + dataDir.toString());
        cob = dataDir.getName();
        logger.info("Using COB " + cob);
    }

    /**
     * Constructor
     * @param shell
     * @param styles
     */
    public MarketDataGraphDialog(Shell shell, int styles) {
        super(shell, styles);
    }

    /* (non-Javadoc)
     * @see com.sungard.hadoop.console.gui.BaseDialog#getTitle()
     */
    @Override
    protected String getTitle() {
        return mktDataType + ":" + mktDataInstance + " (for " + cob + ")";
    }

    /* (non-Javadoc)
     * @see com.sungard.hadoop.console.gui.BaseDialog#createControls(org.eclipse.swt.widgets.Composite)
     */
    @Override
    protected void createControls(Composite parent) {
        //Composite container = createContainer(parent, 1);
    	XYDataset data = createDataset();
    	JFreeChart chart = null;
    	if (isCurve)
    		chart = createTimeSeries(data);
    	else 
    		chart = createLineChart(data);
        ChartComposite frame = new ChartComposite(parent, SWT.NONE, chart, true);
        frame.setDisplayToolTips(true);
        frame.setHorizontalAxisTrace(false);
        frame.setVerticalAxisTrace(false);
        //frame.setSize(600, 300);
        
        //Composite buttonComposite = createContainer(parent, 1);
        //this.addCloseButton(buttonComposite);
    }

    /* (non-Javadoc)
     * @see com.sungard.hadoop.console.gui.BaseDialog#initDisplay(org.eclipse.swt.widgets.Composite)
     */
    @Override
    protected void initDisplay(Composite shell) {
        super.initDisplay(shell);
        shell.setSize(850,  650);
    }

    /**
     * Retrieves our path data
     * @return List<Map> Our path data
     */
    @SuppressWarnings({ "rawtypes", "unchecked" })
    protected Map<String, Map> getPathData() {
        Map<String, Map> result = new HashMap<String, Map>();
        
        try {
            Configuration config = getConfig();
            FileSystem fs = FileSystem.get(config);

            // find our profile dates file
            FileStatus[] files = fs.listStatus(dataDir);
            for (FileStatus f : files) {
                if (f.getPath().getName().startsWith("path_")) {
                    logger.info("Found path file " + f.getPath().toString());
                    BufferedReader reader =new BufferedReader(new InputStreamReader(fs.open(f.getPath())));
                    
                    // we will only process the first line - all other lines will
                    // be ignored
                    String line = reader.readLine();
                    while (line != null){
                        String[] lineSplit = line.split("\t", 2);                        
                        Map<String, Map> mktData = JSONUtils.fromJSONToMap(lineSplit[1]);
                        
                        // now find the element we are looking for
                        String pathId = lineSplit[0];
                        Map mktDataTypeValues = mktData.get(mktDataType);                        
                        if ((mktDataInstance != null) && (mktDataInstance.length() > 0)) {
                            Map data = (Map)mktDataTypeValues.get("data");                            
                            Map instanceData = (Map)data.get(mktDataInstance);
                            Map curveData = (Map)instanceData.get("curve");
                            if (curveData == null) {
                                String rate = (String)instanceData.get("rate");
                                if (rate == null) {
                                    rate = (String)instanceData.get("price");
                                }
                                curveData = new HashMap();
                                curveData.put(cob, rate);
                                isCurve = false;
                            }
                           
                            result.put(pathId, curveData);
                        }
                        else {
                            // now that we've navigated down (if necessary), pull our curve
                            Map curveData = (Map)mktDataTypeValues.get("curve");
                            if (curveData == null) {
                                String rate = (String) mktData.get(mktDataType).get("rate");
                                curveData = new HashMap();
                                curveData.put(cob, rate);
                                isCurve = false;
                            }
                            result.put(pathId, curveData);                            
                        }
                        line = reader.readLine();
                    }
                    reader.close();                    
                }
            }
        }
        catch (Exception e) {
            logger.error("Failed to load profile dates from HDFS!", e);
        }        
        return result;
    }
    
    /**
     * Loads our profile dates from our distributed cache
     * @param config The config
     * @return Calendar[] the set of profile dates
     */
    protected Calendar[] getProfileDates() {
        Calendar[] results = null;
        
        try {
            JSONUtils<List<String>> jsonUtils = new JSONUtils<List<String>>(new TypeToken<List<String>>(){}.getType());

            Configuration config = getConfig();

            Path profileDatesFile = null;
            FileSystem fs = FileSystem.get(config);
            FileStatus[] roots = fs.listStatus(new Path("/user/sungard/creditrisk-montecarlo/profile_dates/" + cob));
            for (FileStatus f : roots) {
                if (f.getPath().getName().startsWith("profile_dates")) {
                    profileDatesFile = f.getPath();
                    break;
                }
            }
            
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
            
            logger.info("Find our profile dates for this run");
            if (profileDatesFile != null) {
                // find our profile dates file
                if (profileDatesFile.getName().startsWith("profile_dates")) {
                    logger.info("Found profile dates file " + profileDatesFile.toString());
                    BufferedReader reader =new BufferedReader(new InputStreamReader(fs.open(profileDatesFile)));
                    
                    // we will only process the first line - all other lines will
                    // be ignored
                    String line = reader.readLine();
                    while (line != null){
                        List<String> profileDates = jsonUtils.fromMRInput(line);
    
                        results = new Calendar[profileDates.size()];
                        for (int i = 0; i < profileDates.size(); i++) {
                            Calendar cal = Calendar.getInstance();
                            cal.setTime(dateFormat.parse(profileDates.get(i).trim()));
                            results[i] = cal;
                        }
                        line = reader.readLine();
                    }
                    reader.close();
                    
                    // return the first result
                    return results;
                }
                else {
                    logger.info("Failed to find our profile dates!");
                }
            }           
        }
        catch (Exception e) {
            logger.error("Failed to load profile dates from HDFS!", e);
        }
        return null;
    }
    
    /**
     * Gets our config for HDFS
     * @return Configuration The configuration
     */
    protected Configuration getConfig() {
        HadoopConfigFactory configFactory = new HadoopConfigFactory();
        HadoopConfig hConfig = configFactory.getConfig();           
        Configuration config = new Configuration();
        config.set("mapred.job.tracker", hConfig.getMapReduceMaster());
        config.set("fs.default.name", hConfig.getHdfsMaster());
        return config;
    }
    
    
    /**
     * Creates a dataset, consisting of two series of monthly data.
     *
     * @return The dataset.
     */
    @SuppressWarnings("rawtypes")
    protected XYDataset createDataset() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
        
        Map<String, Map> pathData = getPathData();
        
        if (isCurve) {
	        TimeSeriesCollection dataset = new TimeSeriesCollection();
	        for (Map.Entry<String, Map> me : pathData.entrySet()) {
	            Map pathDataMap = me.getValue();
	            TimeSeries s1 = new TimeSeries("Path " + me.getKey());
	            for (Object k : pathDataMap.keySet()) {
	                try {
	                    Calendar c = Calendar.getInstance();
	                    c.setTime(dateFormat.parse(k.toString()));
	                    BigDecimal price = new BigDecimal(pathDataMap.get(k).toString());
	                    s1.add(new Day(c.getTime()), price.doubleValue());
	                }
	                catch (Exception e) {
	                    logger.error("Failed to parse our date " + k.toString(), e);
	                }
	            }
	            dataset.addSeries(s1);
	        }
	        return dataset;
        }
        else {
        	XYSeries dataset = new XYSeries(mktDataType);
	        for (Map.Entry<String, Map> me : pathData.entrySet()) {
	        	Integer pathId = new Integer(me.getKey());
	            Map pathDataMap = me.getValue();
	            String value = null;
	            for (Object k : pathDataMap.keySet()) {
	            	value = pathDataMap.get(k).toString();
	            }
	            dataset.add(pathId, new BigDecimal(value));
	        }
	        return new XYSeriesCollection(dataset);
        }
        
   }
    
    /**
     * Creates a chart.
     * 
     * @param dataset  a dataset.
     * 
     * @return A chart.
     */
    private JFreeChart createTimeSeries(XYDataset dataset) {
        String title = "Market Data: " + mktDataType;
        if ((mktDataInstance != null) && (mktDataInstance.length() > 0)) {
            title = title + " (" + mktDataInstance + ")";
        }
        
        JFreeChart chart = ChartFactory.createTimeSeriesChart(
            title,
            "Date",             // x-axis label
            "Price",            // y-axis label
            dataset,            // data
            true,               // create legend?
            true,               // generate tooltips?
            false               // generate URLs?
        );

        chart.setBackgroundPaint(Color.white);

        XYPlot plot = (XYPlot) chart.getPlot();
        plot.setBackgroundPaint(Color.lightGray);
        plot.setDomainGridlinePaint(Color.white);
        plot.setRangeGridlinePaint(Color.white);
        plot.setAxisOffset(new RectangleInsets(5.0, 5.0, 5.0, 5.0));
        plot.setDomainCrosshairVisible(true);
        plot.setRangeCrosshairVisible(true);
        
        XYItemRenderer r = plot.getRenderer();
        if (r instanceof XYLineAndShapeRenderer) {
            XYLineAndShapeRenderer renderer = (XYLineAndShapeRenderer) r;
            renderer.setBaseShapesVisible(true);
            renderer.setBaseShapesFilled(true);
        }
        
        DateAxis axis = (DateAxis) plot.getDomainAxis();
        axis.setDateFormatOverride(new SimpleDateFormat("dd-MMM-yyyy"));
        
        return chart;

    }
    
    /**
     * Creates a chart.
     * 
     * @param dataset  a dataset.
     * 
     * @return A chart.
     */
    private JFreeChart createLineChart(XYDataset dataset) {
        String title = "Market Data: " + mktDataType;
        if ((mktDataInstance != null) && (mktDataInstance.length() > 0)) {
            title = title + " (" + mktDataInstance + ")";
        }
        
        JFreeChart chart = ChartFactory.createXYLineChart(
            title,
            "Path",             // x-axis label
            "Rate",            // y-axis label
            dataset,            // data
            PlotOrientation.VERTICAL,
            true,               // create legend?
            true,               // generate tooltips?
            false               // generate URLs?
        );

        chart.setBackgroundPaint(Color.white);

        XYPlot plot = (XYPlot) chart.getPlot();
        plot.setBackgroundPaint(Color.lightGray);
        plot.setDomainGridlinePaint(Color.white);
        plot.setRangeGridlinePaint(Color.white);
        plot.setAxisOffset(new RectangleInsets(5.0, 5.0, 5.0, 5.0));
        plot.setDomainCrosshairVisible(true);
        plot.setRangeCrosshairVisible(true);
        plot.getDomainAxis().setStandardTickUnits(NumberAxis.createIntegerTickUnits());
        
        XYItemRenderer r = plot.getRenderer();
        if (r instanceof XYLineAndShapeRenderer) {
            XYLineAndShapeRenderer renderer = (XYLineAndShapeRenderer) r;
            renderer.setBaseShapesVisible(true);
            renderer.setBaseShapesFilled(true);
        }
        
        //DateAxis axis = (DateAxis) plot.getDomainAxis();
        //axis.setDateFormatOverride(new SimpleDateFormat("dd-MMM-yyyy"));
        
        return chart;

    }
    
}
