/*
 * Copyright 2008 by RBC Capital Markets Inc., All rights reserved.
 * 
 * This source code, and resulting software, is confidential and proprietary
 * information and is the intellectual property of RBC Capital Markets Inc.
 * 
 * You shall not disclose such Proprietary Information and shall use it only in
 * accordance with the terms and conditions of any and all license agreements
 * you have entered into with The Company.
 */
package com.sungard.hadoop.console.gui;

import java.awt.Desktop;
import java.net.URI;
import java.util.LinkedList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;

import com.sungard.hadoop.console.profile.HBaseShellJob;
import com.sungard.hadoop.console.profile.HadoopConfig;
import com.sungard.hadoop.console.profile.HadoopConfigFactory;
import com.sungard.hadoop.console.profile.HadoopJob;
import com.sungard.hadoop.console.profile.HadoopJobFactory;
import com.sungard.hadoop.console.profile.HadoopShellJob;

/**
 * The main dialog class for our management console process
 *
 * @author jbramlet
 */
public class HadoopMainDialog extends BaseDialog  {
	private static final Log logger = LogFactory.getLog(HadoopMainDialog.class);
	
	protected Table hadoopProfiles;
	protected Button runButton;
	protected Button addProfileButton;
	protected Button editProfileButton;
	protected Button removeProfileButton;
	protected final HadoopJobFactory hadoopProfileFactory;
	protected Button launchHadoopMaster;
	protected Button launchHadoopJobTracker;
    protected Button launchHbaseMaster;
    protected Button launchHbaseRegion;
	protected Button browseDFS;
    protected Button browseHbase;
	protected Button configHadoopButton;
	protected Button configHbaseButton;
	protected Button configAWSButton;
	protected Button startAllHadoopButton;
	protected Button stopAllHadoopButton;
	protected Button startHbaseButton;
	protected Button stopHbaseButton;
	protected Button browseS3;
	protected Button browseEMR;
	
	/**
	 * Constructor
	 * @param shell The shell
	 */
	public HadoopMainDialog(Shell shell, HadoopJobFactory backupProfileFactory) {
		super(shell);
		this.hadoopProfileFactory = backupProfileFactory;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.jpmorgan.gcrm.basel.ui.BaseDialog#getShell()
	 */
	protected Shell getShell() {
		// return our parent - this will allow our topmost shell to serve
		// as the shell for this dialog (versus a shell within a shell)
		// allowing for us to show up in the Windows taskbar
		return getParent();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.jpmorgan.gcrm.basel.ui.BaseDialog#getTitle()
	 */
	protected String getTitle() {
		return "Hadoop Console";
	}
	/*
	 * (non-Javadoc)
	 * 
	 * @see com.jpmorgan.gcrm.basel.ui.BaseDialog#processButton(java.lang.Object)
	 */
	protected boolean processButton(Object source) {
		boolean processed = super.processButton(source);
		HadoopConfigFactory configFactory = new HadoopConfigFactory();
		HadoopConfig config = configFactory.getConfig();
		
		try {			
			// if we didn't process this button in our super class then
			// process it here
			if (!processed) {
				// see if this is one of our buttons
				if (source == runButton) {
					run();
				}
				else if (source == addProfileButton) {
					addProfile();
				}
				else if (source == editProfileButton) {
					editProfile();
				}
				else if (source == removeProfileButton) {
					removeProfile();
				}
				else if (source == launchHadoopJobTracker) {
					launchBrowser(config.getJobTrackerWebURL());
				}
				else if (source == launchHadoopMaster) {
					launchBrowser(config.getNamenodeWebURL());
				}
                else if (source == launchHbaseMaster) {
                    launchBrowser(config.getHbaseMasterWebUrl());
                }
                else if (source == launchHbaseRegion) {
                    launchBrowser(config.gethBaseRegionServerWebUrl());
                }
				else if (source == browseDFS) {
					launchDFSBrowser();
				}
                else if (source == browseHbase) {
                    launchHbaseBrowser();
                }
				else if (source == configHadoopButton) {
					configHadoop();
				}
				else if (source == configHbaseButton) {
					configHbase();
				}
				else if (source == configAWSButton) {
					configAWS();
				}
				else if (source == startAllHadoopButton) {
					startAll();
				}
				else if (source == stopAllHadoopButton) {
					stopAll();
				}
				else if (source == startHbaseButton) {
					startHbase();
				}
				else if (source == stopHbaseButton) {
					stopHbase();
				}
				else if (source == browseS3) {
				    //launchS3Browser();
				    launchBrowser("https://console.aws.amazon.com/s3/home?region=us-east-1");
				}
				else if (source == browseEMR) {
                    launchBrowser("https://console.aws.amazon.com/elasticmapreduce/home?region=us-east-1");				    
				}
			}
		}
		catch (Exception e) {
			throw new RuntimeException("Failed trying to open dialog!", e);
		}

		return processed;
	}
	
	/**
	 * COnfigure our hadoop environment
	 */
	protected void configHadoop() {
		HadoopConfigDialog configDialog = new HadoopConfigDialog(GUIManager.shell);
		configDialog.open();
	}
	
	/**
	 * COnfigure our hbase environment
	 */
	protected void configHbase() {
		HBaseConfigDialog configDialog = new HBaseConfigDialog(GUIManager.shell);
		configDialog.open();
	}

	/**
	 * COnfigure our AWS environment
	 */
	protected void configAWS() {
		AWSConfigDialog configDialog = new AWSConfigDialog(GUIManager.shell);
		configDialog.open();
	}

	/**
	 * Launches our DFS browser
	 */
	protected void launchDFSBrowser() {
		DFSBrowserDialog browserDialog = new DFSBrowserDialog(GUIManager.shell);
		browserDialog.open();		
		
		//launchBrowser("http://localhost:50075/browseDirectory.jsp?namenodeInfoPort=50070&dir=/");	
	}
		
    /**
     * Launches our Hbase browser
     */
    protected void launchHbaseBrowser() {
        HBaseBrowserDialog browserDialog = new HBaseBrowserDialog(GUIManager.shell);
        browserDialog.open();       
        
        //launchBrowser("http://localhost:50075/browseDirectory.jsp?namenodeInfoPort=50070&dir=/"); 
    }

    /**
     * Launches our S3 browser
     */
    protected void launchS3Browser() {
        AWSS3BrowserDialog browserDialog = new AWSS3BrowserDialog(GUIManager.shell);
        browserDialog.open();       
        
        //launchBrowser("http://localhost:50075/browseDirectory.jsp?namenodeInfoPort=50070&dir=/"); 
    }

    /**
	 * Starts all the processes in Hadoop
	 */
	protected void startAll() {
		HadoopRunnerDialog runnerDialog = new HadoopRunnerDialog(GUIManager.shell, new HadoopShellJob("Start All"));
		runnerDialog.open();		
	}
	
	/**
	 * Stops all the processes in Hadoop
	 */
	protected void stopAll() {
		HadoopRunnerDialog runnerDialog = new HadoopRunnerDialog(GUIManager.shell, new HadoopShellJob("Stop All"));
		runnerDialog.open();				
	}

    /**
	 * Starts all the processes in hbase
	 */
	protected void startHbase() {
		HadoopRunnerDialog runnerDialog = new HadoopRunnerDialog(GUIManager.shell, new HBaseShellJob("Start HBase"));
		runnerDialog.open();		
	}
	
	/**
	 * Stops all the processes in hbase
	 */
	protected void stopHbase() {
		HadoopRunnerDialog runnerDialog = new HadoopRunnerDialog(GUIManager.shell, new HBaseShellJob("Stop HBase"));
		runnerDialog.open();				
	}

	/**
	 * Runs our backup against the given profiles - the preview mode is determined
	 * based on our member variable (since used elsewhere)
	 */
	protected void run() {
		List<HadoopJob> profiles = new LinkedList<HadoopJob>();
		for (TableItem ti : hadoopProfiles.getSelection()) {
			HadoopJob profile = (HadoopJob)ti.getData();
			profiles.add(profile);
		}		
		
		if (profiles.size() > 0) {
		    HadoopRunnerDialog runnerDialog = new HadoopRunnerDialog(GUIManager.shell, profiles.get(0));
		    runnerDialog.open();
		}
	}
	
	/**
	 * Launches the default browser
	 * @param url The url to show
	 */
	protected void launchBrowser(String url) {
		try {
			logger.info("Launching browser to " + url);
			if (Desktop.isDesktopSupported()) {
				Desktop desktop = Desktop.getDesktop();
				desktop.browse(new URI(url));
			}
			else {
				logger.info("Desktop environment not supported! Will try manually");
				String[] args = new String[] {"firefox", url};
				Runtime.getRuntime().exec(args);
			}
		}
		catch (Exception e) {
			logger.error("Failed to launch browser!", e);
		}
	}
	
	/**
	 * Adds a new backup profile
	 */
	protected void addProfile() {
		InputDialog nameDlg = new InputDialog(GUIManager.shell, "Hadoop Job Creation", "Enter Job Name", "");
		nameDlg.open();
		if (nameDlg.getText() != null) {
			// now get our wizard and let's start this process
			HadoopJobDialog dlg = new HadoopJobDialog(GUIManager.shell, hadoopProfileFactory.createNewJob(nameDlg.getText()));
			dlg.open();
	
			// see how the user ended this
			if (dlg.isSaved()) {
				HadoopJob newProfile = dlg.getBackupProfile(); 
				hadoopProfileFactory.saveJob(newProfile);
				addProfile(newProfile);
			}
		}
	}
	
	/**
	 * Edits a new backup profile
	 */
	protected void editProfile() {
		// get the current selection
		TableItem[] selectedItems = hadoopProfiles.getSelection();
		if (selectedItems.length > 0) {
			// now get our wizard and let's start this process
			HadoopJobDialog dlg = new HadoopJobDialog(GUIManager.shell, (HadoopJob)selectedItems[0].getData());
			dlg.open();

			// see how the user ended this
			if (dlg.isSaved()) {
				HadoopJob updatedProfile = dlg.getBackupProfile(); 
				hadoopProfileFactory.saveJob(updatedProfile);
				editProfile(updatedProfile);
			}
		}		
	}
	
	/**
	 * Removes a backup profile
	 */
	protected void removeProfile() {
		// get the current selection
		TableItem[] selectedItems = hadoopProfiles.getSelection();
		if (selectedItems.length > 0) {
			// loop over all selected items removing each one
			for (TableItem ti : selectedItems) {
				HadoopJob bp = (HadoopJob)ti.getData();
				hadoopProfileFactory.removeJob(bp);
			}
			
			// just remove all the items and add them back
			hadoopProfiles.removeAll();
			loadProfiles();			
		}		
	}

	/**
	 * Adds a backup profile to our table
	 * @param bp The backup profile we are adding
	 */
	protected void addProfile(HadoopJob bp) {
		TableItem ti = new TableItem(hadoopProfiles, SWT.NONE);
		ti.setData(bp);
		ti.setText(getProfileData(bp));
	}
	
	/**
	 * Updates a profile row in the table
	 * @param bp The profile to update
	 * @param running Flag indicating if the profile is running
	 */
	protected void editProfile(HadoopJob bp) {
		// find this table item
		TableItem[] tis = hadoopProfiles.getItems();
		for (TableItem t : tis) {
			if (t.getData() == bp) {
				t.setText(getProfileData(bp));
				break;
			}
		}
	}
	
	/**
	 * Builds our string[] of profile data
	 * @param bp The profile data we are building from
	 * @param running The flag indicating if the profile is running
	 * @return String[] The profile data broken out in to columns
	 */
	private String[] getProfileData(HadoopJob bp) {
		return new String[] {bp.getName(), bp.getJarFile(), bp.getMainClass(), bp.getArgs()};		
	}
	
	/**
	 * Loads our profiles in to the table
	 */
	protected void loadProfiles() {
		// now load our profile
		List<HadoopJob> data = hadoopProfileFactory.listJobs();
		if (data != null) {
			for (HadoopJob bp : data) {
				addProfile(bp);
			}
		}			
	}
	
	/**
	 * Create the controls to display in the dialog
	 */
	protected void createControls(Composite parent) {

		Composite container = createContainer(parent, 1);

		Group backupProfileGroup = createGroup(container, "Hadoop Jobs");
		
		
		// create our table
		hadoopProfiles = this.createMultiTable(backupProfileGroup, true, 0, 0);//675, 150);
		// and add our columns
		TableColumn nameColumn = new TableColumn(hadoopProfiles, SWT.NONE);
		nameColumn.setWidth(200);
		nameColumn.setText("Name");
		TableColumn lastRunColumn = new TableColumn(hadoopProfiles, SWT.NONE);
		lastRunColumn.setWidth(200);
		lastRunColumn.setText("Jar");
		TableColumn lastRunEndColumn = new TableColumn(hadoopProfiles, SWT.NONE);
		lastRunEndColumn.setWidth(200);
		lastRunEndColumn.setText("Main Class");
		TableColumn lastRunElapsedColumn = new TableColumn(hadoopProfiles, SWT.NONE);
		lastRunElapsedColumn.setWidth(250);
		lastRunElapsedColumn.setText("Args");

		loadProfiles();
		
		// add our buttons
		Composite profileButtons = createContainer(backupProfileGroup, 3);		
		addProfileButton = createButton(profileButtons, "Add Job");		
		editProfileButton = createButton(profileButtons, "Edit Job");
		removeProfileButton = createButton(profileButtons, "Remove Job");

		Composite runButtons = createContainer(backupProfileGroup, 1);		
		runButton = createButton(runButtons, "Run Selected");		

		
		// management container
        Composite managementContainer = createContainer(container, 3);
		
		// Hadoop management group
		Composite hadoopManagementGroup = createGroup(managementContainer, "Hadoop Management");
		Composite hadoopButtons = createContainer(hadoopManagementGroup, 2);		
		launchHadoopMaster = createButton(hadoopButtons, "View Hadoop Master");		
		launchHadoopJobTracker = createButton(hadoopButtons, "View Job Tracker");		
		browseDFS = createButton(hadoopButtons, "Browse DFS");		
		Button empty = createButton(hadoopButtons, "Placeholder");
		empty.setVisible(false);
        startAllHadoopButton = createButton(hadoopButtons, "Start-All Hadoop");
        stopAllHadoopButton = createButton(hadoopButtons, "Stop-All Hadoop");
		
        // HBase management
		Composite hbaseManagementGroup = createGroup(managementContainer, "HBase Management");
		Composite hbaseButtons = createContainer(hbaseManagementGroup, 2);		
        launchHbaseMaster = createButton(hbaseButtons, "View HBase Master");     
        launchHbaseRegion = createButton(hbaseButtons, "View HBase Region");       
        browseHbase = createButton(hbaseButtons, "Browse HBase");      
		Button hBasempty = createButton(hbaseButtons, "Placeholder");
		hBasempty.setVisible(false);
        startHbaseButton = createButton(hbaseButtons, "Start HBase");
        stopHbaseButton = createButton(hbaseButtons, "Stop HBase");

        // AWS management
        Composite awsManagementGroup = createGroup(managementContainer, "AWS");
        Composite awsButtons = createContainer(awsManagementGroup, 1);      
        browseEMR = createButton(awsButtons, "View EMR");       
        browseS3 = createButton(awsButtons, "Browse S3");     
        
        Composite hadoopConfig = createGroup(container, "Configuration");        
		Composite settingsButtons = createContainer(hadoopConfig, 3);		
		configHadoopButton = createButton(settingsButtons, "Configure Hadoop");
		configHbaseButton = createButton(settingsButtons, "Configure Hbase");
		configAWSButton = createButton(settingsButtons, "Configure AWS");
		
		addCloseButton(container);
	}	
}
