/*
 * Copyright 2008 by RBC Capital Markets Inc., All rights reserved.
 * 
 * This source code, and resulting software, is confidential and proprietary
 * information and is the intellectual property of RBC Capital Markets Inc.
 * 
 * You shall not disclose such Proprietary Information and shall use it only in
 * accordance with the terms and conditions of any and all license agreements
 * you have entered into with The Company.
 */
package com.sungard.hadoop.console.gui;

import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import com.sungard.hadoop.console.profile.HadoopConfig;
import com.sungard.hadoop.console.profile.HadoopConfigFactory;

/**
 * Allows you to edit a backup profile
 *
 * @author jbramlet
 */
public class AWSConfigDialog extends BaseDialog {
	
	protected Text accessId;
	protected Text secretKey;
	protected Text logURL;
	protected Text jarDir;
	protected Button saveButton;
	protected HadoopConfig config;
	protected boolean saved = false;
	
	/**
	 * Constructor
	 * @param shell The shell
	 * @param profile The profile we are editing
	 */
	public AWSConfigDialog(Shell shell) {
		super(shell);
		HadoopConfigFactory configFactory = new HadoopConfigFactory();
		config = configFactory.getConfig();
	}
	/**
	 * Constructor
	 * 
	 * @param shell The owning shell
	 * @param styles The dialog style
	 */
	public AWSConfigDialog(Shell shell, int styles) {
		super(shell, styles);
		HadoopConfigFactory configFactory = new HadoopConfigFactory();
		config = configFactory.getConfig();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.jpmorgan.gcrm.basel.ui.BaseDialog#getTitle()
	 */
	protected String getTitle() {
		return "AWS Configuration";
	}
	/*
	 * (non-Javadoc)
	 * 
	 * @see com.jpmorgan.gcrm.basel.ui.BaseDialog#processButton(java.lang.Object)
	 */
	protected boolean processButton(Object source) {
		boolean processed = super.processButton(source);

		try {			
			// if we didn't process this button in our super class then
			// process it here
			if (!processed) {
				if (source == saveButton) {
					saveSelected();
				}
			}
		}
		catch (Exception e) {
			throw new RuntimeException("Failed trying to open dialog!", e);
		}

		return processed;
	}
	
	
	/**
	 * Process the save button
	 */
	private void saveSelected() {
		// verify our content
		if ((accessId.getText() == null) || (accessId.getText().trim().length() == 0)) {
			this.showMessageBox("You must specify your Access ID", "Error!");
			return;						
		}
		if ((secretKey.getText() == null) || (secretKey.getText().trim().length() == 0)) {
			this.showMessageBox("You must specify your secret key", "Error!");
			return;						
		}
		if ((logURL.getText() == null) || (logURL.getText().trim().length() == 0)) {
			this.showMessageBox("You must specify a log URL", "Error!");
			return;						
		} 		
		if ((jarDir.getText() == null) || (jarDir.getText().trim().length() == 0)) {
			this.showMessageBox("You must specify the jar dir", "Error!");
			return;						
		}

		config.setAwsAccessId(accessId.getText());
		config.setAwsSecretKey(secretKey.getText());
		config.setAwsLogUrl(logURL.getText());
		config.setAwsJarDir(jarDir.getText());
		HadoopConfigFactory configFactory = new HadoopConfigFactory();
		configFactory.saveConfig(config);
		saved = true;
		this.closeDialog();		
	}
	
	/**
	 * Create the controls to display in the dialog
	 */
	protected void createControls(Composite parent) {
		Composite container = createContainer(parent, 1);


		// create a group box for the set of actions we can perform
		Group backupConfigGroup = createGroup(container, "AWS Configuration");
		
		accessId = createText(backupConfigGroup, 300, 0, "Access Id:");
		secretKey = createText(backupConfigGroup, 300, 0, "Secret Key:");
		logURL = createText(backupConfigGroup, 300, 0, "Log URL:");
		jarDir = createText(backupConfigGroup, 300, 0, "Jar Dir:");
		
		if (config.getAwsAccessId() != null) {
			accessId.setText(config.getAwsAccessId());
		}
		
		if (config.getAwsSecretKey() != null) {
			secretKey.setText(config.getAwsSecretKey());
		}
		
		if (config.getAwsLogUrl() != null) {
			logURL.setText(config.getAwsLogUrl());
		}

		if (config.getAwsJarDir() != null) {
			jarDir.setText(config.getAwsJarDir());
		}

		// and add our run and close
		Composite buttonContainer = createContainer(container, 2);
		saveButton = createButton(buttonContainer, "Save");
		
		addCloseButton(buttonContainer);
	}

	/**
	 * Gets the saved
	 * @return boolean The saved
	 */
	public boolean isSaved() {
		return saved;
	}

}
