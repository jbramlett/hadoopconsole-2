/*
 * Copyright 2008 by RBC Capital Markets Inc., All rights reserved.
 * 
 * This source code, and resulting software, is confidential and proprietary
 * information and is the intellectual property of RBC Capital Markets Inc.
 * 
 * You shall not disclose such Proprietary Information and shall use it only in
 * accordance with the terms and conditions of any and all license agreements
 * you have entered into with The Company.
 */
package com.sungard.hadoop.console.gui;

/**
 * 
 *
 * @author jbramlet
 */
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

/**
 * A class to hold a set of global variables for the GUI
 * @author John Bramlett
 */
public class GUIManager {

	public static Shell shell;
	public static Display display;

}
