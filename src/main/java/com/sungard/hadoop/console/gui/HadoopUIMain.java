/*
 * Copyright 2008 by RBC Capital Markets Inc., All rights reserved.
 * 
 * This source code, and resulting software, is confidential and proprietary
 * information and is the intellectual property of RBC Capital Markets Inc.
 * 
 * You shall not disclose such Proprietary Information and shall use it only in
 * accordance with the terms and conditions of any and all license agreements
 * you have entered into with The Company.
 */
package com.sungard.hadoop.console.gui;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

import com.sungard.common.utils.RemoteMessageHandler;
import com.sungard.hadoop.console.profile.HadoopJobFactory;

/**
 * The main class for our gui component - used to launch our
 * window
 *
 * @author jbramlet
 */
public class HadoopUIMain  {
	private static final String kAppName = "Hadoop Console";
		
	/**
	 * Constructor
	 */
	public HadoopUIMain() {
		super();
	}
	
	/**
	 * Runs the app
	 */
	public void run() {
	    RemoteMessageHandler msgHandler = null;
	    try {
    		// initialize our logging
    		initLogging();
    
    		// initialize our socket
    		msgHandler = RemoteMessageHandler.getInstance();
    		
    		GUIManager.display = new Display();
    		Display.setAppName(kAppName);
    
    		GUIManager.shell = new Shell(GUIManager.display, SWT.DIALOG_TRIM | SWT.MIN);
    
    		HadoopJobFactory backupProfileFactory = new HadoopJobFactory();
    		
    		// now get our wizard and let's start this process
    		HadoopMainDialog dlg = new HadoopMainDialog(GUIManager.shell, backupProfileFactory);
    
    		dlg.open();
	    }
	    finally {
	        if (msgHandler != null) {
	            msgHandler.close();
	        }
	    }
	}

	/**
	 * Initializes our logging
	 * 
	 */
	protected void initLogging() {
	    LogConfiguration.configure();
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		HadoopUIMain main = new HadoopUIMain();
		main.run();
	}

}
