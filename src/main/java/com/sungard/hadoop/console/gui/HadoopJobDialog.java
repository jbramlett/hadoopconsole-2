/*
 * Copyright 2008 by RBC Capital Markets Inc., All rights reserved.
 * 
 * This source code, and resulting software, is confidential and proprietary
 * information and is the intellectual property of RBC Capital Markets Inc.
 * 
 * You shall not disclose such Proprietary Information and shall use it only in
 * accordance with the terms and conditions of any and all license agreements
 * you have entered into with The Company.
 */
package com.sungard.hadoop.console.gui;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.Text;

import com.sungard.hadoop.console.profile.HadoopJob;

/**
 * Allows you to edit a backup profile
 *
 * @author jbramlet
 */
public class HadoopJobDialog extends BaseDialog {
	
	protected Text jarFile;
	protected Button jarPicker;
	protected Text mainClass;
	protected Text args;
	protected Button saveButton;
	protected Button runOnAWS;
	protected Text inputDir;
	protected Button dirPicker;
	protected Text s3Dir;
	protected Button awsAlwaysCopy;
	protected Table awsAdditonalCopyItems;
	protected Button addAwsCopyItem;
	protected Button removeAwsCopyItem;
	
	protected HadoopJob profile;
	protected boolean saved = false;
	protected Map<String, String> awsCopyItems;
	
	/**
	 * Constructor
	 * @param shell The shell
	 * @param profile The profile we are editing
	 */
	public HadoopJobDialog(Shell shell, HadoopJob profile) {
		super(shell);
		this.profile = profile;
	}
	/**
	 * Constructor
	 * 
	 * @param shell The owning shell
	 * @param styles The dialog style
	 * @param profile The profile we are editing
	 */
	public HadoopJobDialog(Shell shell, int styles, HadoopJob profile) {
		super(shell, styles);
		this.profile = profile;
	}

	/**
	 * Gets the backup profile for this backup task
	 * @return BackupProfile The profile
	 */
	public HadoopJob getBackupProfile() {
		return profile;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.jpmorgan.gcrm.basel.ui.BaseDialog#getTitle()
	 */
	protected String getTitle() {
		return "Hadoop Job - " + profile.getName();
	}
	/*
	 * (non-Javadoc)
	 * 
	 * @see com.jpmorgan.gcrm.basel.ui.BaseDialog#processButton(java.lang.Object)
	 */
	protected boolean processButton(Object source) {
		boolean processed = super.processButton(source);

		try {			
			// if we didn't process this button in our super class then
			// process it here
			if (!processed) {
				if (source == jarPicker) {
					pickFile(jarFile);
				}
                if (source == dirPicker) {
                    pickDirectory(inputDir);
                }
				else if (source == saveButton) {
					saveSelected();
				}
				else if (source == addAwsCopyItem) {
					addAwsCopyItem();
				}
				else if (source == removeAwsCopyItem) {
					removeAwsCopyItem();
				}
			}
		}
		catch (Exception e) {
			throw new RuntimeException("Failed trying to open dialog!", e);
		}

		return processed;
	}
	
	/**
	 * Adds a new copy item for our aws
	 */
	protected void addAwsCopyItem() {
		CopyItemInputDialog dlg = new CopyItemInputDialog(GUIManager.shell);
		dlg.open();
		if (dlg.getS3Bucket() != null && dlg.getSrcDir() != null) {
			if (awsCopyItems == null) {
				awsCopyItems = new HashMap<String, String>();
			}
			awsCopyItems.put(dlg.getSrcDir(),  dlg.getS3Bucket());
			awsAdditonalCopyItems.removeAll();
			loadCopyItems();
		}
	}
	
	/**
	 * Removes an item from our set of aws copy items
	 */
	protected void removeAwsCopyItem() {
		if (awsAdditonalCopyItems.getSelectionCount() > 0) {
			TableItem[] sis = awsAdditonalCopyItems.getSelection();
			for (TableItem ti : sis) {
				awsCopyItems.remove(ti.getData());
			}
			if (awsCopyItems.size() == 0) {
				awsCopyItems = null;
			}
			awsAdditonalCopyItems.removeAll();
			loadCopyItems();
		}
	}
	
	/**
	 * Process the save button
	 */
	private void saveSelected() {
		// verify our content
		if ((jarFile.getText() == null) || (jarFile.getText().trim().length() == 0)) {
			this.showMessageBox("You must specify a JAR file", "Error!");
			return;						
		}
		if ((mainClass.getText() == null) || (mainClass.getText().trim().length() == 0)) {
			this.showMessageBox("You must specify a main class file", "Error!");
			return;						
		}
 		
		profile.setArgs(args.getText());
		profile.setJarFile(jarFile.getText());
		profile.setMainClass(mainClass.getText());
		profile.setRunOnAWS(runOnAWS.getSelection());
		profile.setInputDataDir(inputDir.getText());
		profile.setS3Dir(s3Dir.getText());
		profile.setAwsAlwaysCopy(awsAlwaysCopy.getSelection());
		profile.setAdditionalCopyItems(awsCopyItems);
		
		saved = true;
		this.closeDialog();		
	}

	/**
	 * Create the controls to display in the dialog
	 */
	protected void createControls(Composite parent) {
		Composite container = createContainer(parent, 1);


		// create a group box for the set of actions we can perform
		Group backupConfigGroup = createGroup(container, "Hadoop Configuration");

		// jar file directory
		Composite tdContainer = createContainer(backupConfigGroup, 3);
		Composite tdTextLabelContainer = createContainer(tdContainer, 2);
		createLabel(tdTextLabelContainer, "JAR File:");
		jarFile = createText(tdTextLabelContainer, 300, 0);
		if (profile.getJarFile() != null) jarFile.setText(profile.getJarFile());
		jarPicker = createButton(tdContainer, "Select", this, 0);

		// main class
		Composite mcTextLabelContainer = createContainer(backupConfigGroup, 2);
		createLabel(mcTextLabelContainer, "Main:");
		mainClass = createText(mcTextLabelContainer, 300, 0);
		if (profile.getMainClass() != null) mainClass.setText(profile.getMainClass());
		
		// args class
		Composite aTextLabelContainer = createContainer(backupConfigGroup, 2);
		createLabel(aTextLabelContainer, "Args:");
		args = createText(aTextLabelContainer, 300, 0);
		if (profile.getArgs() != null) args.setText(profile.getArgs());

		Group awsGroup = createGroup(backupConfigGroup, "AWS");
		runOnAWS = createCheckbox(awsGroup, "Run on AWS EMR");
		runOnAWS.setSelection(profile.isRunOnAWS());
		
		Composite inputDirComposite = createContainer(awsGroup, 2);
		inputDir = createText(inputDirComposite, 300, 0, "Input Data Dir:");
		inputDir.setEnabled(profile.isRunOnAWS());
		if (profile.getInputDataDir() != null) inputDir.setText(profile.getInputDataDir());		
		dirPicker = createButton(inputDirComposite, "Select", this, 0);
		dirPicker.setEnabled(profile.isRunOnAWS());
		s3Dir = createText(awsGroup, 300, 0, "S3 Target Dir:");
		s3Dir.setEnabled(profile.isRunOnAWS());
		if (profile.getS3Dir() != null) s3Dir.setText(profile.getS3Dir());
		awsAlwaysCopy = createCheckbox(awsGroup, "Always Copy");
		awsAlwaysCopy.setSelection(profile.isAwsAlwaysCopy());
		
		Group copyItemsGroup = createGroup(awsGroup, "Additional Items to Copy");
		awsAdditonalCopyItems = createMultiTable(copyItemsGroup, true, 675, 150);
		TableColumn fromColumn = new TableColumn(awsAdditonalCopyItems, SWT.NONE);
		fromColumn.setWidth(200);
		fromColumn.setText("Src Directory");
		TableColumn toBucket = new TableColumn(awsAdditonalCopyItems, SWT.NONE);
		toBucket.setWidth(200);
		toBucket.setText("To Bucket");
		awsCopyItems = profile.getAdditionalCopyItems();
		loadCopyItems();

		Composite additonalCopyItemsButtons = createContainer(copyItemsGroup, 2);
		addAwsCopyItem = createButton(additonalCopyItemsButtons, "Add");
		removeAwsCopyItem = createButton(additonalCopyItemsButtons, "Remove");
		
		runOnAWS.addSelectionListener(new SelectionListener() {
            @Override
            public void widgetDefaultSelected(SelectionEvent arg0) {
                widgetSelected(arg0);
            }

            @Override
            public void widgetSelected(SelectionEvent arg0) {
                s3Dir.setEnabled(runOnAWS.getSelection());
                dirPicker.setEnabled(runOnAWS.getSelection());
                inputDir.setEnabled(runOnAWS.getSelection());
                awsAlwaysCopy.setEnabled(runOnAWS.getSelection());
            }
		    
		});
		
		// and add our run and close
		Composite buttonContainer = createContainer(container, 2);
		saveButton = createButton(buttonContainer, "Save");
		
		addCloseButton(buttonContainer);
	}

	/**
	 * Loads our set of items to copy
	 */
	protected void loadCopyItems() {
		if (awsCopyItems != null) {
			for (String fd : awsCopyItems.keySet()) {
				String b = awsCopyItems.get(fd);
				TableItem ti = new TableItem(awsAdditonalCopyItems, SWT.NONE);
				ti.setText(new String[] {fd, b});
				ti.setData(fd);
			}
		}		
	}
	
	/**
	 * Creates a new button for our set of actions
	 * 
	 * @param parent The parent on which this button will live
	 * @param label The button label
	 * @return Button The newly created button
	 */
	protected Button createButton(Composite parent, String label) {
		return createButton(parent, label, this, 150);
	}
	/**
	 * Gets the saved
	 * @return boolean The saved
	 */
	public boolean isSaved() {
		return saved;
	}

}
