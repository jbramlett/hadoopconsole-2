/*
 * Copyright 2008 by RBC Capital Markets Inc., All rights reserved.
 * 
 * This source code, and resulting software, is confidential and proprietary
 * information and is the intellectual property of RBC Capital Markets Inc.
 * 
 * You shall not disclose such Proprietary Information and shall use it only in
 * accordance with the terms and conditions of any and all license agreements
 * you have entered into with The Company.
 */
package com.sungard.hadoop.console.profile;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;


import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.google.gson.reflect.TypeToken;

/**
 * Factory used to read and write our profiles
 *
 * @author jbramlet
 */
public class HadoopJobFactory {
	private static final Log logger = LogFactory.getLog(HadoopJobFactory.class);
	
    protected JSONUtils<Map<String, HadoopJob>> jsonUtils = new JSONUtils<Map<String, HadoopJob>>(new TypeToken<Map<String, HadoopJob>>(){}.getType());
	private Map<String, HadoopJob> jobs = null;
	private File jobsFile = new File(System.getProperty("user.home") + File.separator + "HadoopConsole" + File.separator + "jobs" + File.separator + "jobs.json");
	
	/**
	 * Constructor
	 */
	public HadoopJobFactory() {
		super();
	}
	
	/**
	 * Lists our jobs
	 * @return List<HadoopJob> Our current set of jobs
	 */
	public synchronized List<HadoopJob> listJobs() {
		if (jobs == null) {
			loadJobs();
		}
		LinkedList<HadoopJob> result = new LinkedList<HadoopJob>(jobs.values());
		Collections.sort(result);
		return result;
	}
		
	/**
	 * Load our jobs from disk
	 */
	protected void loadJobs() {
		try {
			if (jobsFile.exists()) {
				BufferedReader reader = new BufferedReader( new FileReader (jobsFile));
				String         line = null;
				StringBuilder  stringBuilder = new StringBuilder();
				String         ls = System.getProperty("line.separator");
				
				while( ( line = reader.readLine() ) != null ) {
				    stringBuilder.append( line );
				    stringBuilder.append( ls );
				}
				reader.close();
				jobs = jsonUtils.fromJSON(stringBuilder.toString());
			}
			else {
				logger.warn("Profile file does not exist");
				jobs = new HashMap<String, HadoopJob>();
			}
		}
		catch (Exception e) {
			logger.error("Failed to load our profiles!", e);
		}
	}
	
	/**
	 * Creates a new job
	 * @return HadoopJob The new job
	 */
	public HadoopJob createNewJob(String name) {
		return new HadoopJob(name);
	}

	/**
	 * Saves the new job
	 * @param bp The job to save
	 */
	public void saveJob(HadoopJob bp) {
		// and update things in our cache
		jobs.put(bp.getName(), bp);
		saveJobs();
	}
	
	/**
	 * Removes a job
	 * @param bp The job we are removing
	 */
	public void removeJob(HadoopJob bp) {
		logger.debug("Removing profile: " + bp.getName());
		
		// remove this from our internal profile list
		jobs.remove(bp.getName());
		saveJobs();
	}
	
	/**
	 * Saves our jobs
	 */
	protected void saveJobs() {
		String jsonString = jsonUtils.toJSON(jobs);
		try {
			File f = jobsFile.getParentFile();
			f.mkdirs();
			FileWriter fw = new FileWriter(jobsFile);
			fw.write(jsonString);
			fw.close();
		}
		catch (Exception e) {
			logger.error("Failed to save profile changes!", e);
		}
		
	}
}
