/*
 * Copyright 2008 by RBC Capital Markets Inc., All rights reserved.
 * 
 * This source code, and resulting software, is confidential and proprietary
 * information and is the intellectual property of RBC Capital Markets Inc.
 * 
 * You shall not disclose such Proprietary Information and shall use it only in
 * accordance with the terms and conditions of any and all license agreements
 * you have entered into with The Company.
 */
package com.sungard.hadoop.console.profile;

import java.io.Serializable;

/**
 * Represents a hadoop profile
 *
 * @author jbramlet
 */
public class HadoopConfig implements Serializable {

	private static final long serialVersionUID = -6808554007411832845L;

	protected String hadoopCommand = "/usr/local/hadoop/bin/hadoop";
	protected String namenodeWebURL = "http://localhost:50070";
	protected String jobTrackerWebURL = "http://localhost:50030";
	protected String hdfsMaster = "localhost:54310";
	protected String mapReduceMaster = "localhost:54311";
	
	// hbase config
	protected String hbaseDir = "/usr/local/hbase";
	protected String hbaseMasterWebUrl = "http://localhost:60010/master-status";
	protected String hBaseRegionServerWebUrl = "http://localhost:60030/rs-status";
	protected String hbaseZookeeperServer = "localhost";
	protected String hbaseZookeeperPort = "2181";
	
	// aws config
	protected String awsAccessId = "AKIAJYCNN4O5AFR75JPQ";
	protected String awsSecretKey = "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";
	protected String awsLogUrl = "s3://mylog-bramlettny.com/logs";
	protected String awsJarDir = "mapreduce-bramlettny.com/jars";
	
	/**
	 * Constructor
	 */
	public HadoopConfig() {
		super();
	}
	

	/**
	 * Gets the hadoopCommand
	 * @return the hadoopCommand
	 */
	public String getHadoopCommand() {
		return hadoopCommand;
	}


	/**
	 * Sets the hadoopCommand
	 * @param hadoopCommand the hadoopCommand to set
	 */
	public void setHadoopCommand(String hadoopCommand) {
		this.hadoopCommand = hadoopCommand;
	}


	/**
	 * Gets the namenodeWebURL
	 * @return the namenodeWebURL
	 */
	public String getNamenodeWebURL() {
		return namenodeWebURL;
	}


	/**
	 * Sets the namenodeWebURL
	 * @param namenodeWebURL the namenodeWebURL to set
	 */
	public void setNamenodeWebURL(String namenodeWebURL) {
		this.namenodeWebURL = namenodeWebURL;
	}


	/**
	 * Gets the jobTrackerWebURL
	 * @return the jobTrackerWebURL
	 */
	public String getJobTrackerWebURL() {
		return jobTrackerWebURL;
	}


	/**
	 * Sets the jobTrackerWebURL
	 * @param jobTrackerWebURL the jobTrackerWebURL to set
	 */
	public void setJobTrackerWebURL(String jobTrackerWebURL) {
		this.jobTrackerWebURL = jobTrackerWebURL;
	}


	/**
	 * Gets the hdfsMaster
	 * @return the hdfsMaster
	 */
	public String getHdfsMaster() {
		return hdfsMaster;
	}


	/**
	 * Sets the hdfsMaster
	 * @param hdfsMaster the hdfsMaster to set
	 */
	public void setHdfsMaster(String hdfsMaster) {
		this.hdfsMaster = hdfsMaster;
	}


	/**
	 * Gets the mapreduce master
	 * @return the mapreduce master
	 */
	public String getMapReduceMaster() {
		return mapReduceMaster;
	}


	/**
	 * Sets the mapreduce master
	 * @param mapreduce the mapreduce master to set
	 */
	public void setMapReduceMaster(String mapReduceMaster) {
		this.mapReduceMaster = mapReduceMaster;
	}


    /**
     * Returns the hbaseMasterWebUrl
     * @return the hbaseMasterWebUrl
     */
    public String getHbaseMasterWebUrl() {
        return hbaseMasterWebUrl;
    }


    /**
     * Sets hbaseMasterWebUrl
     * @param hbaseMasterWebUrl the hbaseMasterWebUrl to set
     */
    public void setHbaseMasterWebUrl(String hbaseMasterWebUrl) {
        this.hbaseMasterWebUrl = hbaseMasterWebUrl;
    }


    /**
     * Returns the hBaseRegionServerWebUrl
     * @return the hBaseRegionServerWebUrl
     */
    public String gethBaseRegionServerWebUrl() {
        return hBaseRegionServerWebUrl;
    }


    /**
     * Sets hBaseRegionServerWebUrl
     * @param hBaseRegionServerWebUrl the hBaseRegionServerWebUrl to set
     */
    public void sethBaseRegionServerWebUrl(String hBaseRegionServerWebUrl) {
        this.hBaseRegionServerWebUrl = hBaseRegionServerWebUrl;
    }


    /**
     * Returns the hbaseZookeeperServer
     * @return the hbaseZookeeperServer
     */
    public String getHbaseZookeeperServer() {
        return hbaseZookeeperServer;
    }


    /**
     * Sets hbaseZookeeperServer
     * @param hbaseZookeeperServer the hbaseZookeeperServer to set
     */
    public void setHbaseZookeeperServer(String hbaseZookeeperServer) {
        this.hbaseZookeeperServer = hbaseZookeeperServer;
    }


    /**
     * Returns the hbaseZookeeperPort
     * @return the hbaseZookeeperPort
     */
    public String getHbaseZookeeperPort() {
        return hbaseZookeeperPort;
    }


    /**
     * Sets hbaseZookeeperPort
     * @param hbaseZookeeperPort the hbaseZookeeperPort to set
     */
    public void setHbaseZookeeperPort(String hbaseZookeeperPort) {
        this.hbaseZookeeperPort = hbaseZookeeperPort;
    }


	/**
	 * Gets the hbaseDir
	 * @return the hbaseDir
	 */
	public String getHbaseDir() {
		return hbaseDir;
	}


	/**
	 * Sets the hbaseDir
	 * @param hbaseDir the hbaseDir to set
	 */
	public void setHbaseDir(String hbaseDir) {
		this.hbaseDir = hbaseDir;
	}


    /**
     * Returns the awsAccessId
     * @return the awsAccessId
     */
    public String getAwsAccessId() {
        return awsAccessId;
    }


    /**
     * Sets awsAccessId
     * @param awsAccessId the awsAccessId to set
     */
    public void setAwsAccessId(String awsAccessId) {
        this.awsAccessId = awsAccessId;
    }


    /**
     * Returns the awsSecretKey
     * @return the awsSecretKey
     */
    public String getAwsSecretKey() {
        return awsSecretKey;
    }


    /**
     * Sets awsSecretKey
     * @param awsSecretKey the awsSecretKey to set
     */
    public void setAwsSecretKey(String awsSecretKey) {
        this.awsSecretKey = awsSecretKey;
    }


    /**
     * Returns the awsLogUrl
     * @return the awsLogUrl
     */
    public String getAwsLogUrl() {
        return awsLogUrl;
    }


    /**
     * Sets awsLogUrl
     * @param awsLogUrl the awsLogUrl to set
     */
    public void setAwsLogUrl(String awsLogUrl) {
        this.awsLogUrl = awsLogUrl;
    }


    /**
     * Returns the awsJarDir
     * @return the awsJarDir
     */
    public String getAwsJarDir() {
        return awsJarDir;
    }


    /**
     * Sets awsJarDir
     * @param awsJarDir the awsJarDir to set
     */
    public void setAwsJarDir(String awsJarDir) {
        this.awsJarDir = awsJarDir;
    }
	
	
}
