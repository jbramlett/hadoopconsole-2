/*
 * Copyright 2008 by RBC Capital Markets Inc., All rights reserved.
 * 
 * This source code, and resulting software, is confidential and proprietary
 * information and is the intellectual property of RBC Capital Markets Inc.
 * 
 * You shall not disclose such Proprietary Information and shall use it only in
 * accordance with the terms and conditions of any and all license agreements
 * you have entered into with The Company.
 */
package com.sungard.hadoop.console.profile;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Represents a hadoop profile
 *
 * @author jbramlet
 */
public class HadoopJob implements Serializable, Comparable<HadoopJob> {

	private static final long serialVersionUID = -6808554007411832845L;

	protected String name;
	protected String jarFile;
	protected String mainClass;
	protected String args;
	
	protected boolean runOnAWS;
	protected String inputDataDir;
	protected String s3Dir;
	protected boolean awsAlwaysCopy;
	protected Map<String, String> additionalCopyItems;
	
	/**
	 * Constructor
	 */
	public HadoopJob() {
		super();
	}
	/**
	 * Constructor
	 */
	public HadoopJob(String name) {
		super();
		this.name = name;
	}
	/**
	 * Gets the name
	 * @return String The name
	 */
	public String getName() {
		return name;
	}
	/**
	 * Sets the name
	 * @param name The name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * Gets the jarFile
	 * @return String The jarFile
	 */
	public String getJarFile() {
		return jarFile;
	}
	/**
	 * Sets the jarFile
	 * @param jarFile The jarFile to set
	 */
	public void setJarFile(String jarFile) {
		this.jarFile = jarFile;
	}
	/**
	 * Gets the mainClass
	 * @return String The mainClass
	 */
	public String getMainClass() {
		return mainClass;
	}
	/**
	 * Sets the mainClass
	 * @param mainClass The mainClass to set
	 */
	public void setMainClass(String mainClass) {
		this.mainClass = mainClass;
	}
	/**
	 * Gets the args
	 * @return String The args
	 */
	public String getArgs() {
		return args;
	}
	/**
	 * Sets the args
	 * @param args The args to set
	 */
	public void setArgs(String args) {
		this.args = args;
	}
	
	/**
     * Returns the runOnAWS
     * @return the runOnAWS
     */
    public boolean isRunOnAWS() {
        return runOnAWS;
    }
    /**
     * Sets runOnAWS
     * @param runOnAWS the runOnAWS to set
     */
    public void setRunOnAWS(boolean runOnAWS) {
        this.runOnAWS = runOnAWS;
    }
    /**
     * Returns the inputDataDir
     * @return the inputDataDir
     */
    public String getInputDataDir() {
        return inputDataDir;
    }
    /**
     * Sets inputDataDir
     * @param inputDataDir the inputDataDir to set
     */
    public void setInputDataDir(String inputDataDir) {
        this.inputDataDir = inputDataDir;
    }
    /**
     * Returns the s3Dir
     * @return the s3Dir
     */
    public String getS3Dir() {
        return s3Dir;
    }
    /**
     * Sets s3Dir
     * @param s3Dir the s3Dir to set
     */
    public void setS3Dir(String s3Dir) {
        this.s3Dir = s3Dir;
    }
    /* (non-Javadoc)
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(HadoopJob o) {
		return name.compareTo(o.name);
	}		
	
	/**
	 * Returns the set of commands used to run this job
	 * @return List<String> The set commands
	 */
	public List<String> getProcessCommand() {
		List<String> command = new ArrayList<String>();
		
		HadoopConfigFactory configFactory = new HadoopConfigFactory();
		String hadoopCommand = configFactory.getConfig().getHadoopCommand();
		
		command.add(hadoopCommand);
		command.add("jar");
		command.add(jarFile);
		command.add(mainClass);
		if ((args != null) && (args.length() > 0)) {
			String[] argsArray = args.split(" ");
			for (String s : argsArray) {
				command.add(s);
			}
		}
		return command;
	}
	/**
	 * Gets the awsAlwaysCopy
	 * @return the awsAlwaysCopy
	 */
	public boolean isAwsAlwaysCopy() {
		return awsAlwaysCopy;
	}
	/**
	 * Sets the awsAlwaysCopy
	 * @param awsAlwaysCopy the awsAlwaysCopy to set
	 */
	public void setAwsAlwaysCopy(boolean awsAlwaysCopy) {
		this.awsAlwaysCopy = awsAlwaysCopy;
	}
	/**
	 * Gets the additionalCopyItems
	 * @return the additionalCopyItems
	 */
	public Map<String, String> getAdditionalCopyItems() {
		return additionalCopyItems;
	}
	/**
	 * Sets the additionalCopyItems
	 * @param additionalCopyItems the additionalCopyItems to set
	 */
	public void setAdditionalCopyItems(Map<String, String> additionalCopyItems) {
		this.additionalCopyItems = additionalCopyItems;
	}
}
