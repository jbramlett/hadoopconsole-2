/*
 * Copyright 2008 by RBC Capital Markets Inc., All rights reserved.
 * 
 * This source code, and resulting software, is confidential and proprietary
 * information and is the intellectual property of RBC Capital Markets Inc.
 * 
 * You shall not disclose such Proprietary Information and shall use it only in
 * accordance with the terms and conditions of any and all license agreements
 * you have entered into with The Company.
 */
package com.sungard.hadoop.console.profile;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;


import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;


/**
 * Factory used to read and write our configuration
 *
 * @author jbramlet
 */
public class HadoopConfigFactory {
	private static final Log logger = LogFactory.getLog(HadoopConfigFactory.class);
	
    protected JSONUtils<HadoopConfig> jsonUtils = new JSONUtils<HadoopConfig>(HadoopConfig.class);
	private HadoopConfig config;
	private File configFile = new File(System.getProperty("user.home") + File.separator + "HadoopConsole" + File.separator + "config" + File.separator + "config.json");
	
	/**
	 * Constructor
	 */
	public HadoopConfigFactory() {
		super();
	}
	
	/**
	 * Gets our config
	 * @return HadoopConfig The config
	 */
	public synchronized HadoopConfig getConfig() {
		if (config == null) {
			loadConfig();
		}
		return config;
	}
		
	/**
	 * Load our config from disk
	 */
	protected void loadConfig() {
		try {
			if (configFile.exists()) {
				BufferedReader reader = new BufferedReader( new FileReader (configFile));
				String         line = null;
				StringBuilder  stringBuilder = new StringBuilder();
				String         ls = System.getProperty("line.separator");
				
				while( ( line = reader.readLine() ) != null ) {
				    stringBuilder.append( line );
				    stringBuilder.append( ls );
				}
				reader.close();
				config = jsonUtils.fromJSON(stringBuilder.toString());
			}
			else {
				logger.warn("Config file does not exist");
				config = new HadoopConfig();
			}
		}
		catch (Exception e) {
			logger.error("Failed to load our config!", e);
		}
	}
	
	/**
	 * Saves the config
	 * @param bp The config to save
	 */
	public void saveConfig(HadoopConfig bp) {
		// and update things in our cache
		config = bp;
		saveConfig();
	}
	
	/**
	 * Saves our config
	 */
	protected void saveConfig() {
		String jsonString = jsonUtils.toJSON(config);
		try {
			File f = configFile.getParentFile();
			f.mkdirs();
			FileWriter fw = new FileWriter(configFile, false);
			fw.write(jsonString);
			fw.close();
		}
		catch (Exception e) {
			logger.error("Failed to save config changes!", e);
		}
		
	}
}
