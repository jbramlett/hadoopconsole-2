/*
 * @(#)HadoopShellJob.java
 *
 * Copyright 2013 Sungard and/or its affiliates. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or 
 * without modification, are permitted provided that the following 
 * conditions are met:
 * 
 * -Redistributions of source code must retain the above copyright  
 * notice, this  list of conditions and the following disclaimer.
 * 
 * -Redistribution in binary form must reproduct the above copyright 
 * notice, this list of conditions and the following disclaimer in 
 * the documentation and/or other materials provided with the 
 * distribution.
 * 
 * Neither the name of Sungard and/or its affiliates. or the names of 
 * contributors may be used to endorse or promote products derived 
 * from this software without specific prior written permission.
 * 
 * This software is provided "AS IS," without a warranty of any 
 * kind. ALL EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND 
 * WARRANTIES, INCLUDING ANY IMPLIED WARRANTY OF MERCHANTABILITY, 
 * FITNESS FOR A PARTICULAR PURPOSE OR NON-INFRINGEMENT, ARE HEREBY 
 * EXCLUDED. SUN AND ITS LICENSORS SHALL NOT BE LIABLE FOR ANY 
 * DAMAGES OR LIABILITIES  SUFFERED BY LICENSEE AS A RESULT OF  OR 
 * RELATING TO USE, MODIFICATION OR DISTRIBUTION OF THE SOFTWARE OR 
 * ITS DERIVATIVES. IN NO EVENT WILL SUN OR ITS LICENSORS BE LIABLE 
 * FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR DIRECT, INDIRECT, 
 * SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES, HOWEVER 
 * CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING OUT OF 
 * THE USE OF OR INABILITY TO USE SOFTWARE, EVEN IF SUN HAS BEEN 
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 * 
 * You acknowledge that Software is not designed, licensed or 
 * intended for use in the design, construction, operation or 
 * maintenance of any nuclear facility. 
 */
package com.sungard.hadoop.console.profile;

import java.util.ArrayList;
import java.util.List;

import org.apache.hadoop.fs.Path;

/**
 * Basic override of our HadoopJob that let's us run the hadoop shell commands for start
 * and stop
 * @author jbramlett
 */
public class HBaseShellJob extends HadoopJob {
	private static final long serialVersionUID = 1457151785063498318L;
	
	protected String script;
	
	/**
	 * Constructor
	 */
	public HBaseShellJob() {
		super();
	}

	/**
	 * Constructor
	 * @param name
	 */
	public HBaseShellJob(String name) {
		super(name);
		if (name.equals("Start HBase")) {
			script = "start-hbase.sh";
		}
		else {
			script = "stop-hbase.sh";
		}
	}

	/* (non-Javadoc)
	 * @see com.sungard.hadoop.console.profile.HadoopJob#getProcessCommand()
	 */
	@Override
	public List<String> getProcessCommand() {
		List<String> command = new ArrayList<String>();
		
		HadoopConfigFactory configFactory = new HadoopConfigFactory();
		String hadoopCommand = configFactory.getConfig().getHbaseDir();
		hadoopCommand = hadoopCommand + Path.SEPARATOR + "bin" + Path.SEPARATOR + script;
		command.add(hadoopCommand);
		return command;
	}

}
