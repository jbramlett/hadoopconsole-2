/*
 * @(#)RemoteMessageHandler.java
 *
 * Copyright 2013 Sungard and/or its affiliates. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or 
 * without modification, are permitted provided that the following 
 * conditions are met:
 * 
 * -Redistributions of source code must retain the above copyright  
 * notice, this  list of conditions and the following disclaimer.
 * 
 * -Redistribution in binary form must reproduct the above copyright 
 * notice, this list of conditions and the following disclaimer in 
 * the documentation and/or other materials provided with the 
 * distribution.
 * 
 * Neither the name of Sungard and/or its affiliates. or the names of 
 * contributors may be used to endorse or promote products derived 
 * from this software without specific prior written permission.
 * 
 * This software is provided "AS IS," without a warranty of any 
 * kind. ALL EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND 
 * WARRANTIES, INCLUDING ANY IMPLIED WARRANTY OF MERCHANTABILITY, 
 * FITNESS FOR A PARTICULAR PURPOSE OR NON-INFRINGEMENT, ARE HEREBY 
 * EXCLUDED. SUN AND ITS LICENSORS SHALL NOT BE LIABLE FOR ANY 
 * DAMAGES OR LIABILITIES  SUFFERED BY LICENSEE AS A RESULT OF  OR 
 * RELATING TO USE, MODIFICATION OR DISTRIBUTION OF THE SOFTWARE OR 
 * ITS DERIVATIVES. IN NO EVENT WILL SUN OR ITS LICENSORS BE LIABLE 
 * FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR DIRECT, INDIRECT, 
 * SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES, HOWEVER 
 * CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING OUT OF 
 * THE USE OF OR INABILITY TO USE SOFTWARE, EVEN IF SUN HAS BEEN 
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 * 
 * You acknowledge that Software is not designed, licensed or 
 * intended for use in the design, construction, operation or 
 * maintenance of any nuclear facility. 
 */
package com.sungard.common.utils;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Class used to handler remote messages
 * @author sungard
 */
public final class RemoteMessageHandler {

    private static RemoteMessageHandler instance = null;
    private static Log logger = LogFactory.getLog(RemoteMessageHandler.class);
    
    private int socketPort = 1111;
    private ServerSocket providerSocket;
    private List<IRemoteMessageReceiver> handlers = new ArrayList<IRemoteMessageReceiver>();
    private boolean running = true;
    
    /**
     * Gets our singleton instance
     * @return RemoteMessageHandler The singleton instance
     */
    public static RemoteMessageHandler getInstance() {
        if (instance == null) {
            instance = new RemoteMessageHandler();
        }
        return instance;
    }
    
    /**
     * Constructor - singleton
     */
    private RemoteMessageHandler() {
        try {
            providerSocket = new ServerSocket(socketPort);
            
            // start our thread to listen for connections
            new Thread(new Runnable() {
                public void run() {
                    try {
                        while (running) {
                            final Socket s = providerSocket.accept();
                            new Thread(new StatusHandler(s), "ClientSConnection").start();
                        }                   
                    }
                    catch (Exception e) {
                        logger.error(e);
                    }
                }
            }, "SocketConnectionListener").start();
                    
        }
        catch (Exception e) {
            logger.error("Failed to initialize our server socket", e);
        }
    }
    
    /**
     * Adds a new receiver to our set of remote message receivers
     * @param receiver The receiver we are adding
     */
    public void addReceiver(IRemoteMessageReceiver receiver) {
        handlers.add(receiver);
    }
    
    /**
     * Removes a new receiver to our set of remote message receivers
     * @param receiver The receiver we are adding
     */
    public void removeReceiver(IRemoteMessageReceiver receiver) {
        handlers.remove(receiver);
    }

    /**
     * Send notifications to our receivers
     * @param msg The message to send
     */
    public void notifyReceivers(String msg) {
        for (IRemoteMessageReceiver r : handlers) {
            r.updateMessage(msg);
        }
    }
    
    /**
     * Closes our socket
     */
    public void close() {
        try {
            running = false;
            providerSocket.close();
        }
        catch (Exception e) {
            logger.error("Failed to close our socket!",e);
        }
        providerSocket = null;
    }

    class StatusHandler implements Runnable {
        private Socket s;
        public StatusHandler(Socket s) {
            this.s = s;
        }
        
        public void run() {
            try{
                BufferedReader in = new BufferedReader(new InputStreamReader (s.getInputStream()));               
                
                String message = null;
                do{
                    message = in.readLine();
                    if ((message.length() > 0)  && !message.equals("goodbye")) {
                        notifyReceivers(message);
                    }
                }while(!message.equals("goodbye") && running);
            }
            catch(Exception e){
                logger.error("Failed process socket", e);
            }
                
        }
    }
    
    
}
