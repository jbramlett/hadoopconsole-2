/*
 * @(#)AWSS3Utils.java
 *
 * Copyright 2013 Sungard and/or its affiliates. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or 
 * without modification, are permitted provided that the following 
 * conditions are met:
 * 
 * -Redistributions of source code must retain the above copyright  
 * notice, this  list of conditions and the following disclaimer.
 * 
 * -Redistribution in binary form must reproduct the above copyright 
 * notice, this list of conditions and the following disclaimer in 
 * the documentation and/or other materials provided with the 
 * distribution.
 * 
 * Neither the name of Sungard and/or its affiliates. or the names of 
 * contributors may be used to endorse or promote products derived 
 * from this software without specific prior written permission.
 * 
 * This software is provided "AS IS," without a warranty of any 
 * kind. ALL EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND 
 * WARRANTIES, INCLUDING ANY IMPLIED WARRANTY OF MERCHANTABILITY, 
 * FITNESS FOR A PARTICULAR PURPOSE OR NON-INFRINGEMENT, ARE HEREBY 
 * EXCLUDED. SUN AND ITS LICENSORS SHALL NOT BE LIABLE FOR ANY 
 * DAMAGES OR LIABILITIES  SUFFERED BY LICENSEE AS A RESULT OF  OR 
 * RELATING TO USE, MODIFICATION OR DISTRIBUTION OF THE SOFTWARE OR 
 * ITS DERIVATIVES. IN NO EVENT WILL SUN OR ITS LICENSORS BE LIABLE 
 * FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR DIRECT, INDIRECT, 
 * SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES, HOWEVER 
 * CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING OUT OF 
 * THE USE OF OR INABILITY TO USE SOFTWARE, EVEN IF SUN HAS BEEN 
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 * 
 * You acknowledge that Software is not designed, licensed or 
 * intended for use in the design, construction, operation or 
 * maintenance of any nuclear facility. 
 */
package com.sungard.common.utils;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.Bucket;
import com.amazonaws.services.s3.model.ObjectListing;
import com.amazonaws.services.s3.model.ProgressListener;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import com.amazonaws.services.s3.transfer.TransferManager;
import com.amazonaws.services.s3.transfer.Upload;

/**
 * A wrapper around amazon s3 API
 * @author jbramlett
 */
public class AWSS3Utils {
	private static final Log logger = LogFactory.getLog(AWSS3Utils.class);
	
    protected String accessId;
    protected String secretKey;
    protected BasicAWSCredentials credentials;
    protected AmazonS3Client s3;
    
	
	
	/**
	 * Constructor
	 */
	public AWSS3Utils(String accessId, String secretKey) {
		this.accessId = accessId;
		this.secretKey = secretKey;
		init();
	}
	
	/**
	 * Initialize our s3 instance
	 */
	private void init() {
        credentials = new BasicAWSCredentials(accessId, secretKey);
		s3 = new AmazonS3Client(credentials);
	}
	
	/**
	 * Returns a list of our buckets
	 * @return List<String> The set of buckets
	 */
	public List<String> listBuckets() {
	    List<String> result = null;
	    
	    // read in our bucket data
	    List<Bucket> buckets = s3.listBuckets();
	    result = new ArrayList<String>(buckets.size());
	    for (Bucket b : buckets) {
	        result.add(b.getName());
	    }
	    return result;
	}
	
	/**
	 * Lists the contents of the bucket - this will be a list containing 
	 * strings and maps, the strings are bucket content and the maps represent
	 * "folders" within the bucket (which in turn might contain content and sub-folders)
	 * @param bucket The bucket we are retrieving the data for
	 * @return List The bucket contents
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
    public Map<String, Object> listBucketContents(String bucket) {
	    Map<String, Object> result = new HashMap<String, Object>();
	    
	    ObjectListing bo = s3.listObjects(bucket);
        List<S3ObjectSummary> objects = bo.getObjectSummaries();
        for (S3ObjectSummary os : objects) {
            String key = os.getKey();
            
            String[] folders = key.split("/");
            Map level = result;
            for (int i = 0; i < folders.length - 1; i++) {
                Map newLevel = (Map)level.get(folders[i]);
                if (newLevel == null) {
                    newLevel = new HashMap();
                    level.put(folders[i], newLevel);
                }
                level = newLevel;
            }
            if (os.getSize() > 0) {
                level.put(folders[folders.length - 1], os);
            }
            else {
                level.put(folders[folders.length - 1], new HashMap());
            }
        }
	    
	    return result;
	}
	
	/**
	 * Creates the given bucket
	 * @param bucket The bucket we are creating
	 * @return boolean Returns true if the create was successful
	 */
	public boolean create(String bucket) {
		try {
			String awsBucket = getAWSBucket(bucket);
			s3.createBucket(awsBucket);
			return true;
		}
		catch (AmazonServiceException e) {
		    logger.error("Failed to create bucket " + bucket, e);
		    return false;
		}		
	}
	
	/**
	 * Returns a flag if the given item exists in the given bucket
	 * @param bucket The bucket we are looking in
	 * @param objectName The name of the item we are looking for
	 * @return boolean Returns true if the item exits, false otherwise
	 */
	public boolean exists(String bucket, String objectName) {
		try {
			String awsBucket = getAWSBucket(bucket);
			String folder = getFolder(bucket);
			s3.getObjectMetadata(awsBucket, folder + objectName);
			return true;
		}
		catch (AmazonServiceException e) {
		    String errorMsg = e.getMessage();
		    if (!errorMsg.equals("Not Found")) {
		        logger.error("Unexpected error occurred calling AWS!", e);
		        throw e;
		    }
		    return false;
		}
	}
	
	/**
	 * Checks to see if the given bucket exists
	 * @param bucket The bucket we are checking
	 * @return boolean Returns true if the bucket exists, false otherwise
	 */
	public boolean exists(String bucket) {
		String awsBucket = getAWSBucket(bucket);
		String folder = getFolder(bucket);

		if (!s3.doesBucketExist(awsBucket)) {
			return false;
		}
		else {
			// now verify the folder
			if (folder != null) {
	        	ObjectListing bo = s3.listObjects(awsBucket);
	        	List<S3ObjectSummary> objects = bo.getObjectSummaries();
	        	for (S3ObjectSummary o : objects) {
        			if (o.getKey().startsWith(folder)) {
        				return true;
        			}
	        	}
				
			}
		}
		return false;
	}
	
	/**
	 * Removes the given object from our bucket
	 * @param bucket The bucket we are looking in
	 * @param objectName The object to remove
	 * @return boolean Returns true if it worked, false otherwise
	 */
	public boolean delete(String bucket, String objectName) {
        try {
    		String awsBucket = getAWSBucket(bucket);
    		String folder = getFolder(bucket);
        	
            s3.deleteObject(awsBucket, folder + objectName);
            return true;
        }
        catch (Exception e) {
            logger.error("Failed to delete " + objectName + " from " + bucket, e);
            return false;
        }		
	}
	
	/**
	 * Deletes all of the objects in the given bucket
	 * @param bucket The bucket we are looking in
	 * @return boolean Returns true if it worked, false otherwise
	 */
	public boolean deleteAllObjects(String bucket) {		
        try {
    		String awsBucket = getAWSBucket(bucket);
    		String folder = getFolder(bucket);

    		ObjectListing bo = s3.listObjects(awsBucket);
        	List<S3ObjectSummary> objects = bo.getObjectSummaries();
        	for (S3ObjectSummary o : objects) {
        		try {
        			if (o.getKey().startsWith(folder)) {
        				s3.deleteObject(o.getBucketName(), o.getKey());
        			}
        		}
        		catch (Exception e) {
        			logger.error("Failed to delete " + o.getKey() + " from " + o.getBucketName());
        			return false;
        		}
        	}
        	        	
            return true;
        }
        catch (Exception e) {
            logger.error("Failed to delete all objects from " + bucket, e);
            return false;
        }		
	}

	/**
	 * Deletes the given bucket
	 * @param bucket The bucket to delete
	 * @return boolean Returns true if the bucket was deleted, false otherwise
	 */
	public boolean delete(String bucket) {
        try {
    		String awsBucket = getAWSBucket(bucket);

    		deleteAllObjects(bucket);
            s3.deleteBucket(awsBucket);
            return true;
        }
        catch (Exception e) {
            logger.error("Failed to delete " + bucket, e);
            return false;
        }				
	}
	
	/**
	 * Copies the local file to the given bucket
	 * @param localFile The file we are copying
	 * @param bucket The bucket we are copying to
	 * @param listener The listener used to receive status events
	 */
	public boolean copy(File localFile, String bucket, ProgressListener listener) {
		TransferManager tm = null;
		try {
    		String awsBucket = getAWSBucket(bucket);
    		String folder = getFolder(bucket);
			
			if (!exists(awsBucket)) {
				create(awsBucket);
			}
	        tm = new TransferManager(credentials);
	        Upload myUpload = tm.upload(awsBucket, folder + localFile.getName(), localFile);
	        if (listener != null) {
	        	myUpload.addProgressListener(listener);
	        }
	        
	        try {
	        	myUpload.waitForCompletion();
	            return true;
	        }
	        catch (InterruptedException ie) {
	        	logger.error("Upload was interrupted!", ie);
	        	return false;
	        }
		}
		catch (Exception e) {
			logger.error("Failed to copy file " + localFile.getAbsolutePath() + " to bucket " + bucket, e);
			return false;
		}
		finally {
			tm.shutdownNow();
		}
        		
	}

	/**
	 * Copies all of the files in the given dir to the specified bucket
	 * @param localDir The directory we are copying from
	 * @param bucket The bucket we are copying to
	 * @param listener The listener used to receive status events
	 */
	public boolean copyAll(File localDir, String bucket, ProgressListener listener) {
		try {
    		String awsBucket = getAWSBucket(bucket);

    		if (!exists(awsBucket)) {
				create(awsBucket);
			}

    		File[] dirContents = localDir.listFiles();
    		for (File f: dirContents) {
    			if (f.isFile()) {
    				if (!copy(f, bucket, listener)) {
    					return false;
    				}
    			}
    		}
    		return true;
		}
		catch (Exception e) {
			logger.error("Failed to directory contents from " + localDir.getAbsolutePath() + " to bucket " + bucket, e);
			return false;
		}
        		
	}
	
	/**
	 * Gets the real bucket name from string (our buckets use a combo of bucket + folder
	 * @param bucket The bucket name (bucket + folder)
	 * @return String The AWS bucket
	 */
	private String getAWSBucket(String bucket) {
		if (bucket.startsWith("s3://")) {
			bucket = bucket.substring("s3://".length());
		}
		String[] bucketSplit = bucket.split("/");
		return bucketSplit[0];
	}

	/**
	 * Gets the folder name from string (our buckets use a combo of bucket + folder
	 * @param bucket The bucket name (bucket + folder)
	 * @return String The folder structure under the bucket
	 */
	private String getFolder(String bucket) {
		String folder = null;
		String[] bucketSplit = bucket.split("/");
		if (bucketSplit.length > 1) {
			folder = bucketSplit[1];
			for (int i = 2; i < bucketSplit.length; i++) {
				folder = folder + "/" + bucketSplit[i];
			}
			folder = folder + "/";
		}
		return folder;
	}

}
