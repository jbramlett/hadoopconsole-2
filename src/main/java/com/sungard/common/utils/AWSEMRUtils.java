/*
 * @(#)AWSEMRUtils.java
 *
 * Copyright 2013 Sungard and/or its affiliates. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or 
 * without modification, are permitted provided that the following 
 * conditions are met:
 * 
 * -Redistributions of source code must retain the above copyright  
 * notice, this  list of conditions and the following disclaimer.
 * 
 * -Redistribution in binary form must reproduct the above copyright 
 * notice, this list of conditions and the following disclaimer in 
 * the documentation and/or other materials provided with the 
 * distribution.
 * 
 * Neither the name of Sungard and/or its affiliates. or the names of 
 * contributors may be used to endorse or promote products derived 
 * from this software without specific prior written permission.
 * 
 * This software is provided "AS IS," without a warranty of any 
 * kind. ALL EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND 
 * WARRANTIES, INCLUDING ANY IMPLIED WARRANTY OF MERCHANTABILITY, 
 * FITNESS FOR A PARTICULAR PURPOSE OR NON-INFRINGEMENT, ARE HEREBY 
 * EXCLUDED. SUN AND ITS LICENSORS SHALL NOT BE LIABLE FOR ANY 
 * DAMAGES OR LIABILITIES  SUFFERED BY LICENSEE AS A RESULT OF  OR 
 * RELATING TO USE, MODIFICATION OR DISTRIBUTION OF THE SOFTWARE OR 
 * ITS DERIVATIVES. IN NO EVENT WILL SUN OR ITS LICENSORS BE LIABLE 
 * FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR DIRECT, INDIRECT, 
 * SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES, HOWEVER 
 * CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING OUT OF 
 * THE USE OF OR INABILITY TO USE SOFTWARE, EVEN IF SUN HAS BEEN 
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 * 
 * You acknowledge that Software is not designed, licensed or 
 * intended for use in the design, construction, operation or 
 * maintenance of any nuclear facility. 
 */
package com.sungard.common.utils;

import java.util.Arrays;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.ec2.model.InstanceType;
import com.amazonaws.services.elasticmapreduce.AmazonElasticMapReduceClient;
import com.amazonaws.services.elasticmapreduce.model.DescribeJobFlowsRequest;
import com.amazonaws.services.elasticmapreduce.model.DescribeJobFlowsResult;
import com.amazonaws.services.elasticmapreduce.model.HadoopJarStepConfig;
import com.amazonaws.services.elasticmapreduce.model.JobFlowDetail;
import com.amazonaws.services.elasticmapreduce.model.JobFlowExecutionState;
import com.amazonaws.services.elasticmapreduce.model.JobFlowInstancesConfig;
import com.amazonaws.services.elasticmapreduce.model.RunJobFlowRequest;
import com.amazonaws.services.elasticmapreduce.model.RunJobFlowResult;
import com.amazonaws.services.elasticmapreduce.model.StepConfig;

/**
 * Wrapper used to manage the interactions with AWS EMR
 * 
 * @author sungard
 */
public class AWSEMRUtils {
    private static final Log logger = LogFactory.getLog(AWSEMRUtils.class);
    
    protected String hadoopVersion = "1.0.3";
    protected String masterInstanceType = InstanceType.M1Small.toString();
    protected String slaveInstanceType = InstanceType.M1Small.toString();
    protected Integer instanceCount = new Integer(4);
    protected String logUrl;
    protected Integer statusInterval = new Integer(10000);
             
    protected String accessId;
    protected String secretKey;
    protected AmazonElasticMapReduceClient client;
    protected BasicAWSCredentials credentials;
    
    /**
     * Constructor
     * @param accessId The access id for EMR
     * @param secretKey The secret key
     */
    public AWSEMRUtils(String accessId, String secretKey, String logUrl) {
        this.accessId = accessId;
        this.secretKey = secretKey;
        this.logUrl = logUrl;
        init();
    }

    /**
     * Initialize our client
     */
    protected void init() {
        credentials = new BasicAWSCredentials(accessId, secretKey);
        client = new AmazonElasticMapReduceClient(credentials);        
    }

    /**
     * Runs a Hadoop job on AWS EMR
     * @param name The name of the job
     * @param jar The jar file we are running
     * @param mainClass The main class in the jar
     * @param args The arguments to pass to the job
     * @param listener The listener we are sending alerts to (can be null)
     */
    public void runJob(String name, String jar, String mainClass, String[] args, AWSEMRListener listener) {
        try {
            RunJobFlowRequest request = new RunJobFlowRequest(name, getJobFlowConfig());
            request.setLogUri(getURL(logUrl));
    
            // Configure the Hadoop jar to use
            HadoopJarStepConfig jarConfig = new HadoopJarStepConfig(getURL(jar));
            jarConfig.setArgs(Arrays.asList(args));
            jarConfig.setMainClass(mainClass);
            String[] mainClassSplit = mainClass.split("\\.");
            String stepName = mainClassSplit[mainClassSplit.length - 1];
            StepConfig stepConfig = new StepConfig(stepName, jarConfig);
            request.setSteps(Arrays.asList(new StepConfig[] { stepConfig }));
            
            //Run the job flow
            RunJobFlowResult result = client.runJobFlow(request);
            String jobId = result.getJobFlowId();
            JobFlowExecutionState previousState = JobFlowExecutionState.WAITING;
            STATUS_LOOP: while (true)
            {
                DescribeJobFlowsRequest desc = new DescribeJobFlowsRequest(Arrays.asList(new String[] { result.getJobFlowId() }));
                DescribeJobFlowsResult descResult = client.describeJobFlows(desc);
                for (JobFlowDetail detail : descResult.getJobFlows())
                {
                    String state = detail.getExecutionStatusDetail().getState();
                    JobFlowExecutionState jobState = JobFlowExecutionState.fromValue(state);
                    
                    // if our job state hasn't changed then do nothing
                    if (jobState == previousState) {
                        continue;
                    }
                    previousState = jobState;
                    
                    // if we haven't sent out an initial starting event then send alert
                    // our listener
                    if (jobState == JobFlowExecutionState.STARTING) {
                        logger.info("Job " + jobId + " starting");
                        if (listener != null) 
                            listener.jobStarting(jobId);
                    }
                    else if (jobState == JobFlowExecutionState.COMPLETED) {
                        logger.info("Job " + jobId + " has completed");
                        if (listener != null)
                            listener.jobComplete(jobId);
                        break STATUS_LOOP;
                    }
                    else if (jobState == JobFlowExecutionState.FAILED) {
                        logger.error("Job " + jobId + " has failed!");
                        if (listener != null)
                            listener.jobFailed("Job: " + jobId + " failed");
                        break STATUS_LOOP;                        
                    }
                    else if (jobState == JobFlowExecutionState.TERMINATED) {
                        logger.warn("Job " + jobId + " has terminated");
                        if (listener != null)
                            listener.jobTerminated(jobId);                        
                        break STATUS_LOOP;
                    }
                    else if (jobState == JobFlowExecutionState.RUNNING) {
                        logger.info("Job " + jobId + " is running");
                        if (listener != null)
                            listener.jobRunning(jobId);                        
                    }
                    else if (jobState == JobFlowExecutionState.WAITING) {
                        logger.info("Job " + jobId + " is waiting");
                        if (listener != null)
                            listener.jobWaiting(jobId);                        
                    }
                    else if (jobState == JobFlowExecutionState.SHUTTING_DOWN) {
                        logger.info("Job " + jobId + " is shutting down");
                        if (listener != null)
                            listener.jobShuttingDown(jobId);                        
                    }
                    else if (jobState == JobFlowExecutionState.BOOTSTRAPPING) {
                        logger.info("Job " + jobId + " is bootstrapping");
                        if (listener != null)
                            listener.jobBootstrapping(jobId);                        
                    }
                    else {
                        logger.debug("Job " + jobId + " status: " + jobState + " not notification sent to listener");
                    }
                }
                Thread.sleep(statusInterval);
            }
            
        }
        catch (AmazonServiceException ase) {
            String errMsg = "Caught Exception: " + ase.getMessage() + 
                            " Reponse Status Code: " + ase.getStatusCode() +
                            " Error Code: " + ase.getErrorCode() +
                            " Request ID: " + ase.getRequestId();
            logger.error(errMsg, ase);
            if (listener != null) {
                listener.jobFailed(errMsg);
            }
        }
        catch (Exception e) {
            logger.error("Failed starting job " + name, e);
        }        
    }
    
    /**
     * Verifies that our url's have the right format (i.e. s3://)
     * @param input The input url we are verifying
     * @return String The fixed url
     */
    private String getURL(String input) {
    	if (input.startsWith("s3://")) {
    		return input;
    	}
    	return "s3://" + input;
    }
    
    /**
     * Creates our job flow configuration
     * @return JobFlowInstancesConfig The job flow config
     */
    protected JobFlowInstancesConfig getJobFlowConfig() {
        // Configure instances to use
        JobFlowInstancesConfig instances = new JobFlowInstancesConfig();
        instances.setHadoopVersion(hadoopVersion);
        instances.setMasterInstanceType(masterInstanceType);
        instances.setSlaveInstanceType(slaveInstanceType);
        instances.setInstanceCount(instanceCount);

        return instances;
    }

    /**
     * Returns the hadoopVersion
     * @return the hadoopVersion
     */
    public String getHadoopVersion() {
        return hadoopVersion;
    }

    /**
     * Sets hadoopVersion
     * @param hadoopVersion the hadoopVersion to set
     */
    public void setHadoopVersion(String hadoopVersion) {
        this.hadoopVersion = hadoopVersion;
    }

    /**
     * Returns the masterInstanceType
     * @return the masterInstanceType
     */
    public String getMasterInstanceType() {
        return masterInstanceType;
    }

    /**
     * Sets masterInstanceType
     * @param masterInstanceType the masterInstanceType to set
     */
    public void setMasterInstanceType(String masterInstanceType) {
        this.masterInstanceType = masterInstanceType;
    }

    /**
     * Returns the slaveInstanceType
     * @return the slaveInstanceType
     */
    public String getSlaveInstanceType() {
        return slaveInstanceType;
    }

    /**
     * Sets slaveInstanceType
     * @param slaveInstanceType the slaveInstanceType to set
     */
    public void setSlaveInstanceType(String slaveInstanceType) {
        this.slaveInstanceType = slaveInstanceType;
    }

    /**
     * Returns the instanceCount
     * @return the instanceCount
     */
    public Integer getInstanceCount() {
        return instanceCount;
    }

    /**
     * Sets instanceCount
     * @param instanceCount the instanceCount to set
     */
    public void setInstanceCount(Integer instanceCount) {
        this.instanceCount = instanceCount;
    }

    /**
     * Returns the logUrl
     * @return the logUrl
     */
    public String getLogUrl() {
        return logUrl;
    }

    /**
     * Sets logUrl
     * @param logUrl the logUrl to set
     */
    public void setLogUrl(String logUrl) {
        this.logUrl = logUrl;
    }
    
    
    /**
     * Returns the statusInterval
     * @return the statusInterval
     */
    public Integer getStatusInterval() {
        return statusInterval;
    }

    /**
     * Sets statusInterval
     * @param statusInterval the statusInterval to set
     */
    public void setStatusInterval(Integer statusInterval) {
        this.statusInterval = statusInterval;
    }



    /**
     * Interface for listeners interested in receiving notification of the job status
     * @author sungard
     */
    public interface AWSEMRListener {
        /**
         * Invoked when the job has started
         * @param jobId The job id for the starting job
         */
        public void jobStarting(String jobId);
        
        /**
         * Invoked when the job is complete
         * @param jobId The job id for the completed job
         */
        public void jobComplete(String jobId);
        
        /**
         * Invoked when the job was terminated
         * @param jobId The job id for the terminated job
         */
        public void jobTerminated(String jobId);

        /**
         * Invoked when the job fails
         * @param error Information on the job failure
         */
        public void jobFailed(String error);
        
        /**
         * Indicates the job is running
         * @param jobId The job id
         */
        public void jobRunning(String jobId);

        /**
         * Indicates the job is waiting
         * @param jobId The job id
         */
        public void jobWaiting(String jobId);
    
        /**
         * Indicates the job is shutting down
         * @param jobId The job id
         */
        public void jobShuttingDown(String jobId);

        /**
         * Indicates the job is bootstrapping
         * @param jobId The job id
         */
        public void jobBootstrapping(String jobId);
    }
}
