/*
 * @(#)HBaseUtils.java
 *
 * Copyright 2013 Sungard and/or its affiliates. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or 
 * without modification, are permitted provided that the following 
 * conditions are met:
 * 
 * -Redistributions of source code must retain the above copyright  
 * notice, this  list of conditions and the following disclaimer.
 * 
 * -Redistribution in binary form must reproduct the above copyright 
 * notice, this list of conditions and the following disclaimer in 
 * the documentation and/or other materials provided with the 
 * distribution.
 * 
 * Neither the name of Sungard and/or its affiliates. or the names of 
 * contributors may be used to endorse or promote products derived 
 * from this software without specific prior written permission.
 * 
 * This software is provided "AS IS," without a warranty of any 
 * kind. ALL EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND 
 * WARRANTIES, INCLUDING ANY IMPLIED WARRANTY OF MERCHANTABILITY, 
 * FITNESS FOR A PARTICULAR PURPOSE OR NON-INFRINGEMENT, ARE HEREBY 
 * EXCLUDED. SUN AND ITS LICENSORS SHALL NOT BE LIABLE FOR ANY 
 * DAMAGES OR LIABILITIES  SUFFERED BY LICENSEE AS A RESULT OF  OR 
 * RELATING TO USE, MODIFICATION OR DISTRIBUTION OF THE SOFTWARE OR 
 * ITS DERIVATIVES. IN NO EVENT WILL SUN OR ITS LICENSORS BE LIABLE 
 * FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR DIRECT, INDIRECT, 
 * SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES, HOWEVER 
 * CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING OUT OF 
 * THE USE OF OR INABILITY TO USE SOFTWARE, EVEN IF SUN HAS BEEN 
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 * 
 * You acknowledge that Software is not designed, licensed or 
 * intended for use in the design, construction, operation or 
 * maintenance of any nuclear facility. 
 */
package com.sungard.common.utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.NavigableMap;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.HColumnDescriptor;
import org.apache.hadoop.hbase.HTableDescriptor;
import org.apache.hadoop.hbase.client.Delete;
import org.apache.hadoop.hbase.client.Get;
import org.apache.hadoop.hbase.client.HBaseAdmin;
import org.apache.hadoop.hbase.client.HTable;
import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.client.ResultScanner;
import org.apache.hadoop.hbase.client.Scan;
import org.apache.hadoop.hbase.util.Bytes;

/**
 * A utilities used to interact with HBase
 * @author mprajapati
 */
public class HBaseUtils {
    private static final Log logger = LogFactory.getLog(HBaseUtils.class);
	
    private Configuration conf;
    private HBaseAdmin admin;
    private ConcurrentHashMap<String, HTable> tables = new ConcurrentHashMap<String, HTable>();

    /**
     * Constructor
     */
    public HBaseUtils() {
        this(null);
    }
    
    /**
     * Constructor
     * @param conf Our configuration
     */
    public HBaseUtils(Configuration conf) {
        this.conf = conf;
        if (this.conf == null) {
            this.conf = HBaseConfiguration.create(conf);
        }
    }
    
    /**
     * Releases the resources associated with this utility
     */
    public void close() {
        // close our admin
        closeQuietly(admin);
        
        // close the tables
        for (String tableName : tables.keySet()) {
            closeQuietly(tables.get(tableName));
        }
    }
    
    /**
     * Gets our HBaseAdmin instance
     * @return HBaseAdmin The admin
     */
    protected HBaseAdmin getAdmin() throws Exception {
        if (admin == null) {
            logger.info("No HBaseAdmin initialized, initializing...");
            admin = new HBaseAdmin(conf);
        }
        return admin;
    }
    
    /**
     * Returns a list of the tables in our HBase instance
     * @return String[] The set of tables in our instance
     */
    public String[] listTables() {
        String[] result = null;
        try {
            HBaseAdmin admin = getAdmin();
            HTableDescriptor[] tables = admin.listTables();
            result = new String[tables.length];
            for (int i = 0; i < tables.length; i++) {
                result[i] = tables[i].getNameAsString();
            }
            return result;
        }
        catch (Exception e) {
            logger.error("Failed to list our tables!", e);
        }
        return result;
    }
    
    /**
     * Gets a table (or will create a reference if it is new
     * @param tableName The table we are referencing
     * @return HTable The table
     * @throws Exception
     */
    protected HTable getTable(String tableName) throws Exception {
        logger.info("Looking up table " + tableName + " to see if exists in cache");
        HTable hTable = tables.get(tableName);
        if (hTable == null) {
            logger.info("Table " + tableName + " does not exist, obtaining reference...");
            hTable = new HTable(conf, tableName);
            tables.put(tableName, hTable);
        }
        
        return hTable;
    }
    
    /**
     * Creates a new table in our DB
     * @param tableName The ame of the table we are creating
     * @param families The set of families to create in the table
     */
    public void createTable(String tableName, String[] families) throws Exception {
        HBaseAdmin admin = getAdmin();
        if (admin.tableExists(tableName)) {
            logger.warn("Table " + tableName + " already exists - nothing will be created");
        } 
        else {
            logger.info("Creating table " + tableName);
            HTableDescriptor tableDesc = new HTableDescriptor(tableName);
            for (int i = 0; i < families.length; i++) {
                tableDesc.addFamily(new HColumnDescriptor(families[i]));
            }
            admin.createTable(tableDesc);
            logger.info("create table " + tableName + " ok.");
        }
    }
    
    /**
     * Deletes the given table from our db
     * @param tableName The name of the table we are deleting
     * @throws Exception
     */
    public void deleteTable(String tableName) throws Exception {
        logger.info("Attempting to delete table " + tableName);
        HBaseAdmin admin = getAdmin();
        admin.disableTable(tableName);
        
        // clean up any cached reference
        if (tables.containsKey(tableName)) {
            closeQuietly(tables.remove(tableName));
        }
        
        admin.deleteTable(tableName);
        logger.info("Deleted table " + tableName);        
    }

    /**
     * Adds a record to our database
     * @param tableName The name of the table we are writing to
     * @param rowKey The key for the row
     * @param family The column family
     * @param qualifier The qualifier
     * @param value The value we are writing
     * @throws Exception
     */
    public void addRecord(String tableName, String rowKey, String family, String qualifier, String value) throws Exception {
        logger.debug("Adding record to " + tableName + " with rowkey " + rowKey + " in family " + family + " in column " + qualifier + " with value " + value);
        HTable table = getTable(tableName);
        Put put = new Put(Bytes.toBytes(rowKey));
        put.add(Bytes.toBytes(family), Bytes.toBytes(qualifier), Bytes.toBytes(value));
        table.put(put);
    }
    
    /**
     * Does a batch insert in to the given table
     * @param tableName The table we are inserting in to
     * @param putRecords The records to insert
     */
    public void addBulkRecords(String tableName, List<Put> putRecords) throws Exception {
        logger.info("Adding " + putRecords.size() + " bulk records to " + tableName);
        HTable table = getTable(tableName);
        table.put(putRecords);
    }

    /**
     * Deletes the given row from the database
     * @param tableName The table we are removing the record from
     * @param rowKey The row we are removing
     * @throws Exception
     */
    public void deleteRecord(String tableName, String rowKey) throws Exception {
        logger.info("Attempting to delete record in " + tableName + " with rowKey "+ rowKey);
        HTable table = getTable(tableName);
        List<Delete> list = new ArrayList<Delete>();
        Delete del = new Delete(rowKey.getBytes());
        list.add(del);
        table.delete(list);
        logger.info("Deleted record " + rowKey + " from " + tableName);
    }

    /**
     * Gets a record from our DB
     * 
     * @param tableName The table containing the record
     * @param rowKey The row we are loading
     * @return Map<String, byte[]> The data mapped out to a key-value list
     * @throws Exception
     */
    public  Map<String, byte[]> getRecord(String tableName, String rowKey) throws Exception {
        logger.debug("Retrieving row " + rowKey + " from " + tableName);
        HTable table = getTable(tableName);
        Get get = new Get(rowKey.getBytes());
        Result rs = table.get(get);
        return resultToMap(rs);
    }
	
    /**
     * Reads all the records from the table
     * 
     * @param tableName The table we are reading from
     * @return List<Map<String, byte[]>> A list representing the row containing a list representing
     * the key-values in that row
     * @throws Exception
     */
    public List<Map<String, byte[]>> getAllRecords(String tableName) throws Exception {
        logger.debug("Retrieving all records from " + tableName);
        return getRecords(tableName, null, null);
    }

    /**
     * Reads all the records between the given row keys
     * 
     * @param tableName The table we are reading from
     * @param startRowKey The start row key
     * @param endRowKey The end row key
     * @return List<Map<String, byte[]>> A list representing the row containing a list representing
     * the key-values in that row
     * @throws Exception
     */
    public List<Map<String, byte[]>> getAllRecordFromRowKeys(String tableName, String startRowKey, String endRowKey) throws Exception {
        logger.debug("Retrieving records from " + tableName + " between " + startRowKey + " and " + endRowKey);
        return getRecords(tableName, startRowKey, endRowKey);
    }

    /**
     * Reads all the records between the given row keys
     * 
     * @param tableName The table we are reading from
     * @param startRowKey The start row key
     * @param endRowKey The end row key
     * @return List<Map<String, byte[]>> A list representing the row containing a list representing
     * the key-values in that row
     * @throws Exception
     */
    private List<Map<String, byte[]>> getRecords(String tableName, String startRowKey, String endRowKey) throws Exception {
        List<Map<String, byte[]>> result = new ArrayList<Map<String, byte[]>>();

        HTable table = getTable(tableName);

        // if we have a start and end then create our scan around those
        Scan s = null;        
        if ((startRowKey != null) && (endRowKey != null)) {
            s = new Scan(startRowKey.getBytes(),endRowKey.getBytes());
        }
        else {
            s = new Scan();
        }
        
        // now get our results and convert in to our list of list
        ResultScanner rs = table.getScanner(s);
        Result r = rs.next();
        while (r != null) {
            Map<String, byte[]> row = resultToMap(r);
            result.add(row);
            r = rs.next();
        }
        return result;                
    }
    
    /**
     * Maps a result to a friendly map
     * @param r The result we are mapping
     * @return Map<String, byte[]> The data from the resul
     */
    private Map<String, byte[]> resultToMap(Result r) {
        Map<String, byte[]> row = new HashMap<String, byte[]>();
        row.put("rowKey", r.getRow());
        NavigableMap<byte[],NavigableMap<byte[],byte[]>> resultSetRow = r.getNoVersionMap();
        for (byte[] family : resultSetRow.keySet()) {
            NavigableMap<byte[],byte[]> familyColumns = resultSetRow.get(family);
            for (Map.Entry<byte[], byte[]> me : familyColumns.entrySet()) {
                row.put(new String(me.getKey()), me.getValue());
            }
        }
        return row;
    }
    
    /**
     * Closes our admin in a manner that ignores any failures
     * @param admin The admin handle we are closing
     */
    private void closeQuietly(HBaseAdmin admin) {
    	try {
    		if (admin != null) {
    			admin.close();
    		}
    	}
    	catch (Exception e) {
    		logger.warn("Failed clsoing our HBaseAdmin connection", e);
    	}
    }

    /**
     * Closes our table in a manner that ignores any failures
     * @param table The table we are closing
     */
    private void closeQuietly(HTable table) {
    	try {
    		if (table != null) {
    			table.close();
    		}
    	}
    	catch (Exception e) {
            logger.warn("Failed clsoing our table", e);
    	}
    }
}
