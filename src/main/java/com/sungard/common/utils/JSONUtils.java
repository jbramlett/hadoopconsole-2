/*
 * @(#)JSONUtils.java
 *
 * Copyright 2013 Sungard and/or its affiliates. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or 
 * without modification, are permitted provided that the following 
 * conditions are met:
 * 
 * -Redistributions of source code must retain the above copyright  
 * notice, this  list of conditions and the following disclaimer.
 * 
 * -Redistribution in binary form must reproduct the above copyright 
 * notice, this list of conditions and the following disclaimer in 
 * the documentation and/or other materials provided with the 
 * distribution.
 * 
 * Neither the name of Sungard and/or its affiliates. or the names of 
 * contributors may be used to endorse or promote products derived 
 * from this software without specific prior written permission.
 * 
 * This software is provided "AS IS," without a warranty of any 
 * kind. ALL EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND 
 * WARRANTIES, INCLUDING ANY IMPLIED WARRANTY OF MERCHANTABILITY, 
 * FITNESS FOR A PARTICULAR PURPOSE OR NON-INFRINGEMENT, ARE HEREBY 
 * EXCLUDED. SUN AND ITS LICENSORS SHALL NOT BE LIABLE FOR ANY 
 * DAMAGES OR LIABILITIES  SUFFERED BY LICENSEE AS A RESULT OF  OR 
 * RELATING TO USE, MODIFICATION OR DISTRIBUTION OF THE SOFTWARE OR 
 * ITS DERIVATIVES. IN NO EVENT WILL SUN OR ITS LICENSORS BE LIABLE 
 * FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR DIRECT, INDIRECT, 
 * SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES, HOWEVER 
 * CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING OUT OF 
 * THE USE OF OR INABILITY TO USE SOFTWARE, EVEN IF SUN HAS BEEN 
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 * 
 * You acknowledge that Software is not designed, licensed or 
 * intended for use in the design, construction, operation or 
 * maintenance of any nuclear facility. 
 */
package com.sungard.common.utils;



import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.codehaus.jettison.json.JSONObject;

import com.google.gson.Gson;

/**
 * A set of utilities used to serialize an object to and from JSON
 * @author jbramlett
 */
public final class JSONUtils<T> {
	private static final Log logger = LogFactory.getLog(JSONUtils.class);
	
	private Gson gson = new Gson();
	private Class<T> clazz;
	private Type type;
	
	/**
	 * Constructor
	 * @param The class we are constructing
	 */
	public JSONUtils(Class<T> clazz) {
		this.clazz = clazz;
	}

	/**
	 * Constructor
	 * @param type The type we are constructing (used for arrays)
	 */
	public JSONUtils(Type type) {
		this.type = type;
	}

	/**
	 * Converts the given object to a JSON string
	 * @param o The object we are converting
	 * @return String the JSON string
	 */
	public String toJSON(T o) {
		return gson.toJson(o);
	}
	
	/**
	 * Converts our JSON string in to an object
	 * @param json The JSON string
	 * @return Object The instance
	 */
	public T fromJSON(String json) {
		if (clazz != null)
			return gson.fromJson(json, clazz);
		else 
			return gson.fromJson(json, type);
	}	

	/**
	 * Converts the data in the JSON String to a generic map - the map will
	 * contain a a variety of key values, some will be pure key (string) to
	 * value (string) while others will be key to map 
	 * @param json The JSON string we are converting to a map
	 * @return Map The map containing our JSON data
	 */
    @SuppressWarnings("rawtypes")
    public static Map fromJSONToMap(String json) {
        try {
            JSONObject obj = new JSONObject(json);
            Map result = new HashMap();
            buildJSONMap(obj, result);
            
            return result;
        }
        catch (Exception e) {
            logger.error("Failed to create map from our JSON string!", e);
        }
        return null;
    }

    /**
     * Internal recursive routine used to build our map from our JSON object
     * @param obj The object we are mapping
     * @param dataMap The map to hold the values for this object
     * @throws Exception
     */
    @SuppressWarnings({ "rawtypes", "unchecked" })
    protected static void buildJSONMap(JSONObject obj, Map dataMap) throws Exception {
        Iterator i = obj.keys();
        while (i.hasNext()) {
            String key = (String)i.next();
            Object val = obj.get(key);
            if (val.getClass().isAssignableFrom(JSONObject.class)) {
                Map<String, Map> inner = new HashMap<String, Map>();
                dataMap.put(key, inner);
                buildJSONMap((JSONObject)val, inner);
            }
            else {
                dataMap.put(key, val.toString());
            }
        }
    }
	
	/**
	 * Converts our JSON string in to an object - in this case our data is the result of 
	 * a prior map/reduce therefore there are two elements in our line the first being the
	 * key and the second our json string. This is just a utility since we do this operation
	 * for several different things
	 * 
	 * @param mrline The line from our mapreduce
	 * @return Object The instance
	 */
	public T fromMRInput(String mrline) {
	    String[] inputSplit = mrline.toString().split("\t", 2);
	    if (inputSplit.length != 2) {
	    	logger.error("Invalid input line (will not be processed): " + mrline.toString());
	    	return null;
	    }
	    
	    return fromJSON(inputSplit[1]);
	}	

}
